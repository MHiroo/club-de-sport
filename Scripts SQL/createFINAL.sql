-- Supprimer les tables si elles existent déjà
DROP TABLE IF EXISTS Historique;
DROP TABLE IF EXISTS Licence;
DROP TABLE IF EXISTS Etablissement;
DROP TABLE IF EXISTS CodeCommuneCoordonnee;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS posts;
DROP TABLE IF EXISTS spaces;
DROP TABLE IF EXISTS departement;

-- Table users
CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255) NOT NULL,
    prenom VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    adresse VARCHAR(255) NOT NULL,
    userType VARCHAR(50) NOT NULL
);

-- Table Departement
CREATE TABLE Departement (
    code_departement VARCHAR(255) PRIMARY KEY,
    nom_departement VARCHAR(255)
);

-- Table CodeCommuneCoordonnee
CREATE TABLE CodeCommuneCoordonnee (
    Insee_code VARCHAR(255) PRIMARY KEY,
    Zip_code VARCHAR(255),
    Latitude FLOAT,
    Longitude FLOAT,
    Departement VARCHAR(255),
    Region VARCHAR(255),
    UNIQUE KEY unique_code_commune (Insee_code, Zip_code)
);
-- Table Etablissement
CREATE TABLE Etablissement (
    Etablissement_ID INT AUTO_INCREMENT PRIMARY KEY,
    Code_Commune VARCHAR(255),
    Commune VARCHAR(255),
    Departement VARCHAR(255),
    Region VARCHAR(255),
    Statut_geo VARCHAR(255),
    Code VARCHAR(255),
    Federation VARCHAR(255),
    Clubs INT,
    EPA INT,
    Total INT,
    FOREIGN KEY (Code_Commune) REFERENCES CodeCommuneCoordonnee(Insee_code),
    FOREIGN KEY (Departement) REFERENCES Departement(code_departement)
);

-- Table Licence
CREATE TABLE Licence (
    Licence_ID INT AUTO_INCREMENT PRIMARY KEY,
    Code_Commune VARCHAR(255),
    Commune VARCHAR(255),
    Departement VARCHAR(255),
    Region VARCHAR(255),
    Statut_geo VARCHAR(255),
    Code VARCHAR(255),
    Federation VARCHAR(255),
    F_1_4_ans INT,
    F_5_9_ans INT,
    F_10_14_ans INT,
    F_15_19_ans INT,
    F_20_24_ans INT,
    F_25_29_ans INT,
    F_30_34_ans INT,
    F_35_39_ans INT,
    F_40_44_ans INT,
    F_45_49_ans INT,
    F_50_54_ans INT,
    F_55_59_ans INT,
    F_60_64_ans INT,
    F_65_69_ans INT,
    F_70_74_ans INT,
    F_75_79_ans INT,
    F_80_99_ans INT,
    F_NR INT,
    H_1_4_ans INT,
    H_5_9_ans INT,
    H_10_14_ans INT,
    H_15_19_ans INT,
    H_20_24_ans INT,
    H_25_29_ans INT,
    H_30_34_ans INT,
    H_35_39_ans INT,
    H_40_44_ans INT,
    H_45_49_ans INT,
    H_50_54_ans INT,
    H_55_59_ans INT,
    H_60_64_ans INT,
    H_65_69_ans INT,
    H_70_74_ans INT,
    H_75_79_ans INT,
    H_80_99_ans INT,
    H_NR INT,
    NR_NR INT,
    Total INT,
    FOREIGN KEY (Code_Commune) REFERENCES CodeCommuneCoordonnee(Insee_code),
    FOREIGN KEY (Departement) REFERENCES Departement(code_departement)
);
-- Table Historique
CREATE TABLE historique (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_users int(11) DEFAULT NULL,
  adresse_ip varchar(45) NOT NULL,
  date_connexion datetime NOT NULL,
  navigateur varchar(1000) NOT NULL,
  statut_connexion varchar(45) NOT NULL,
  PRIMARY KEY (id),
  KEY id_users (id_users)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;


-- Index pour lier toutes les régions
CREATE INDEX idx_etablissement_region ON Etablissement (Region);
CREATE INDEX idx_licence_region ON Licence (Region);
CREATE INDEX idx_code_commune_coordonee_region ON CodeCommuneCoordonnee (Region);

-- Index pour lier tous les codes communes
CREATE INDEX idx_etablissement_code_commune ON Etablissement (Code_Commune);
CREATE INDEX idx_licence_code_commune ON Licence (Code_Commune);
CREATE INDEX idx_code_commune_coordonee_insee_code ON CodeCommuneCoordonnee (Insee_code);

-- Index pour lier tous les départements
CREATE INDEX idx_etablissement_departement ON Etablissement (Departement);
CREATE INDEX idx_licence_departement ON Licence (Departement);
CREATE INDEX idx_code_commune_coordonee_departement ON CodeCommuneCoordonnee (Departement);

-- Index sur la colonne Commune
CREATE INDEX idx_licence__commune ON Licence (Commune);
-- Index sur la colonne Federation
CREATE INDEX idx_licence__federation ON Licence (Federation);

-- Index sur la colonne Total
CREATE INDEX idx_licence_total ON Licence (Total);
CREATE INDEX idx_licence_federation_commune ON Licence (Federation, Commune);

/**
ALTER TABLE Etablissement
ADD CONSTRAINT fk_departement
FOREIGN KEY (Departement) REFERENCES Departement(code_departement);

ALTER TABLE Licence
ADD CONSTRAINT fk_departement_licence
FOREIGN KEY (Departement) REFERENCES Departement(code_departement);
*/
