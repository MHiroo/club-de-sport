# Utiliser une image de base Tomcat avec JDK
FROM tomcat:10.1-jdk17

# Copier le fichier WAR dans le dossier webapps de Tomcat
COPY temp/club-de-sport.war /usr/local/tomcat/webapps/ROOT.war

# Exposer le port 8080 pour Tomcat
EXPOSE 8080

# Démarrer Tomcats
CMD ["catalina.sh", "run"]
