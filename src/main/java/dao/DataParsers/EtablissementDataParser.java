package dao.DataParsers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * EtablissementDataParser est une classe utilitaire qui lit les données d'un fichier CSV
 * et les enregistre dans une base de données.
 * 
 * Cette classe lit un fichier CSV contenant des informations sur les établissements,
 * traite les données pour gérer les champs vides, et insère les données dans une table
 * de la base de données.
 * 
 * @version 1.0
 */
public class EtablissementDataParser {

    public static void main(String[] args) {
        String csvFilePath = "lib/clubs-data-2021.csv";
        String csvFilePath2 = "Scripts SQL/insert_CodeCommuneCoordonnee.sql";
        String url = "jdbc:mysql://localhost:3306/club_de_sport";
        String user = "root";
        String password = "root";
        parseAndInsertEtablissementData(csvFilePath, csvFilePath2, url, user, password);
    }

    private static void parseAndInsertEtablissementData(String csvFilePath,String csvFilePath2, String url, String user, String password) {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(csvFilePath), StandardCharsets.UTF_8);
             Connection connection = DriverManager.getConnection(url, user, password)) {

            reader.lines().skip(1) // Skip the header line
                    .forEach(line -> {
                        line = line.replaceAll("\"", "").replaceAll("\\p{C}", "");
                        String[] columns = line.split(";");

                        if (columns.length >= 12) {
                            String codeCommune = columns[0].trim();
                            String commune = columns[1].trim();
                            String codeQPV = columns[2].trim();
                            String nomQPV = columns[3].trim();
                            String departement = columns[4].trim();
                            String region = columns[5].trim();
                            String statutGeo = columns[6].trim();
                            String code = columns[7].trim();
                            String federation = columns[8].trim();
                            int clubs = columns[9].isEmpty() ? 0 : Integer.parseInt(columns[9].trim());
                            int epa = columns[10].isEmpty() ? 0 : Integer.parseInt(columns[10].trim());
                            int total = columns[11].isEmpty() ? 0 : Integer.parseInt(columns[11].trim());

                            try {
                                // Vérifier si le code Insee existe déjà dans la table CodeCommuneCoordonnee
                                if (!codeCommuneExists(connection, codeCommune)) {
                                    // Si le code Insee n'existe pas, l'ajouter dans la table CodeCommuneCoordonnee
                                    insertCodeCommuneCoordonnee(connection, codeCommune, csvFilePath2);
                                }

                                // Exécuter la requête d'insertion directement dans la base de données
                                PreparedStatement statement = connection.prepareStatement("INSERT INTO Etablissement (Code_Commune, Commune, Departement, Region, Statut_geo, Code, Federation, Clubs, EPA, Total) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                                statement.setString(1, codeCommune);
                                statement.setString(2, commune);
                                statement.setString(3, departement);
                                statement.setString(4, region);
                                statement.setString(5, statutGeo);
                                statement.setString(6, code);
                                statement.setString(7, federation);
                                statement.setInt(8, clubs);
                                statement.setInt(9, epa);
                                statement.setInt(10, total);
                                statement.executeUpdate();
                            } catch (NumberFormatException | SQLException e) {
                                e.printStackTrace();
                            }
                        }
                    });

        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Les requêtes INSERT pour les établissements ont été exécutées");
    }

    private static boolean codeCommuneExists(Connection connection, String codeCommune) throws SQLException {
        String query = "SELECT COUNT(*) FROM CodeCommuneCoordonnee WHERE Insee_code = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, codeCommune);
        ResultSet resultSet = statement.executeQuery();
        if (resultSet.next()) {
            int count = resultSet.getInt(1);
            return count > 0;
        }
        return false;
    }

    private static void insertCodeCommuneCoordonnee(Connection connection, String codeCommune, String sqlFilePath) throws SQLException {
        String query = "INSERT INTO CodeCommuneCoordonnee (Insee_code) VALUES (?)";
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, codeCommune);
            statement.executeUpdate();
            
            // Lire le contenu actuel du fichier SQL
            StringBuilder content = new StringBuilder();
            try (BufferedReader reader = new BufferedReader(new FileReader(sqlFilePath))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    content.append(line).append("\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            
            // Ajouter la nouvelle requête d'insertion à la fin du contenu
            content.append("INSERT INTO CodeCommuneCoordonnee (Insee_code) VALUES ('" +
                    codeCommune + "');\n");
            
            // Réécrire tout le contenu dans le fichier
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(sqlFilePath))) {
                writer.write(content.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
