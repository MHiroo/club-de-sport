package dao.DataParsers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.Set;

/**
 * CodeCommuneCoordonneeSaver est une classe utilitaire qui lit les données d'un fichier CSV
 * et génère un script SQL pour insérer les données dans la base de données.
 * 
 * Cette classe lit un fichier CSV contenant des informations sur les communes,
 * traite les données pour gérer les champs vides et les doublons, puis génère 
 * des requêtes SQL INSERT IGNORE pour insérer les données dans la table CodeCommuneCoordonnee.
 * 
 * @version 1.0
 */
public class CodeCommuneCoordonneeSaver {

    public static void main(String[] args) {
        String csvFilePath = "lib/cities.csv"; // Chemin vers le fichier CSV
        String sqlFilePath = "Scripts SQL/insert_CodeCommuneCoordonnee.sql"; // Chemin du fichier SQL

        try (BufferedReader reader = new BufferedReader(new FileReader(csvFilePath));
             BufferedWriter writer = new BufferedWriter(new FileWriter(sqlFilePath))) {

            String line;
            Set<String> codeInseeSet = new HashSet<>();

            // Ignorer la première ligne (entête)
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                String[] data = line.split(",", -1); // Utiliser "-1" pour conserver les champs vides à la fin du tableau

                // Vérifier et remplacer les champs vides par une chaîne vide
                for (int i = 0; i < data.length; i++) {
                    if (data[i].isEmpty()) {
                        data[i] = "";
                    }
                }

                // Vérifier si le code Insee est déjà présent dans l'ensemble
                if (!codeInseeSet.contains(data[0].trim())) {
                    // Ajouter le code Insee à l'ensemble
                    codeInseeSet.add(data[0].trim());

                    // Échapper les apostrophes dans les valeurs
                    String inseeCode = data[0].trim().replace("'", "''");
                    String zipCode = data[2].trim().replace("'", "''");
                    String latitude = data[4].trim().replace("'", "''");
                    String longitude = data[5].trim().replace("'", "''");
                    String departement = data[7].trim().replace("'", "''");
                    String region = data[8].trim().replace("'", "''");

                    // Écriture de la requête SQL dans le fichier
                    writer.write("INSERT IGNORE INTO CodeCommuneCoordonnee (Insee_code, Zip_code, Latitude, Longitude, Departement, Region) VALUES ('" +
                            inseeCode + "', '" + zipCode + "', " + latitude + ", " + longitude + ", '" + departement + "', '" + region + "');\n");
                }
            }
            System.out.println("Les requêtes d'insertion ont été enregistrées dans le fichier " + sqlFilePath);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
