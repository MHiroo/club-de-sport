package dao.DataParsers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.HashSet;
import java.util.Set;

/**
 * CodeCommuneCoordonneeParser est une classe utilitaire qui lit les données d'un fichier CSV
 * et insère ces données dans une base de données MySQL.
 * 
 * Cette classe lit un fichier CSV contenant des informations sur les communes,
 * traite les données pour gérer les champs vides et les doublons, puis insère 
 * les données dans la table CodeCommuneCoordonnee de la base de données.
 * 
 * @version 1.0
 */
public class CodeCommuneCoordonneeParser {

    public static void main(String[] args) {
        String csvFilePath = "lib/cities.csv"; // Chemin vers le fichier CSV
        String url = "jdbc:mysql://localhost:3306/club_de_sport"; // URL de la base de données
        String user = "root"; // Nom d'utilisateur de la base de données
        String password = "root"; // Mot de passe de la base de données

        try (Connection connection = DriverManager.getConnection(url, user, password);
             BufferedReader reader = new BufferedReader(new FileReader(csvFilePath))) {

            String line;
            String sql = "INSERT IGNORE INTO CodeCommuneCoordonnee (Insee_code, Zip_code, Latitude, Longitude, Departement, Region) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql);

            // Utilisation d'un ensemble pour vérifier les doublons de code Insee
            Set<String> codeInseeSet = new HashSet<>();

            // Ignorer la première ligne (entête)
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                String[] data = line.split(",", -1); // Utiliser "-1" pour conserver les champs vides à la fin du tableau

                // Vérifier chaque champ et le remplacer par une chaîne vide si nécessaire
                for (int i = 0; i < data.length; i++) {
                    if (data[i].isEmpty()) {
                        data[i] = ""; // Remplacer les champs vides par une chaîne vide
                    }
                }

                // Vérifier si le code Insee est déjà présent dans l'ensemble
                if (!codeInseeSet.contains(data[0].trim())) {
                    // Utiliser les données dans la requête préparée
                    statement.setString(1, data[0].trim()); // insee_code
                    statement.setString(2, data[2].trim()); // zip_code

                    // Vérifier et convertir la latitude en double
                    double latitude = 0.0;
                    if (!data[4].isEmpty()) {
                        latitude = Double.parseDouble(data[4].trim());
                    }
                    statement.setDouble(3, latitude);

                    // Vérifier et convertir la longitude en double
                    double longitude = 0.0;
                    if (!data[5].isEmpty()) {
                        longitude = Double.parseDouble(data[5].trim());
                    }
                    statement.setDouble(4, longitude);

                    // Utiliser le département dans la requête préparée
                    statement.setString(5, data[7].trim()); // department_number

                    // Utiliser la région dans la requête préparée
                    statement.setString(6, data[8].trim()); // region_name

                    statement.addBatch();

                    // Ajouter le code Insee à l'ensemble
                    codeInseeSet.add(data[0].trim());
                }
            }

            // Exécuter les requêtes en lot
            statement.executeBatch();
            System.out.println("Les données ont été insérées avec succès.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
