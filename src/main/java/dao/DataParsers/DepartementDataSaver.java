package dao.DataParsers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.Set;

/**
 * DepartementDataSaver est une classe utilitaire qui lit les données d'un fichier CSV
 * et les enregistre sous forme de requêtes SQL dans un fichier de sortie.
 * 
 * Cette classe lit un fichier CSV contenant des informations sur les départements,
 * traite les données pour gérer les champs vides et les doublons, puis génère
 * des requêtes SQL pour insérer les données dans une table Departement.
 * 
 * @version 1.0
 */
public class DepartementDataSaver {

    public static void main(String[] args) {
        String csvFilePath = "lib/departements-france.csv"; // Chemin vers le fichier CSV
        String sqlFilePath = "Scripts SQL/insert_departement.sql"; // Chemin du fichier SQL

        try (BufferedReader reader = new BufferedReader(new FileReader(csvFilePath));
             BufferedWriter writer = new BufferedWriter(new FileWriter(sqlFilePath))) {

            String line;
            Set<String> codeDepartementSet = new HashSet<>();

            // Ignorer la première ligne (entête)
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                String[] data = line.split(",", -1); // Utiliser "-1" pour conserver les champs vides à la fin du tableau

                // Vérifier et remplacer les champs vides par une chaîne vide
                for (int i = 0; i < data.length; i++) {
                    if (data[i].isEmpty()) {
                        data[i] = "";
                    }
                }

                // Vérifier si le code département est déjà présent dans l'ensemble
                if (!codeDepartementSet.contains(data[0].trim())) {
                    // Ajouter le code département à l'ensemble
                    codeDepartementSet.add(data[0].trim());

                    // Échapper les apostrophes dans les valeurs
                    String codeDepartement = data[0].trim().replace("'", "''");
                    String nomDepartement = data[1].trim().replace("'", "''");

                    // Écriture de la requête SQL dans le fichier
                    writer.write("INSERT IGNORE INTO Departement (code_departement, nom_departement) VALUES ('" +
                            codeDepartement + "', '" + nomDepartement + "');\n");
                }
            }

            // Ajout des départements supplémentaires spécifiques
            writer.write("INSERT IGNORE INTO Departement (code_departement, nom_departement) VALUES\r\n"
                    + "('NR', 'Non renseigné'),\r\n"
                    + "('ETR', 'Étranger'),\r\n"
                    + "('988', 'Nouvelle-Calédonie'),\r\n"
                    + "('987', 'Polynésie française'),\r\n"
                    + "('986', 'Wallis-et-Futuna'),\r\n"
                    + "('980', 'Saint-Pierre-et-Miquelon'),\r\n"
                    + "('978', 'Saint-Barthélemy'),\r\n"
                    + "('977', 'Saint-Martin'),\r\n"
                    + "('975', 'Saint-Pierre-et-Miquelon');\r\n");

            System.out.println("Les requêtes d'insertion ont été enregistrées dans le fichier " + sqlFilePath);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
