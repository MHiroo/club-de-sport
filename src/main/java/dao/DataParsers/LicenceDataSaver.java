package dao.DataParsers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * LicenceDataSaver est une classe utilitaire qui lit les données d'un fichier CSV
 * et les enregistre dans un fichier SQL.
 * 
 * Cette classe lit un fichier CSV contenant des informations sur les licences,
 * traite les données pour gérer les champs vides, et génère un fichier SQL contenant
 * les instructions INSERT IGNORE INTO pour insérer les données dans une table de la base de données.
 * 
 * @version 1.0
 */
public class LicenceDataSaver {

	public static void main(String[] args) {
		String csvFilePath = "lib/lic-data-2021.csv";
		String url = "jdbc:mysql://localhost:3306/club_de_sport";
		String user = "root";
		String password = "root";
		String sqlFilePath = "Scripts SQL/Insert_Licence.sql";
		String csvFilePath2 = "Scripts SQL/insert_CodeCommuneCoordonnee.sql";
		parseAndInsertLicenceData(csvFilePath, url, user, password, sqlFilePath, csvFilePath2);
	}

	private static void parseAndInsertLicenceData(String csvFilePath, String url, String user, String password, String sqlFilePath, String csvFilePath2) {
		try (BufferedReader reader = Files.newBufferedReader(Paths.get(csvFilePath), StandardCharsets.UTF_8);
				BufferedWriter writer = new BufferedWriter(new FileWriter(sqlFilePath, StandardCharsets.UTF_8));
				Connection connection = DriverManager.getConnection(url, user, password)) {

			reader.lines().skip(1) // Skip the header line
			.forEach(line -> {
				// Supprimer les guillemets et les caractères invisibles de la ligne
				line = line.replaceAll("\"", "").replaceAll("\\p{C}", "");
				String[] columns = line.split(";");

				if (columns.length >= 47) {
					String codeCommune = columns[0].trim().replace("'", "''");
					String commune = columns[1].trim().replace("'", "''");
					String codeQPV = columns[2].trim().replace("'", "''");
					String nomQPV = columns[3].trim().replace("'", "''");
					String departement = columns[4].trim().replace("'", "''");
					String region = columns[5].trim().replace("'", "''");
					String statutGeo = columns[6].trim().replace("'", "''");
					String code = columns[7].trim().replace("'", "''");
					String federation = columns[8].trim().replace("'", "''");
					int f_1_4_ans = Integer.parseInt(columns[9].trim());
					int f_5_9_ans = Integer.parseInt(columns[10].trim());
					int f_10_14_ans = Integer.parseInt(columns[11].trim());
					int f_15_19_ans = Integer.parseInt(columns[12].trim());
					int f_20_24_ans = Integer.parseInt(columns[13].trim());
					int f_25_29_ans = Integer.parseInt(columns[14].trim());
					int f_30_34_ans = Integer.parseInt(columns[15].trim());
					int f_35_39_ans = Integer.parseInt(columns[16].trim());
					int f_40_44_ans = Integer.parseInt(columns[17].trim());
					int f_45_49_ans = Integer.parseInt(columns[18].trim());
					int f_50_54_ans = Integer.parseInt(columns[19].trim());
					int f_55_59_ans = Integer.parseInt(columns[20].trim());
					int f_60_64_ans = Integer.parseInt(columns[21].trim());
					int f_65_69_ans = Integer.parseInt(columns[22].trim());
					int f_70_74_ans = Integer.parseInt(columns[23].trim());
					int f_75_79_ans = Integer.parseInt(columns[24].trim());
					int f_80_99_ans = Integer.parseInt(columns[25].trim());
					int f_NR = Integer.parseInt(columns[26].trim());
					int h_1_4_ans = Integer.parseInt(columns[27].trim());
					int h_5_9_ans = Integer.parseInt(columns[28].trim());
					int h_10_14_ans = Integer.parseInt(columns[29].trim());
					int h_15_19_ans = Integer.parseInt(columns[30].trim());
					int h_20_24_ans = Integer.parseInt(columns[31].trim());
					int h_25_29_ans = Integer.parseInt(columns[32].trim());
					int h_30_34_ans = Integer.parseInt(columns[33].trim());
					int h_35_39_ans = Integer.parseInt(columns[34].trim());
					int h_40_44_ans = Integer.parseInt(columns[35].trim());
					int h_45_49_ans = Integer.parseInt(columns[36].trim());
					int h_50_54_ans = Integer.parseInt(columns[37].trim());
					int h_55_59_ans = Integer.parseInt(columns[38].trim());
					int h_60_64_ans = Integer.parseInt(columns[39].trim());
					int h_65_69_ans = Integer.parseInt(columns[40].trim());
					int h_70_74_ans = Integer.parseInt(columns[41].trim());
					int h_75_79_ans = Integer.parseInt(columns[42].trim());
					int h_80_99_ans = Integer.parseInt(columns[43].trim());
					int h_NR =columns[44].trim().isEmpty() ? 0 :  Integer.parseInt(columns[44].trim());
					int nr_NR =columns[45].trim().isEmpty() ? 0 :  Integer.parseInt(columns[45].trim());
					int total =columns[46].trim().isEmpty() ? 0 :  Integer.parseInt(columns[46].trim());

					try {
						// Vérifier si le code Insee existe déjà dans la table CodeCommuneCoordonnee
						if (!codeCommuneExists(connection, codeCommune)) {
							// Si le code Insee n'existe pas, l'ajouter dans la table CodeCommuneCoordonnee
							insertCodeCommuneCoordonnee(connection, codeCommune, csvFilePath2);
						}
						// Écrire la requête d'insertion pour la table Licence dans le fichier SQL
						writer.write("INSERT INTO Licence (Code_Commune, Commune, Departement, Region, Statut_geo, Code, Federation, F_1_4_ans, F_5_9_ans, F_10_14_ans, F_15_19_ans, F_20_24_ans, F_25_29_ans, F_30_34_ans, F_35_39_ans, F_40_44_ans, F_45_49_ans, F_50_54_ans, F_55_59_ans, F_60_64_ans, F_65_69_ans, F_70_74_ans, F_75_79_ans, F_80_99_ans, F_NR, H_1_4_ans, H_5_9_ans, H_10_14_ans, H_15_19_ans, H_20_24_ans, H_25_29_ans, H_30_34_ans, H_35_39_ans, H_40_44_ans, H_45_49_ans, H_50_54_ans, H_55_59_ans, H_60_64_ans, H_65_69_ans, H_70_74_ans, H_75_79_ans, H_80_99_ans, H_NR, NR_NR, Total)\n");
						writer.write("VALUES ('" + codeCommune + "', '" + commune + "', '" + departement + "', '" + region + "', '" + statutGeo + "', '" + code + "', '" + federation + "', " + f_1_4_ans + ", " + f_5_9_ans + ", " + f_10_14_ans + ", " + f_15_19_ans + ", " + f_20_24_ans + ", " + f_25_29_ans + ", " + f_30_34_ans + ", " + f_35_39_ans + ", " + f_40_44_ans + ", " + f_45_49_ans + ", " + f_50_54_ans + ", " + f_55_59_ans + ", " + f_60_64_ans + ", " + f_65_69_ans + ", " + f_70_74_ans + ", " + f_75_79_ans + ", " + f_80_99_ans + ", " + f_NR + ", " + h_1_4_ans + ", " + h_5_9_ans + ", " + h_10_14_ans + ", " + h_15_19_ans + ", " + h_20_24_ans + ", " + h_25_29_ans + ", " + h_30_34_ans + ", " + h_35_39_ans + ", " + h_40_44_ans + ", " + h_45_49_ans + ", " + h_50_54_ans + ", " + h_55_59_ans + ", " + h_60_64_ans + ", " + h_65_69_ans + ", " + h_70_74_ans + ", " + h_75_79_ans + ", " + h_80_99_ans + ", " + h_NR + ", " + nr_NR + ", " + total + ");\n");
					} catch (IOException | NumberFormatException | SQLException e) {
						e.printStackTrace();
					}
				}
			});

		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Les requêtes INSERT pour les licences ont été exécutées et stockées dans le fichier Insert_Licence.sql");
	}

	private static boolean codeCommuneExists(Connection connection, String codeCommune) throws SQLException {
		String query = "SELECT COUNT(*) FROM CodeCommuneCoordonnee WHERE Insee_code = ?";
		try (PreparedStatement statement = connection.prepareStatement(query)) {
			statement.setString(1, codeCommune);
			try (ResultSet resultSet = statement.executeQuery()) {
				if (resultSet.next()) {
					int count = resultSet.getInt(1);
					return count > 0;
				}
			}
		}
		return false;
	}

	private static void insertCodeCommuneCoordonnee(Connection connection, String codeCommune, String sqlFilePath) throws SQLException {
		String query = "INSERT INTO CodeCommuneCoordonnee (Insee_code) VALUES (?)";
		try (PreparedStatement statement = connection.prepareStatement(query)) {
			statement.setString(1, codeCommune);
			statement.executeUpdate();

			// Lire le contenu actuel du fichier SQL
			StringBuilder content = new StringBuilder();
			try (BufferedReader reader = Files.newBufferedReader(Paths.get(sqlFilePath), StandardCharsets.UTF_8)) {
				String line;
				while ((line = reader.readLine()) != null) {
					content.append(line).append("\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			// Ajouter la nouvelle requête d'insertion à la fin du contenu
			content.append("INSERT INTO CodeCommuneCoordonnee (Insee_code) VALUES ('" + codeCommune + "');\n");

			// Réécrire tout le contenu dans le fichier
			try (BufferedWriter writer = new BufferedWriter(new FileWriter(sqlFilePath))) {
				writer.write(content.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Les requêtes INSERT pour les Code_Commune ont été exécutées et stockées dans le fichier Insert_CodeCommuneCoordonnee.sql");
	}
}
