package dao.DataParsers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * LicenceDataParser est une classe utilitaire qui lit les données d'un fichier CSV
 * et les insère dans une base de données.
 * 
 * Cette classe lit un fichier CSV contenant des informations sur les licences,
 * traite les données pour gérer les champs vides, et insère les données dans une table
 * de la base de données.
 * 
 * @version 1.0
 */
public class LicenceDataParser {

	public static void main(String[] args) {
		String csvFilePath = "lib/lic-data-2021.csv";
		String url = "jdbc:mysql://localhost:3306/club_de_sport";
		String user = "root";
		String password = "root";
		String csvFilePath2 = "Scripts SQL/insert_CodeCommuneCoordonnee.sql";
		parseAndInsertLicenceData(csvFilePath, url, user, password, csvFilePath2);
	}

	private static void parseAndInsertLicenceData(String csvFilePath, String url, String user, String password, String csvFilePath2) {
		try (BufferedReader reader = Files.newBufferedReader(Paths.get(csvFilePath), StandardCharsets.UTF_8);
				Connection connection = DriverManager.getConnection(url, user, password)) {

			reader.lines().skip(1) // Skip the header line
			.forEach(line -> {
				// Supprimer les guillemets et les caractères invisibles de la ligne
				line = line.replaceAll("\"", "").replaceAll("\\p{C}", "");
				String[] columns = line.split(";");

				if (columns.length >= 47) {
					String codeCommune = columns[0].trim();
					String commune = columns[1].trim();
					String codeQPV = columns[2].trim();
					String nomQPV = columns[3].trim();
					String departement = columns[4].trim();
					String region = columns[5].trim();
					String statutGeo = columns[6].trim();
					String code = columns[7].trim();
					String federation = columns[8].trim();
					int f_1_4_ans = Integer.parseInt(columns[9].trim());
					int f_5_9_ans = Integer.parseInt(columns[10].trim());
					int f_10_14_ans = Integer.parseInt(columns[11].trim());
					int f_15_19_ans = Integer.parseInt(columns[12].trim());
					int f_20_24_ans = Integer.parseInt(columns[13].trim());
					int f_25_29_ans = Integer.parseInt(columns[14].trim());
					int f_30_34_ans = Integer.parseInt(columns[15].trim());
					int f_35_39_ans = Integer.parseInt(columns[16].trim());
					int f_40_44_ans = Integer.parseInt(columns[17].trim());
					int f_45_49_ans = Integer.parseInt(columns[18].trim());
					int f_50_54_ans = Integer.parseInt(columns[19].trim());
					int f_55_59_ans = Integer.parseInt(columns[20].trim());
					int f_60_64_ans = Integer.parseInt(columns[21].trim());
					int f_65_69_ans = Integer.parseInt(columns[22].trim());
					int f_70_74_ans = Integer.parseInt(columns[23].trim());
					int f_75_79_ans = Integer.parseInt(columns[24].trim());
					int f_80_99_ans = Integer.parseInt(columns[25].trim());
					int f_NR = Integer.parseInt(columns[26].trim());
					int h_1_4_ans = Integer.parseInt(columns[27].trim());
					int h_5_9_ans = Integer.parseInt(columns[28].trim());
					int h_10_14_ans = Integer.parseInt(columns[29].trim());
					int h_15_19_ans = Integer.parseInt(columns[30].trim());
					int h_20_24_ans = Integer.parseInt(columns[31].trim());
					int h_25_29_ans = Integer.parseInt(columns[32].trim());
					int h_30_34_ans = Integer.parseInt(columns[33].trim());
					int h_35_39_ans = Integer.parseInt(columns[34].trim());
					int h_40_44_ans = Integer.parseInt(columns[35].trim());
					int h_45_49_ans = Integer.parseInt(columns[36].trim());
					int h_50_54_ans = Integer.parseInt(columns[37].trim());
					int h_55_59_ans = Integer.parseInt(columns[38].trim());
					int h_60_64_ans = Integer.parseInt(columns[39].trim());
					int h_65_69_ans = Integer.parseInt(columns[40].trim());
					int h_70_74_ans = Integer.parseInt(columns[41].trim());
					int h_75_79_ans = Integer.parseInt(columns[42].trim());
					int h_80_99_ans = Integer.parseInt(columns[43].trim());
					int h_NR =columns[44].trim().isEmpty() ? 0 :  Integer.parseInt(columns[44].trim());
					int nr_NR =columns[45].trim().isEmpty() ? 0 :  Integer.parseInt(columns[45].trim());
					int total =columns[46].trim().isEmpty() ? 0 :  Integer.parseInt(columns[46].trim());

					try {
						// Vérifier si le code Insee existe déjà dans la table CodeCommuneCoordonnee
						if (!codeCommuneExists(connection, codeCommune)) {
							// Si le code Insee n'existe pas, l'ajouter dans la table CodeCommuneCoordonnee
							insertCodeCommuneCoordonnee(connection, codeCommune, csvFilePath2);
						}
						// Exécuter la requête d'insertion directement dans la base de données
						PreparedStatement statement = connection.prepareStatement("INSERT INTO Licence (Code_Commune, Commune, Departement, Region, Statut_geo, Code, Federation, F_1_4_ans, F_5_9_ans, F_10_14_ans, F_15_19_ans, F_20_24_ans, F_25_29_ans, F_30_34_ans, F_35_39_ans, F_40_44_ans, F_45_49_ans, F_50_54_ans, F_55_59_ans, F_60_64_ans, F_65_69_ans, F_70_74_ans, F_75_79_ans, F_80_99_ans, F_NR, H_1_4_ans, H_5_9_ans, H_10_14_ans, H_15_19_ans, H_20_24_ans, H_25_29_ans, H_30_34_ans, H_35_39_ans, H_40_44_ans, H_45_49_ans, H_50_54_ans, H_55_59_ans, H_60_64_ans, H_65_69_ans, H_70_74_ans, H_75_79_ans, H_80_99_ans, H_NR, NR_NR, Total) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
						statement.setString(1, codeCommune);
						statement.setString(2, commune);
						statement.setString(3, departement);
						statement.setString(4, region);
						statement.setString(5, statutGeo);
						statement.setString(6, code);
						statement.setString(7, federation);
						statement.setInt(8, f_1_4_ans);
						statement.setInt(9, f_5_9_ans);
						statement.setInt(10, f_10_14_ans);
						statement.setInt(11, f_15_19_ans);
						statement.setInt(12, f_20_24_ans);
						statement.setInt(13, f_25_29_ans);
						statement.setInt(14, f_30_34_ans);
						statement.setInt(15, f_35_39_ans);
						statement.setInt(16, f_40_44_ans);
						statement.setInt(17, f_45_49_ans);
						statement.setInt(18, f_50_54_ans);
						statement.setInt(19, f_55_59_ans);
						statement.setInt(20, f_60_64_ans);
						statement.setInt(21, f_65_69_ans);
						statement.setInt(22, f_70_74_ans);
						statement.setInt(23, f_75_79_ans);
						statement.setInt(24, f_80_99_ans);
						statement.setInt(25, f_NR);
						statement.setInt(26, h_1_4_ans);
						statement.setInt(27, h_5_9_ans);
						statement.setInt(28, h_10_14_ans);
						statement.setInt(29, h_15_19_ans);
						statement.setInt(30, h_20_24_ans);
						statement.setInt(31, h_25_29_ans);
						statement.setInt(32, h_30_34_ans);
						statement.setInt(33, h_35_39_ans);
						statement.setInt(34, h_40_44_ans);
						statement.setInt(35, h_45_49_ans);
						statement.setInt(36, h_50_54_ans);
						statement.setInt(37, h_55_59_ans);
						statement.setInt(38, h_60_64_ans);
						statement.setInt(39, h_65_69_ans);
						statement.setInt(40, h_70_74_ans);
						statement.setInt(41, h_75_79_ans);
						statement.setInt(42, h_80_99_ans);
						statement.setInt(43, h_NR);
						statement.setInt(44, nr_NR);
						statement.setInt(45, total);
						statement.executeUpdate();
					} catch ( NumberFormatException | SQLException e) {
						e.printStackTrace();
					}
				}
			});

		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Les requêtes INSERT pour les licences ont été exécutées et stockées dans le fichier Insert_Licence.sql");
	}

	private static boolean codeCommuneExists(Connection connection, String codeCommune) throws SQLException {
		String query = "SELECT COUNT(*) FROM CodeCommuneCoordonnee WHERE Insee_code = ?";
		try (PreparedStatement statement = connection.prepareStatement(query)) {
			statement.setString(1, codeCommune);
			try (ResultSet resultSet = statement.executeQuery()) {
				if (resultSet.next()) {
					int count = resultSet.getInt(1);
					return count > 0;
				}
			}
		}
		return false;
	}

	private static void insertCodeCommuneCoordonnee(Connection connection, String codeCommune, String sqlFilePath) throws SQLException {
		String query = "INSERT INTO CodeCommuneCoordonnee (Insee_code) VALUES (?)";
		try (PreparedStatement statement = connection.prepareStatement(query)) {
			statement.setString(1, codeCommune);
			statement.executeUpdate();

			// Lire le contenu actuel du fichier SQL
			StringBuilder content = new StringBuilder();
			try (BufferedReader reader = Files.newBufferedReader(Paths.get(sqlFilePath), StandardCharsets.UTF_8)) {
				String line;
				while ((line = reader.readLine()) != null) {
					content.append(line).append("\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

			// Ajouter la nouvelle requête d'insertion à la fin du contenu
			content.append("INSERT INTO CodeCommuneCoordonnee (Insee_code) VALUES ('" + codeCommune + "');\n");

			// Réécrire tout le contenu dans le fichier
			try (BufferedWriter writer = new BufferedWriter(new FileWriter(sqlFilePath))) {
				writer.write(content.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Les requêtes INSERT pour les Code_Commune ont été exécutées et stockées dans le fichier Insert_CodeCommuneCoordonnee.sql");
	}
}
