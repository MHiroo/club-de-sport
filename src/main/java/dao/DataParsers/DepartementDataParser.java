package dao.DataParsers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.HashSet;
import java.util.Set;

/**
 * DepartementDataParser est une classe utilitaire qui lit les données d'un fichier CSV
 * et insère ces données dans une base de données MySQL.
 * 
 * Cette classe lit un fichier CSV contenant des informations sur les départements,
 * traite les données pour gérer les champs vides et les doublons, puis insère 
 * les données dans la table Departement de la base de données.
 * 
 * @version 1.0
 */
public class DepartementDataParser {

    public static void main(String[] args) {
        String csvFilePath = "lib/departements-france.csv"; // Chemin vers le fichier CSV
        String url = "jdbc:mysql://localhost:3306/club_de_sport"; // URL de la base de données
        String user = "root"; // Nom d'utilisateur de la base de données
        String password = "root"; // Mot de passe de la base de données

        try (Connection connection = DriverManager.getConnection(url, user, password);
             BufferedReader reader = new BufferedReader(new FileReader(csvFilePath))) {

            String line;
            String sql = "INSERT IGNORE INTO Departement (code_departement, nom_departement) VALUES (?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql);

            // Utilisation d'un ensemble pour vérifier les doublons de code département
            Set<String> codeDepartementSet = new HashSet<>();

            // Ignorer la première ligne (entête)
            reader.readLine();

            while ((line = reader.readLine()) != null) {
                String[] data = line.split(",", -1); // Utiliser "-1" pour conserver les champs vides à la fin du tableau

                // Vérifier chaque champ et le remplacer par une chaîne vide si nécessaire
                for (int i = 0; i < data.length; i++) {
                    if (data[i].isEmpty()) {
                        data[i] = ""; // Remplacer les champs vides par une chaîne vide
                    }
                }

                // Vérifier si le code département est déjà présent dans l'ensemble
                if (!codeDepartementSet.contains(data[0].trim())) {
                    // Utiliser les données dans la requête préparée
                    statement.setString(1, data[0].trim()); // code_departement
                    statement.setString(2, data[1].trim()); // nom_departement

                    statement.addBatch();
                    // Ajouter le code département à l'ensemble
                    codeDepartementSet.add(data[0].trim());
                }
            }

            // Ajout de départements supplémentaires spécifiques
            String[] codesDepartement = {"NR", "ETR", "988", "987", "986", "980", "978", "977", "975"};
            String[] nomsDepartement = {"Non renseigné", "Étranger", "Nouvelle-Calédonie", "Polynésie française", "Wallis-et-Futuna", "Saint-Pierre-et-Miquelon", "Saint-Barthélemy", "Saint-Martin", "Saint-Pierre-et-Miquelon"};

            for (int i = 0; i < codesDepartement.length; i++) {
                statement.setString(1, codesDepartement[i]);
                statement.setString(2, nomsDepartement[i]);
                statement.addBatch();
            }

            // Exécuter les requêtes en lot
            statement.executeBatch();
            System.out.println("Les données ont été insérées avec succès.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
