package dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.mindrot.jbcrypt.BCrypt;

import model.*;

/**
 * UserDAO est une classe qui gère les opérations de base de données pour les utilisateurs.
 * 
 * Cette classe fournit des méthodes pour créer, lire, mettre à jour et supprimer des enregistrements d'utilisateurs
 * dans la base de données. Elle utilise les connexions fournies par ConnectionDAO pour interagir avec la base de données.
 * 
 * @version 1.0
 */
public class UserDAO {
    private String url;
    private String user;
    private String password;

    // Requêtes SQL
    private static final String INSERT_USER = "INSERT INTO inscription (Nom, Prenom, Email, Type, Etablissement_ID, Preuve, valider) VALUES (?, ?, ?, ?, ?, ?, 0)";
    private static final String SELECT_USER_BY_ID = "SELECT * FROM users WHERE id=?";
    private static final String SELECT_USER_BY_EMAIL_PASSWORD = "SELECT * from inscription where Email = ?";
    private static final String UPDATE_USER = "UPDATE users SET nom=?, prenom=?, password=?, email=?, adresse=?, userType=? WHERE id=?";
    private static final String DELETE_USER = "DELETE FROM users WHERE id=?";
    private static final String SELECT_ALL_USERS = "SELECT * FROM users";
    private static final String SELECT_USER_BY_EMAIL = "SELECT * FROM inscription WHERE Email=?";

    /**
     * Constructeur de la classe UserDAO.
     * Charge les paramètres de connexion depuis un fichier de configuration et assure
     * que le driver JDBC est chargé.
     */
    public UserDAO() {
        Properties prop = new Properties();
        try {
            // Charger le fichier de propriétés depuis les ressources
            InputStream inputStream = ClubSportifDAO.class.getClassLoader().getResourceAsStream("config.properties");
            if (inputStream != null) {
                prop.load(inputStream);
                url = prop.getProperty("db.url");
                user = prop.getProperty("db.username");
                password = prop.getProperty("db.password");
            } else {
                System.err.println("Le fichier config.properties n'a pas pu être chargé.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // Assurez-vous que le driver JDBC de MySQL est chargé
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    // Méthodes pour accéder aux informations de connexion

    /**
     * Retourne l'URL de connexion à la base de données.
     * 
     * @return URL de connexion.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Retourne le nom d'utilisateur pour la connexion à la base de données.
     * 
     * @return Nom d'utilisateur.
     */
    public String getUser() {
        return user;
    }

    /**
     * Retourne le mot de passe pour la connexion à la base de données.
     * 
     * @return Mot de passe.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Méthode pour établir la connexion à la base de données.
     * 
     * @return Une connexion à la base de données.
     * @throws SQLException Si une erreur SQL survient.
     */
    public Connection getConnection() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            // Gestion de l'erreur ici. Par exemple, en la journalisant et en lançant une exception personnalisée
            e.printStackTrace();
        }
        return DriverManager.getConnection(url, user, password);
    }

    /**
     * Méthode pour vérifier si un utilisateur avec l'adresse e-mail spécifiée existe déjà.
     * 
     * @param email L'adresse e-mail à vérifier.
     * @return true si l'e-mail existe déjà, false sinon.
     */
    public boolean emailExists(String email) {
        try (Connection conn = getConnection();
             PreparedStatement statement = conn.prepareStatement(SELECT_USER_BY_EMAIL)) {
            statement.setString(1, email);
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next(); // Si un utilisateur est trouvé, l'e-mail existe déjà
        } catch (SQLException e) {
            e.printStackTrace();
            return false; // En cas d'erreur, on suppose que l'e-mail n'existe pas déjà pour éviter les erreurs potentielles
        }
    }

    /**
     * Méthode pour vérifier si un utilisateur est déjà validé.
     * 
     * @param email L'adresse e-mail à vérifier.
     * @return true si l'utilisateur est validé, false sinon.
     */
    public boolean isValid(String email) {
        try (Connection conn = getConnection();
             PreparedStatement statement = conn.prepareStatement("SELECT * FROM inscription WHERE Email=? AND valider = 2")) {
            statement.setString(1, email);
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next(); // Si un utilisateur est trouvé, l'e-mail existe déjà
        } catch (SQLException e) {
            e.printStackTrace();
            return false; // En cas d'erreur, on suppose que l'e-mail n'existe pas déjà pour éviter les erreurs potentielles
        }
    }

    /**
     * Méthode pour ajouter un utilisateur.
     * 
     * @param nom Le nom de l'utilisateur.
     * @param prenom Le prénom de l'utilisateur.
     * @param mail L'adresse e-mail de l'utilisateur.
     * @param type Le type d'utilisateur.
     * @param id L'identifiant de l'établissement.
     * @param preuve La preuve de l'utilisateur sous forme de tableau d'octets.
     */
    public void addUser(String nom, String prenom, String mail, String type, int id, byte[] preuve) {
        try (Connection conn = getConnection();
             PreparedStatement statement = conn.prepareStatement(INSERT_USER)) {
            statement.setString(1, nom);
            statement.setString(2, prenom);
            statement.setString(3, mail);
            statement.setString(4, type);
            statement.setInt(5, id);

            if (preuve != null) {
                statement.setBlob(6, new javax.sql.rowset.serial.SerialBlob(preuve));
            } else {
                statement.setNull(6, java.sql.Types.BLOB);
            }

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode pour obtenir un utilisateur par son ID.
     * 
     * @param id L'identifiant de l'utilisateur.
     * @return Un objet User correspondant à l'identifiant spécifié.
     */
    public User getUserById(int id) {
        User user = null;
        try (Connection conn = getConnection();
             PreparedStatement statement = conn.prepareStatement(SELECT_USER_BY_ID)) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = extractUserFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * Méthode pour authentifier un utilisateur par son email et son mot de passe.
     * 
     * @param email L'adresse e-mail de l'utilisateur.
     * @param password Le mot de passe de l'utilisateur.
     * @return Un objet User si l'authentification réussit, null sinon.
     */
    public User authenticateUser(String email, String password) {
        User user = null;
        try (Connection conn = getConnection();
             PreparedStatement statement = conn.prepareStatement(SELECT_USER_BY_EMAIL_PASSWORD)) {
            statement.setString(1, email);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                // Récupération du mot de passe haché depuis la base de données
                String hashedPasswordFromDB = resultSet.getString("password");
                // Vérification que le mot de passe fourni correspond au mot de passe haché en base de données
                if (BCrypt.checkpw(password, hashedPasswordFromDB)) {
                    user = new User();
                    user.setId(resultSet.getInt("Inscription_ID"));
                    user.setNom(resultSet.getString("Nom"));
                    user.setPrenom(resultSet.getString("Prenom"));
                    user.setPassword(hashedPasswordFromDB); // Stockage du mot de passe haché
                    user.setEmail(resultSet.getString("Email"));
                    user.setUserType(resultSet.getString("Type"));
                    user.setValider(resultSet.getInt("valider"));
                    user.setPhotoProfil(resultSet.getBytes("PhotoProfil")); // Assurez-vous que votre modèle Post peut gérer les données binaires pour les photos
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * Méthode pour mettre à jour le mot de passe d'un utilisateur.
     * 
     * @param email L'adresse e-mail de l'utilisateur.
     * @param hashedPassword Le nouveau mot de passe haché.
     * @return true si la mise à jour a réussi, false sinon.
     */
    public boolean updatePassword(String email, String hashedPassword) {
        boolean success = false;
        try (Connection connection = getConnection()) {
            String updateQuery = "UPDATE inscription SET password = ? WHERE Email = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(updateQuery)) {
                preparedStatement.setString(1, hashedPassword);
                preparedStatement.setString(2, email);
                preparedStatement.executeUpdate();
                success = true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            success = false; 
        }
        return success;
    }

    /**
     * Méthode pour supprimer un utilisateur.
     * 
     * @param id L'identifiant de l'utilisateur à supprimer.
     */
    public void deleteUser(int id) {
        try (Connection conn = getConnection();
             PreparedStatement statement = conn.prepareStatement(DELETE_USER)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode pour obtenir tous les utilisateurs.
     * 
     * @return Une liste d'objets User représentant tous les utilisateurs.
     */
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        try (Connection conn = getConnection();
             PreparedStatement statement = conn.prepareStatement(SELECT_ALL_USERS)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = extractUserFromResultSet(resultSet);
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    /**
     * Méthode utilitaire pour extraire un utilisateur d'un ResultSet.
     * 
     * @param resultSet Le ResultSet contenant les données de l'utilisateur.
     * @return Un objet User correspondant aux données du ResultSet.
     * @throws SQLException Si une erreur SQL survient.
     */
    private User extractUserFromResultSet(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("id"));
        user.setNom(resultSet.getString("nom"));
        user.setPrenom(resultSet.getString("prenom"));
        user.setPassword(resultSet.getString("password"));
        user.setEmail(resultSet.getString("email"));
        user.setUserType(resultSet.getString("userType"));
        return user;
    }

    /**
     * Méthode pour mettre à jour les tentatives de connexion échouées.
     * 
     * @param failedAttempts Le nombre de tentatives échouées.
     * @param email L'adresse e-mail de l'utilisateur.
     */
    public void updateFailedAttempts(int failedAttempts, String email) {
        String sql = "UPDATE inscription SET failed_attempts = ?, last_failed_attempt = NOW() WHERE Email = ?";
        try (Connection conn = getConnection();
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, failedAttempts);
            statement.setString(2, email);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode pour réinitialiser les tentatives de connexion échouées.
     * 
     * @param email L'adresse e-mail de l'utilisateur.
     */
    public void resetFailedAttempts(String email) {
        String sql = "UPDATE inscription SET failed_attempts = 0, last_failed_attempt = NULL WHERE Email = ?";
        try (Connection conn = getConnection();
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setString(1, email);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode pour obtenir un utilisateur par email.
     * 
     * @param email L'adresse e-mail de l'utilisateur.
     * @return Un objet User correspondant à l'adresse e-mail spécifiée.
     */
    public User getUserByEmail(String email) {
        User user = null;

        try (Connection conn = getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(SELECT_USER_BY_EMAIL)) {

            preparedStatement.setString(1, email);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                int id = rs.getInt("Inscription_ID");
                String userEmail = rs.getString("Email");
                String password = rs.getString("password");
                String userType = rs.getString("Type");
                int failedAttempts = rs.getInt("failed_attempts");
                java.sql.Timestamp lastFailedAttempt = rs.getTimestamp("last_failed_attempt");

                user = new User();
                user.setId(id);
                user.setEmail(userEmail);
                user.setPassword(password);
                user.setUserType(userType);
                user.setFailedAttempts(failedAttempts);
                user.setLastFailedAttempt(lastFailedAttempt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    /**
     * Méthode pour mettre à jour la photo de profil d'un utilisateur.
     * 
     * @param userId L'identifiant de l'utilisateur.
     * @param photoProfil La nouvelle photo de profil sous forme de tableau d'octets.
     */
    public void updatePhotoProfil(int userId, byte[] photoProfil) {
        String requete = "UPDATE Inscription SET PhotoProfil = ? WHERE Inscription_ID = ?";
        try (Connection conn = getConnection();
             PreparedStatement statement = conn.prepareStatement(requete)) {
            statement.setBytes(1, photoProfil);
            statement.setInt(2, userId);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static final String SELECT_VALIDATION_STATUS = "SELECT valider FROM inscription WHERE Inscription_ID = ?";
    private static final String UPDATE_VALIDATION_STATUS_SQL = "UPDATE inscription SET valider = ? WHERE Inscription_ID = ?";

    /**
     * Méthode pour vérifier si c'est la première connexion de l'utilisateur.
     * 
     * @param userId L'identifiant de l'utilisateur.
     * @return true si c'est la première connexion, false sinon.
     * @throws SQLException Si une erreur SQL survient.
     */
    public boolean isFirstLogin(int userId) throws SQLException {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_VALIDATION_STATUS)) {
            preparedStatement.setInt(1, userId);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return rs.getInt("valider") == 1;
            }
        }
        return false;
    }

    /**
     * Méthode pour mettre à jour le statut de validation.
     * 
     * @param userId L'identifiant de l'utilisateur.
     * @param status Le nouveau statut de validation.
     * @throws SQLException Si une erreur SQL survient.
     */
    public void updateValidationStatus(int userId, int status) throws SQLException {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_VALIDATION_STATUS_SQL)) {
            preparedStatement.setInt(1, status);
            preparedStatement.setInt(2, userId);
            preparedStatement.executeUpdate();
        }
    }
}
