package dao;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*; 
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import model.*;

/**
 * SpaceDAO est une classe qui gère les opérations de base de données pour les espaces.
 * 
 * Cette classe fournit des méthodes pour créer, lire, mettre à jour et supprimer des enregistrements d'espaces
 * dans la base de données. Elle utilise les connexions fournies par ConnectionDAO pour interagir avec la base de données.
 * 
 * @version 1.0
 */
public class SpaceDAO {
    private String url;
    private String user;
    private String password;

    /**
     * Constructeur de la classe SpaceDAO.
     * Charge les paramètres de connexion depuis un fichier de configuration et assure
     * que le driver JDBC est chargé.
     */
    public SpaceDAO() {
        Properties prop = new Properties();
        try {
            // Charger le fichier de propriétés depuis les ressources
            InputStream inputStream = ClubSportifDAO.class.getClassLoader().getResourceAsStream("config.properties");
            if (inputStream != null) {
                prop.load(inputStream);
                url = prop.getProperty("db.url");
                user = prop.getProperty("db.username");
                password = prop.getProperty("db.password");
            } else {
                System.err.println("Le fichier config.properties n'a pas pu être chargé.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // Assurez-vous que le driver JDBC de MySQL est chargé
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    // Méthodes pour accéder aux informations de connexion

    /**
     * Retourne l'URL de connexion à la base de données.
     * 
     * @return URL de connexion.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Retourne le nom d'utilisateur pour la connexion à la base de données.
     * 
     * @return Nom d'utilisateur.
     */
    public String getUser() {
        return user;
    }

    /**
     * Retourne le mot de passe pour la connexion à la base de données.
     * 
     * @return Mot de passe.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Méthode pour récupérer la liste de tous les espaces.
     * 
     * @param idU L'identifiant de l'utilisateur (0 pour tous les utilisateurs).
     * @return Une liste d'objets Space.
     * @throws SQLException Si une erreur SQL survient.
     */
    public List<Space> listAllSpaces(int idU) throws SQLException {
        List<Space> listSpace = new ArrayList<>();
        String sql;
        if(idU == 0) {
            sql = "SELECT space_id, name, Commune, description, E.Federation, inscription.Nom, inscription.Prenom " +
                  "FROM spaces " +
                  "INNER JOIN inscription ON user_id = Inscription_ID " +
                  "INNER JOIN etablissement E ON inscription.Etablissement_ID = E.Etablissement_ID ";
        } else {
            sql = "SELECT space_id, name, Commune, description, E.Federation, inscription.Nom, inscription.Prenom " +
                  "FROM spaces " +
                  "INNER JOIN inscription ON user_id = Inscription_ID " +
                  "INNER JOIN etablissement E ON inscription.Etablissement_ID = E.Etablissement_ID " +
                  "WHERE inscription.Inscription_ID = ?";
        }

        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = conn.prepareStatement(sql)) {
            if(idU != 0 ) {
                statement.setInt(1, idU);
            }

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    int id = resultSet.getInt("space_id");
                    String name = resultSet.getString("name");
                    String description = resultSet.getString("description");
                    String nom = resultSet.getString("Nom");
                    String prenom = resultSet.getString("Prenom");
                    String ville = resultSet.getString("Commune");
                    String federation = resultSet.getString("Federation");
                    Space space = new Space(id, name, description, nom, prenom, federation, ville);
                    listSpace.add(space);
                }
            }
        } // Le bloc try-with-resources s'occupe de fermer resultSet, statement, et conn.
        return listSpace;
    }

    /**
     * Méthode pour récupérer la liste des espaces auxquels un utilisateur est abonné.
     * 
     * @param userId L'identifiant de l'utilisateur.
     * @return Une liste d'objets Space.
     * @throws SQLException Si une erreur SQL survient.
     */
    public List<Space> getUserSubscriptions(int userId) throws SQLException {
        List<Space> subscribedSpaces = new ArrayList<>();
        String query = "SELECT s.space_id, s.name, Commune, s.description, e.Federation, i.Nom, i.Prenom " +
                       "FROM spaces s " +
                       "INNER JOIN subscriptions sub ON s.space_id = sub.space_id " +
                       "INNER JOIN inscription i ON s.user_id = i.Inscription_ID " +
                       "INNER JOIN etablissement e ON i.Etablissement_ID = e.Etablissement_ID " +
                       "WHERE sub.user_id = ?";
        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setInt(1, userId);
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("space_id");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                String nom = resultSet.getString("Nom");
                String prenom = resultSet.getString("Prenom");
                String ville = resultSet.getString("Commune");
                String federation = resultSet.getString("Federation");
                Space space = new Space(id, name, description, nom, prenom, federation, ville);
                subscribedSpaces.add(space);
            }
        }
        return subscribedSpaces;
    }

    /**
     * Méthode pour insérer un nouvel espace.
     * 
     * @param userId L'identifiant de l'utilisateur.
     * @param name Le nom de l'espace.
     * @param description La description de l'espace.
     * @return true si l'insertion a réussi, false sinon.
     * @throws SQLException Si une erreur SQL survient.
     */
    public boolean insertSpace(int userId, String name, String description) throws SQLException {
        String sql = "INSERT INTO spaces (user_id, name, description) VALUES (?, ?, ?)";
        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, userId);
            statement.setString(2, name);
            statement.setString(3, description);
            int rowsInserted = statement.executeUpdate();
            return rowsInserted > 0;
        }
    }

    /**
     * Méthode pour récupérer un espace par son identifiant.
     * 
     * @param spaceId L'identifiant de l'espace.
     * @return Un objet Space correspondant à l'identifiant spécifié.
     * @throws SQLException Si une erreur SQL survient.
     */
    public Space getSpaceById(int spaceId) throws SQLException {
        Space space = null;
        String sql = "SELECT space_id, name, description FROM spaces WHERE space_id = ?";
        
        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, spaceId);
            
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    space = new Space(rs.getInt("space_id"), rs.getString("name"), rs.getString("description"));
                }
            }
        }
        return space;
    }

    /**
     * Méthode pour insérer un nouveau post dans un espace.
     * 
     * @param post Un objet Post contenant les détails du post à insérer.
     * @return true si l'insertion a réussi, false sinon.
     */
    public boolean insertPost(Post post) {
        String sql = "INSERT INTO posts (space_id, title, content, photo) VALUES (?,?, ?, ?)";
        boolean rowInserted = false;
        
        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, post.getSpaceId());
            statement.setString(2, post.getTitle());
            statement.setString(3, post.getContent());
            
            if (post.getPhoto() != null) {
                statement.setBlob(4, new javax.sql.rowset.serial.SerialBlob(post.getPhoto()));
            } else {
                statement.setNull(4, java.sql.Types.BLOB);
            }
            
            rowInserted = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowInserted;
    }

    /**
     * Méthode pour récupérer la liste des posts d'un espace par l'identifiant de l'espace.
     * 
     * @param spaceId L'identifiant de l'espace.
     * @return Une liste d'objets Post.
     */
    public List<Post> getPostsBySpaceId(int spaceId) {
        List<Post> posts = new ArrayList<>();
        String sql = "SELECT * FROM posts WHERE space_id = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, spaceId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Post post = new Post();
                    post.setPostId(resultSet.getInt("post_id"));
                    post.setTitle(resultSet.getString("title"));
                    post.setContent(resultSet.getString("content"));
                    post.setPhoto(resultSet.getBytes("photo")); // Assurez-vous que votre modèle Post peut gérer les données binaires pour les photos
                    post.setCreatedAt(resultSet.getTimestamp("created_at"));
                    post.setUpdatedAt(resultSet.getTimestamp("updated_at"));
                    post.setLikes(getLikesCount(resultSet.getInt("post_id")));
                    post.setComments(getCommentsByPostId(resultSet.getInt("post_id")));
                    posts.add(post);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Gérer l'exception de manière plus appropriée selon les besoins de votre application
        }
        return posts;
    }

    /**
     * Méthode pour supprimer un post par son identifiant.
     * 
     * @param postId L'identifiant du post.
     * @return true si la suppression a réussi, false sinon.
     */
    public boolean deletePost(int postId) {
        boolean rowDeleted = false;
        String sql = "DELETE FROM posts WHERE post_id = ?";
        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, postId);
            rowDeleted = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowDeleted;
    }

    /**
     * Méthode pour récupérer un post par son identifiant.
     * 
     * @param postId L'identifiant du post.
     * @return Un objet Post correspondant à l'identifiant spécifié.
     */
    public Post getPostById(int postId) {
        Post post = null;
        String sql = "SELECT * FROM posts WHERE post_id = ?";
        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, postId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                post = new Post();
                post.setPostId(resultSet.getInt("post_id"));
                post.setTitle(resultSet.getString("title"));
                post.setContent(resultSet.getString("content"));
                post.setPhoto(resultSet.getBytes("photo"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return post;
    }

    /**
     * Méthode pour mettre à jour un post.
     * 
     * @param post Un objet Post contenant les détails mis à jour du post.
     * @return true si la mise à jour a réussi, false sinon.
     */
    public boolean updatePost(Post post) {
        String sql = "UPDATE posts SET title = ?, content = ?, photo = ? WHERE post_id = ?";
        boolean rowUpdated = false;

        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = conn.prepareStatement(sql)) {

            statement.setString(1, post.getTitle());
            statement.setString(2, post.getContent());

            if (post.getPhoto() != null) {
                statement.setBlob(3, new javax.sql.rowset.serial.SerialBlob(post.getPhoto()));
            } else {
                statement.setNull(3, java.sql.Types.BLOB);
            }

            statement.setInt(4, post.getPostId());

            rowUpdated = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace(); // Gérer l'exception de manière appropriée selon les besoins de votre application
        }

        return rowUpdated;
    }

    /**
     * Méthode pour s'abonner à un espace.
     * 
     * @param userId L'identifiant de l'utilisateur.
     * @param spaceId L'identifiant de l'espace.
     * @throws SQLException Si une erreur SQL survient.
     */
    public void subscribe(int userId, int spaceId) throws SQLException {
        String query = "INSERT INTO subscriptions (user_id, space_id) VALUES (?, ?)";
        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setInt(1, userId);
            stmt.setInt(2, spaceId);
            stmt.executeUpdate();
        }
    }

    /**
     * Méthode pour aimer un post.
     * 
     * @param userId L'identifiant de l'utilisateur.
     * @param postId L'identifiant du post.
     * @throws SQLException Si une erreur SQL survient.
     */
    public void likePost(int userId, int postId) throws SQLException {
        String query = "INSERT INTO likes (user_id, post_id) VALUES (?, ?)";
        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setInt(1, userId);
            stmt.setInt(2, postId);
            stmt.executeUpdate();
        }
    }

    /**
     * Méthode pour ajouter un commentaire à un post.
     * 
     * @param userId L'identifiant de l'utilisateur.
     * @param postId L'identifiant du post.
     * @param content Le contenu du commentaire.
     * @throws SQLException Si une erreur SQL survient.
     */
    public void addComment(int userId, int postId, String content) throws SQLException {
        String query = "INSERT INTO comments (user_id, post_id, content) VALUES (?, ?, ?)";
        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setInt(1, userId);
            stmt.setInt(2, postId);
            stmt.setString(3, content);
            stmt.executeUpdate();
        }
    }

    /**
     * Méthode pour obtenir le nombre de likes d'un post.
     * 
     * @param postId L'identifiant du post.
     * @return Le nombre de likes du post.
     * @throws SQLException Si une erreur SQL survient.
     */
    private int getLikesCount(int postId) throws SQLException {
        String query = "SELECT COUNT(*) AS like_count FROM likes WHERE post_id = ?";
        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setInt(1, postId);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getInt("like_count");
            }
        }
        return 0;
    }

    /**
     * Méthode pour obtenir la liste des commentaires d'un post par l'identifiant du post.
     * 
     * @param postId L'identifiant du post.
     * @return Une liste d'objets Comment.
     * @throws SQLException Si une erreur SQL survient.
     */
    private List<Comment> getCommentsByPostId(int postId) throws SQLException {
        List<Comment> comments = new ArrayList<>();
        String query = "SELECT c.id, c.post_id, c.user_id, Nom, c.content, c.created_at " +
                       "FROM comments c " +
                       "INNER JOIN inscription u ON c.user_id = u.Inscription_ID " +
                       "WHERE c.post_id = ?";
        
        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setInt(1, postId);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Comment comment = new Comment();
                comment.setCommentId(rs.getInt("id"));
                comment.setPostId(rs.getInt("post_id"));
                comment.setUserId(rs.getInt("user_id"));
                comment.setUsername(rs.getString("Nom"));
                comment.setContent(rs.getString("content"));
                comment.setCreatedAt(rs.getTimestamp("created_at"));
                comments.add(comment);
            }
        }
        return comments;
    }

    /**
     * Méthode pour vérifier si un utilisateur a aimé un post.
     * 
     * @param userId L'identifiant de l'utilisateur.
     * @param postId L'identifiant du post.
     * @return true si l'utilisateur a aimé le post, false sinon.
     * @throws SQLException Si une erreur SQL survient.
     */
    public boolean hasLiked(int userId, int postId) throws SQLException {
        String query = "SELECT COUNT(*) FROM likes WHERE user_id = ? AND post_id = ?";
        try (Connection conn = DriverManager.getConnection(url, user, password);
             PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setInt(1, userId);
            stmt.setInt(2, postId);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        }
        return false;
    }

    /**
     * Méthode pour se désabonner d'un espace.
     * 
     * @param userId L'identifiant de l'utilisateur.
     * @param spaceId L'identifiant de l'espace.
     * @throws SQLException Si une erreur SQL survient.
     */
    public void unsubscribe(int userId, int spaceId) throws SQLException {
        String query = "DELETE FROM subscriptions WHERE user_id = ? AND space_id = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, userId);
            statement.setInt(2, spaceId);
            statement.executeUpdate();
        }
    }

    /**
     * Méthode pour supprimer un espace par son identifiant.
     * 
     * @param spaceId L'identifiant de l'espace.
     * @throws SQLException Si une erreur SQL survient.
     */
    public void deleteSpace(int spaceId) throws SQLException {
        String deleteSQL = "DELETE FROM spaces WHERE space_id = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(deleteSQL)) {
            preparedStatement.setInt(1, spaceId);
            preparedStatement.executeUpdate();
        }
    }
}
