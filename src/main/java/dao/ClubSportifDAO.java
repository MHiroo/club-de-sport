package dao;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.sql.*; 
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import model.ClubSportif;

/**
 * ClubSportifDAO est une classe qui gère les opérations de base de données pour les entités de club sportif.
 * 
 * Cette classe fournit des méthodes pour créer, lire, mettre à jour et supprimer des enregistrements de clubs sportifs 
 * dans la base de données. Elle utilise les connexions fournies par ConnectionDAO pour interagir avec la base de données.
 * 
 * Méthodes disponibles :
 * - ajouterClubSportif(ClubSportif club) : Ajoute un nouveau club sportif à la base de données.
 * - obtenirClubSportif(int id) : Récupère un club sportif en fonction de son identifiant.
 * - mettreAJourClubSportif(ClubSportif club) : Met à jour les informations d'un club sportif existant.
 * - supprimerClubSportif(int id) : Supprime un club sportif de la base de données en fonction de son identifiant.
 * - listerTousLesClubs() : Récupère une liste de tous les clubs sportifs enregistrés dans la base de données.
 * 
 * @author OKOTCHE Dorothée 
 * @version 1.0
 */
public class ClubSportifDAO {
    private String url;
    private String user;
    private String password;

    /**
     * Constructeur de la classe ClubSportifDAO.
     * Initialise la connexion à la base de données en chargeant les propriétés depuis un fichier de configuration.
     */
    public ClubSportifDAO() {
        Properties prop = new Properties();
        try {
            // Charger le fichier de propriétés depuis les ressources
            InputStream inputStream = ClubSportifDAO.class.getClassLoader().getResourceAsStream("config.properties");
            if (inputStream != null) {
                prop.load(inputStream);
                url = prop.getProperty("db.url");
                user = prop.getProperty("db.username");
                password = prop.getProperty("db.password");
            } else {
                System.err.println("Le fichier config.properties n'a pas pu être chargé.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // Assurez-vous que le driver JDBC de MySQL est chargé
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    // Méthodes pour accéder aux informations de connexion

    /**
     * Retourne l'URL de connexion à la base de données.
     * 
     * @return URL de connexion.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Retourne le nom d'utilisateur pour la connexion à la base de données.
     * 
     * @return Nom d'utilisateur.
     */
    public String getUser() {
        return user;
    }

    /**
     * Retourne le mot de passe pour la connexion à la base de données.
     * 
     * @return Mot de passe.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Récupère une liste de clubs sportifs filtrée par fédération, code postal et région.
     * 
     * @param federation La fédération du club sportif.
     * @param codePostal Le code postal du club sportif.
     * @param region La région du club sportif.
     * @return Liste de clubs sportifs filtrée.
     */
    public List<ClubSportif> getClubsFiltered(String federation, String codePostal, String region) {
        List<ClubSportif> clubs = new ArrayList<>();
        if ((codePostal == null && region == null) || (codePostal.equals("") && region.equals(""))) {
            return clubs;
        }
        // La requête de base sélectionne jusqu'à 100 lignes
        String baseQuery = "SELECT DISTINCT E.*, C.Longitude, C.Latitude, C.Zip_code FROM etablissement E INNER JOIN codecommunecoordonnee C ON E.Code_Commune = C.Insee_code LIMIT 100";

        // Construire la requête en fonction des filtres fournis
        ArrayList<String> conditions = new ArrayList<>();
        if (federation != null && !federation.isEmpty()) conditions.add("E.Federation = ?");
        if (codePostal != null && !codePostal.isEmpty()) conditions.add("C.Zip_code = ?");
        if (region != null && !region.isEmpty()) conditions.add("E.Region = ?");

        // Si des conditions de filtrage sont fournies, les ajouter à la requête
        if (!conditions.isEmpty()) {
            String whereClause = String.join(" AND ", conditions);
            // Remplacer "LIMIT 100" par " " pour éviter de limiter la requête filtrée
            baseQuery = baseQuery.replace("LIMIT 100", " ") + " WHERE " + whereClause;
        }

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(baseQuery)) {

            int index = 1;
            if (federation != null && !federation.isEmpty()) preparedStatement.setString(index++, federation);
            if (codePostal != null && !codePostal.isEmpty()) preparedStatement.setString(index++, codePostal);
            if (region != null && !region.isEmpty()) preparedStatement.setString(index, region);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    String zipCode = resultSet.getString("Zip_code");
                    // Vérifier si le code postal n'est pas null avant d'ajouter le club à la liste
                    if (zipCode != null) {
                        ClubSportif club = new ClubSportif();
                        club.setEtablissementId(resultSet.getInt("Etablissement_ID"));
                        club.setCommune(resultSet.getString("Commune"));
                        club.setDepartement(resultSet.getString("Departement"));
                        club.setRegion(resultSet.getString("Region"));
                        club.setFederation(resultSet.getString("Federation"));
                        club.setTotal(resultSet.getInt("Total"));
                        club.setCodePostal(resultSet.getString("Zip_code"));
                        club.setLongitude(resultSet.getFloat("Longitude"));
                        club.setLatitude(resultSet.getFloat("Latitude"));
                        clubs.add(club);
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println("Erreur lors de la récupération des clubs sportifs : " + e.getMessage());
            e.printStackTrace();
        }
        return clubs;
    }

    /**
     * Récupère une liste unique de fédérations à partir de la base de données.
     * 
     * @return Liste de fédérations uniques.
     */
    public List<String> getUniqueFederations() {
        List<String> federations = new ArrayList<>();
        String query = "SELECT DISTINCT Federation FROM etablissement ORDER BY Federation";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {

            while (resultSet.next()) {
                federations.add(resultSet.getString("Federation"));
            }
        } catch (SQLException e) {
            System.err.println("Erreur lors de la récupération des fédérations uniques : " + e.getMessage());
            e.printStackTrace();
        }
        return federations;
    }

    /**
     * Récupère une liste de clubs sportifs filtrée par fédération et code postal.
     * 
     * @param federation La fédération du club sportif.
     * @param codePostal Le code postal du club sportif.
     * @return Liste de clubs sportifs filtrée.
     */
    public List<ClubSportif> getClubsByFederationAndCodePostal(String federation, String codePostal) {
        List<ClubSportif> clubs = new ArrayList<>();
        String query = "SELECT * FROM etablissement E INNER JOIN codecommunecoordonnee C ON E.Code_Commune = C.Insee_code  WHERE E.Federation = ? AND C.Zip_code = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setString(1, federation);
            preparedStatement.setString(2, codePostal);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                ClubSportif club = new ClubSportif();
                club.setEtablissementId(resultSet.getInt("Etablissement_ID"));
                club.setCommune(resultSet.getString("Commune"));
                club.setDepartement(resultSet.getString("Departement"));
                club.setRegion(resultSet.getString("Region"));
                club.setFederation(resultSet.getString("Federation"));
                club.setTotal(resultSet.getInt("Total"));
                club.setCodePostal(resultSet.getString("Zip_code"));
                club.setLongitude(resultSet.getFloat("Longitude"));
                club.setLatitude(resultSet.getFloat("Latitude"));
                clubs.add(club);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clubs;
    }

    /**
     * Récupère une liste unique de régions à partir de la base de données.
     * 
     * @return Liste de régions uniques.
     */
    public List<String> getUniqueRegions() {
        List<String> regions = new ArrayList<>();
        String sql = "SELECT DISTINCT Region FROM etablissement ORDER BY Region";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                regions.add(resultSet.getString("Region"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return regions;
    }

    /**
     * Récupère l'identifiant d'un établissement en fonction de la fédération et du code postal.
     * 
     * @param federation La fédération du club sportif.
     * @param codePostal Le code postal du club sportif.
     * @return L'identifiant de l'établissement.
     */
    public int getEtablissementIdByPostalCodeAndFederation(String federation, String codePostal) {
        int id = 0;
        String query = "SELECT Etablissement_ID FROM etablissement E INNER JOIN codecommunecoordonnee C ON E.Code_Commune = C.Insee_code  WHERE E.Federation = ? AND C.Zip_code = ? LIMIT 1";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            preparedStatement.setString(1, federation);
            preparedStatement.setString(2, codePostal);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                id = resultSet.getInt("Etablissement_ID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }
}

