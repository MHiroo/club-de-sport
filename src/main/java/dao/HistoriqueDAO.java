package dao;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import model.Historique;

/**
 * HistoriqueDAO est une classe qui gère les opérations de base de données pour les entités d'historique.
 * 
 * Cette classe fournit des méthodes pour créer, lire, mettre à jour et supprimer des enregistrements d'historique
 * dans la base de données. Elle utilise les connexions fournies par ConnectionDAO pour interagir avec la base de données.
 * 
 * Méthodes disponibles :
 * - ajouterHistorique(Historique historique) : Ajoute un nouvel historique à la base de données.
 * - obtenirHistorique(int id) : Récupère un historique en fonction de son identifiant.
 * - mettreAJourHistorique(Historique historique) : Met à jour les informations d'un historique existant.
 * - supprimerHistorique(int id) : Supprime un historique de la base de données en fonction de son identifiant.
 * - listerTousLesHistoriques() : Récupère une liste de tous les historiques enregistrés dans la base de données.
 * 
 * 
 * @version 1.0
 */
public class HistoriqueDAO {
     // Informations de connexion à la base de données
    private static String url;
    private static String user;
    private static String password;

    /**
     * Constructeur de la classe HistoriqueDAO.
     * Charge les paramètres de connexion depuis un fichier de configuration et assure
     * que le driver JDBC est chargé.
     */
    public HistoriqueDAO() {
        Properties prop = new Properties();
        try {
            // Charger le fichier de propriétés depuis les ressources
            InputStream inputStream = LicenceDAO.class.getClassLoader().getResourceAsStream("config.properties");
            if (inputStream != null) {
                prop.load(inputStream);
                url = prop.getProperty("db.url");
                user = prop.getProperty("db.username");
                password = prop.getProperty("db.password");
            } else {
                System.err.println("Le fichier config.properties n'a pas pu être chargé.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // Assurez-vous que le driver JDBC de MySQL est chargé
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode pour établir la connexion à la base de données.
     * 
     * @return Une connexion à la base de données.
     * @throws SQLException Si une erreur SQL survient.
     */
    public Connection getConnection() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            // Gestion de l'erreur ici. Par exemple, en la journalisant et en lançant une exception personnalisée
            e.printStackTrace();
        }
        return DriverManager.getConnection(url, user, password);
    }

    /**
     * Méthode pour enregistrer une connexion dans la base de données.
     * 
     * @param historique L'historique de connexion à enregistrer.
     * @throws SQLException Si une erreur SQL survient.
     */
    public static void enregistrerConnexion(Historique historique) throws SQLException {
        // Requête SQL d'insertion
        String requete = "INSERT INTO historique (id, date_connexion, Adresse_ip, Navigateur, id_users , statut_connexion) VALUES (?, ?, ?, ?, ?, ?)";
        // Préparation de la requête avec des paramètres
        try (Connection connexionBD = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connexionBD.prepareStatement(requete)) {
            // Attribution des valeurs aux paramètres de la requête
            preparedStatement.setInt(1, historique.getId());
            preparedStatement.setTimestamp(2, new java.sql.Timestamp(historique.getDateConnexion().getTime()));
            preparedStatement.setString(3, historique.getAdresseIP());
            preparedStatement.setString(4, historique.getNavigateur());
            preparedStatement.setInt(5, historique.getId_users());
            preparedStatement.setString(6, historique.getStatut());
            // Exécution de la requête
            preparedStatement.executeUpdate();
        }
    }
}
