package dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import model.ClubSportif;
import model.Licence;

/**
 * LicenceDAO est une classe qui gère les opérations de base de données pour les entités de licence.
 * 
 * Cette classe fournit des méthodes pour créer, lire, mettre à jour et supprimer des enregistrements de licences
 * dans la base de données. Elle utilise les connexions fournies par ConnectionDAO pour interagir avec la base de données.
 * 
 * Méthodes disponibles :
 * - ajouterLicence(Licence licence) : Ajoute une nouvelle licence à la base de données.
 * - obtenirLicence(int id) : Récupère une licence en fonction de son identifiant.
 * - mettreAJourLicence(Licence licence) : Met à jour les informations d'une licence existante.
 * - supprimerLicence(int id) : Supprime une licence de la base de données en fonction de son identifiant.
 * - listerToutesLesLicences() : Récupère une liste de toutes les licences enregistrées dans la base de données.
 * 
 * @author MIZUNO Hiroo
 * @version 1.0
 */
public class LicenceDAO {
    private String url;
    private String user;
    private String password;

    /**
     * Constructeur de la classe LicenceDAO.
     * Charge les paramètres de connexion depuis un fichier de configuration et assure
     * que le driver JDBC est chargé.
     */
    public LicenceDAO() {
        Properties prop = new Properties();
        try {
            // Charger le fichier de propriétés depuis les ressources
            InputStream inputStream = LicenceDAO.class.getClassLoader().getResourceAsStream("config.properties");
            if (inputStream != null) {
                prop.load(inputStream);
                url = prop.getProperty("db.url");
                user = prop.getProperty("db.username");
                password = prop.getProperty("db.password");
            } else {
                System.err.println("Le fichier config.properties n'a pas pu être chargé.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // Assurez-vous que le driver JDBC de MySQL est chargé
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode pour récupérer la liste de licenciés par fédération et localisation géographique (Commune/Departement/Region).
     * 
     * @param federation La fédération du licencié.
     * @param localisation La localisation géographique (Commune, Département ou Région).
     * @param localisationType Le type de localisation (commune, departement, region).
     * @return Une liste d'objets Licence correspondant aux critères spécifiés.
     */
    public ArrayList<Licence> getLicencesByParameters(String federation, String localisation, String localisationType) {
        // Vérifier si la fédération spécifiée existe dans la base de données
        if (!federation.isEmpty() && !isFederationExists(federation)) {
            // La fédération spécifiée n'existe pas, retourner une liste vide
            return new ArrayList<>();
        }
        // Vérifier si la commune spécifiée existe dans la base de données
        if (!localisation.isEmpty() && localisationType.equalsIgnoreCase("commune") && !isCommuneExists(localisation)) {
            // La commune spécifiée n'existe pas, retourner une liste vide
            return new ArrayList<>();
        }
        // Vérifier si les 2 paramètres sont vides
        if (federation.isEmpty() && localisation.isEmpty()) {
            // Si les 2 paramètres sont vides, retourner null
            return null;
        }

        String query = "SELECT H_1_4_ans, H_5_9_ans, H_10_14_ans, H_15_19_ans, H_20_24_ans, H_25_29_ans, H_30_34_ans, H_35_39_ans, H_40_44_ans, H_45_49_ans, H_50_54_ans, H_55_59_ans, H_60_64_ans, H_65_69_ans, H_70_74_ans, H_75_79_ans, H_80_99_ans, H_NR, F_1_4_ans, F_5_9_ans, F_10_14_ans, F_15_19_ans, F_20_24_ans, F_25_29_ans, F_30_34_ans, F_35_39_ans, F_40_44_ans, F_45_49_ans, F_50_54_ans, F_55_59_ans, F_60_64_ans, F_65_69_ans, F_70_74_ans, F_75_79_ans, F_80_99_ans, F_NR, Total FROM licence WHERE Commune IS NOT NULL AND Commune != ''";

        // Ajouter les conditions en fonction des paramètres non vides
        if (!federation.isEmpty()) {
            query += " AND Federation = ? ";
        }
        if (!localisation.isEmpty()) {
            if (localisationType.equalsIgnoreCase("commune")) {
                query += " AND Commune = ? ";
            } else if (localisationType.equalsIgnoreCase("departement")) {
                query += " AND Departement = ? ";
            } else if (localisationType.equalsIgnoreCase("region")) {
                query += " AND Region = ? ";
            }
        }

        // Variable pour stocker le nombre de licenciés
        ArrayList<Licence> licenceList = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            // Index pour les paramètres de la requête préparée
            int index = 1;

            // Ajouter les valeurs des paramètres non vides
            if (!federation.isEmpty()) {
                preparedStatement.setString(index++, federation);
            }
            if (!localisation.isEmpty()) {
                preparedStatement.setString(index++, localisation);
            }

            // Exécuter la requête et récupérer le résultat
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                // Parcours des résultats
                while (resultSet.next()) {
                    // Création d'un objet Licence à partir des données de la ligne courante
                    Licence licence = new Licence(
                        resultSet.getInt("F_1_4_ans"),
                        resultSet.getInt("F_5_9_ans"),
                        resultSet.getInt("F_10_14_ans"),
                        resultSet.getInt("F_15_19_ans"),
                        resultSet.getInt("F_20_24_ans"),
                        resultSet.getInt("F_25_29_ans"),
                        resultSet.getInt("F_30_34_ans"),
                        resultSet.getInt("F_35_39_ans"),
                        resultSet.getInt("F_40_44_ans"),
                        resultSet.getInt("F_45_49_ans"),
                        resultSet.getInt("F_50_54_ans"),
                        resultSet.getInt("F_55_59_ans"),
                        resultSet.getInt("F_60_64_ans"),
                        resultSet.getInt("F_65_69_ans"),
                        resultSet.getInt("F_70_74_ans"),
                        resultSet.getInt("F_75_79_ans"),
                        resultSet.getInt("F_80_99_ans"),
                        resultSet.getInt("F_NR"),
                        resultSet.getInt("H_1_4_ans"),
                        resultSet.getInt("H_5_9_ans"),
                        resultSet.getInt("H_10_14_ans"),
                        resultSet.getInt("H_15_19_ans"),
                        resultSet.getInt("H_20_24_ans"),
                        resultSet.getInt("H_25_29_ans"),
                        resultSet.getInt("H_30_34_ans"),
                        resultSet.getInt("H_35_39_ans"),
                        resultSet.getInt("H_40_44_ans"),
                        resultSet.getInt("H_45_49_ans"),
                        resultSet.getInt("H_50_54_ans"),
                        resultSet.getInt("H_55_59_ans"),
                        resultSet.getInt("H_60_64_ans"),
                        resultSet.getInt("H_65_69_ans"),
                        resultSet.getInt("H_70_74_ans"),
                        resultSet.getInt("H_75_79_ans"),
                        resultSet.getInt("H_80_99_ans"),
                        resultSet.getInt("H_NR"),
                        resultSet.getInt("Total")
                    );

                    // Ajouter l'objet Licence à la liste
                    licenceList.add(licence);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return licenceList;
    }

    /**
     * Méthode pour récupérer la liste de licenciés par fédération et localisation géographique (Commune/Departement/Region) avec pagination.
     * 
     * @param federation La fédération du licencié.
     * @param localisation La localisation géographique (Commune, Département ou Région).
     * @param localisationType Le type de localisation (commune, departement, region).
     * @param offset Le décalage pour la pagination.
     * @param limit La limite de résultats à retourner.
     * @param orderBy La colonne par laquelle trier les résultats.
     * @param orderDirection La direction de tri (ASC ou DESC).
     * @return Une liste d'objets Licence correspondant aux critères spécifiés avec pagination.
     */
    public ArrayList<Licence> getLicencesByParametersOffset(String federation, String localisation, String localisationType, int offset, int limit, String orderBy, String orderDirection) {
        // Vérifier si la fédération spécifiée existe dans la base de données
        if (!federation.isEmpty() && !isFederationExists(federation)) {
            // La fédération spécifiée n'existe pas, retourner une liste vide
            return new ArrayList<>();
        }
        // Vérifier si la commune spécifiée existe dans la base de données
        if (!localisation.isEmpty() && localisationType.equalsIgnoreCase("commune") && !isCommuneExists(localisation)) {
            // La commune spécifiée n'existe pas, retourner une liste vide
            return new ArrayList<>();
        }
        // Vérifier si les 2 paramètres sont vides
        if (federation.isEmpty() && localisation.isEmpty()) {
            // Si les 2 paramètres sont vides, retourner null
            return null;
        }

        String query = "SELECT l.*,d.nom_departement FROM licence l INNER JOIN departement d ON l.Departement = d.code_departement WHERE Commune IS NOT NULL AND Commune != ''";

        // Ajouter les conditions en fonction des paramètres non vides
        if (!federation.isEmpty()) {
            query += " AND Federation = ? ";
        }
        if (!localisation.isEmpty()) {
            if (localisationType.equalsIgnoreCase("commune")) {
                query += " AND Commune = ? ";
            } else if (localisationType.equalsIgnoreCase("departement")) {
                query += " AND Departement = ? ";
            } else if (localisationType.equalsIgnoreCase("region")) {
                query += " AND Region = ? ";
            }
        }

        // Ajouter l'ordre de tri directement dans la chaîne de requête
        query += " ORDER BY " + orderBy + " " + orderDirection + " LIMIT ? OFFSET ?"; // Mettez d'abord LIMIT, puis OFFSET

        // Variable pour stocker le nombre de licenciés
        ArrayList<Licence> licenceList = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            // Index pour les paramètres de la requête préparée
            int index = 1;

            // Ajouter les valeurs des paramètres non vides
            if (!federation.isEmpty()) {
                preparedStatement.setString(index++, federation);
            }
            if (!localisation.isEmpty()) {
                preparedStatement.setString(index++, localisation);
            }

            // Ajouter l'offset et la limite comme paramètres
            preparedStatement.setInt(index++, limit);
            preparedStatement.setInt(index++, Math.max(offset, 0)); // Remplacer l'offset par 0 s'il est négatif

            // Exécuter la requête et récupérer le résultat
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                // Parcours des résultats
                while (resultSet.next()) {
                    // Création d'un objet Licence à partir des données de la ligne courante
                    Licence licence = new Licence(
                        resultSet.getString("Commune"),
                        resultSet.getString("nom_departement") + " (" + resultSet.getString("Departement") + ")",
                        resultSet.getString("Region"),
                        resultSet.getString("Federation"),
                        resultSet.getInt("F_1_4_ans"),
                        resultSet.getInt("F_5_9_ans"),
                        resultSet.getInt("F_10_14_ans"),
                        resultSet.getInt("F_15_19_ans"),
                        resultSet.getInt("F_20_24_ans"),
                        resultSet.getInt("F_25_29_ans"),
                        resultSet.getInt("F_30_34_ans"),
                        resultSet.getInt("F_35_39_ans"),
                        resultSet.getInt("F_40_44_ans"),
                        resultSet.getInt("F_45_49_ans"),
                        resultSet.getInt("F_50_54_ans"),
                        resultSet.getInt("F_55_59_ans"),
                        resultSet.getInt("F_60_64_ans"),
                        resultSet.getInt("F_65_69_ans"),
                        resultSet.getInt("F_70_74_ans"),
                        resultSet.getInt("F_75_79_ans"),
                        resultSet.getInt("F_80_99_ans"),
                        resultSet.getInt("F_NR"),
                        resultSet.getInt("H_1_4_ans"),
                        resultSet.getInt("H_5_9_ans"),
                        resultSet.getInt("H_10_14_ans"),
                        resultSet.getInt("H_15_19_ans"),
                        resultSet.getInt("H_20_24_ans"),
                        resultSet.getInt("H_25_29_ans"),
                        resultSet.getInt("H_30_34_ans"),
                        resultSet.getInt("H_35_39_ans"),
                        resultSet.getInt("H_40_44_ans"),
                        resultSet.getInt("H_45_49_ans"),
                        resultSet.getInt("H_50_54_ans"),
                        resultSet.getInt("H_55_59_ans"),
                        resultSet.getInt("H_60_64_ans"),
                        resultSet.getInt("H_65_69_ans"),
                        resultSet.getInt("H_70_74_ans"),
                        resultSet.getInt("H_75_79_ans"),
                        resultSet.getInt("H_80_99_ans"),
                        resultSet.getInt("H_NR"),
                        resultSet.getInt("NR_NR"),
                        resultSet.getInt("Total")
                    );

                    // Vérifier si le champ Commune n'est pas vide avant d'ajouter à la liste
                    if (licence.getCommune() != null && !licence.getCommune().isEmpty()) {
                        // Ajouter l'objet Licence à la liste
                        licenceList.add(licence);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return licenceList;
    }

    /**
     * Méthode pour récupérer les sommes des licenciés par fédération et localisation géographique.
     * 
     * @param federation La fédération du licencié.
     * @param localisation La localisation géographique (Commune, Département ou Région).
     * @param localisationType Le type de localisation (commune, departement, region).
     * @return Un tableau d'entiers contenant les sommes des licenciés (Total, Hommes, Femmes).
     */
    public int[] getLicencieSums(String federation, String localisation, String localisationType) {
        // Initialiser un tableau de trois valeurs à zéro
        int[] sums = new int[3];

        // Construire la requête SQL pour récupérer les sommes
        String query = "SELECT SUM(Total), SUM(H_1_4_ans + H_5_9_ans + H_10_14_ans + H_15_19_ans + H_20_24_ans + H_25_29_ans + H_30_34_ans + H_35_39_ans + H_40_44_ans + H_45_49_ans + H_50_54_ans + H_55_59_ans + H_60_64_ans + H_65_69_ans + H_70_74_ans + H_75_79_ans + H_80_99_ans + H_NR), " +
            "SUM(F_1_4_ans + F_5_9_ans + F_10_14_ans + F_15_19_ans + F_20_24_ans + F_25_29_ans + F_30_34_ans + F_35_39_ans + F_40_44_ans + F_45_49_ans + F_50_54_ans + F_55_59_ans + F_60_64_ans + F_65_69_ans + F_70_74_ans + F_75_79_ans + F_80_99_ans + F_NR) " +
            "FROM licence WHERE ";

        // Liste pour stocker les conditions à ajouter à la requête SQL
        List<String> conditions = new ArrayList<>();

        // Ajouter les conditions en fonction des paramètres non vides
        if (!federation.isEmpty()) {
            conditions.add("Federation = ?");
        }
        if (!localisation.isEmpty()) {
            if (localisationType.equalsIgnoreCase("commune")) {
                conditions.add("Commune = ?");
            } else if (localisationType.equalsIgnoreCase("departement")) {
                conditions.add("Departement = ?");
            } else if (localisationType.equalsIgnoreCase("region")) {
                conditions.add("Region = ?");
            }
        }

        // Si aucune condition n'a été ajoutée, retourner le tableau de sommes initialisé à zéro
        if (conditions.isEmpty()) {
            return sums;
        }

        // Construire la clause WHERE en fonction des conditions
        query += String.join(" AND ", conditions);

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            // Index pour les paramètres de la requête préparée
            int index = 1;

            // Ajouter les valeurs des paramètres non vides
            if (!federation.isEmpty()) {
                preparedStatement.setString(index++, federation);
            }
            if (!localisation.isEmpty()) {
                preparedStatement.setString(index++, localisation);
            }

            // Exécuter la requête et récupérer les résultats
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    // Récupérer les sommes et les stocker dans le tableau
                    sums[0] = resultSet.getInt(1); // Total
                    sums[1] = resultSet.getInt(2); // Hommes
                    sums[2] = resultSet.getInt(3); // Femmes
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Retourner le tableau de sommes
        return sums;
    }

    /**
     * Méthode pour récupérer une liste unique de régions à partir des licences.
     * 
     * @return Une liste de noms de régions uniques.
     */
    public List<String> getUniqueRegions() {
        List<String> regions = new ArrayList<>();
        String sql = "SELECT DISTINCT Region FROM licence WHERE Commune != '' ";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                regions.add(resultSet.getString("Region"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return regions;
    }

    /**
     * Méthode pour récupérer une liste unique de départements à partir des licences.
     * 
     * @return Une liste de noms de départements uniques.
     */
    public List<String> getUniqueDepartements() {
        List<String> departements = new ArrayList<>();
        String sql = "SELECT DISTINCT d.nom_departement, d.code_departement FROM licence l INNER JOIN departement d ON l.Departement = d.code_departement WHERE l.Commune != '' ";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                departements.add(resultSet.getString("code_departement") + " - " + resultSet.getString("nom_departement"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return departements;
    }

    /**
     * Méthode pour récupérer le code d'un département en fonction de son nom.
     * 
     * @param nom_departement Le nom du département.
     * @return Le code du département.
     */
    public String getCodeDepartement(String nom_departement) {
        String code_departement = null;
        // Séparer la chaîne en utilisant ' - ' comme séparateur
        String[] parts = nom_departement.split(" - ");
        nom_departement = parts[1]; // Le nom du département est le deuxième élément après la séparation
        String sql = "SELECT code_departement FROM departement WHERE nom_departement = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, nom_departement);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    code_departement = resultSet.getString("code_departement");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return code_departement;
    }

    /**
     * Méthode pour récupérer les suggestions de communes en fonction d'un mot-clé.
     * 
     * @param keyword Le mot-clé pour la recherche de communes.
     * @param limit La limite du nombre de suggestions à retourner.
     * @return Une liste de suggestions de communes.
     */
    public List<String> getCommuneSuggestions(String keyword, int limit) {
        List<String> suggestions = new ArrayList<>();
        String sql = "SELECT DISTINCT Commune FROM licence WHERE Commune IS NOT NULL AND Commune !='' AND Commune LIKE ? LIMIT ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, keyword + "%");
            statement.setInt(2, limit); // Limitez le nombre de suggestions
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    suggestions.add(resultSet.getString("Commune"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return suggestions;
    }

    /**
     * Méthode pour récupérer les suggestions de fédérations en fonction d'un mot-clé.
     * 
     * @param keyword Le mot-clé pour la recherche de fédérations.
     * @param limit La limite du nombre de suggestions à retourner.
     * @return Une liste de suggestions de fédérations.
     */
    public List<String> getFederationSuggestions(String keyword, int limit) {
        List<String> suggestions = new ArrayList<>();
        String sql = "SELECT DISTINCT Federation FROM licence WHERE Commune IS NOT NULL AND Commune !='' AND Federation LIKE ? LIMIT ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, "%" + keyword + "%");
            statement.setInt(2, limit); // Limitez le nombre de suggestions
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    suggestions.add(resultSet.getString("Federation"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return suggestions;
    }

    /**
     * Vérifie si une fédération existe dans la base de données.
     * 
     * @param federation Le nom de la fédération.
     * @return true si la fédération existe, false sinon.
     */
    public boolean isFederationExists(String federation) {
        String query = "SELECT COUNT(*) FROM licence WHERE Federation = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, federation);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    int count = resultSet.getInt(1);
                    return count > 0;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Vérifie si une commune existe dans la base de données.
     * 
     * @param commune Le nom de la commune.
     * @return true si la commune existe, false sinon.
     */
    public boolean isCommuneExists(String commune) {
        String query = "SELECT COUNT(*) FROM licence WHERE Commune = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, commune);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    int count = resultSet.getInt(1);
                    return count > 0;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }
}
