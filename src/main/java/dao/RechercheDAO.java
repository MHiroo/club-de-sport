package dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import model.Recherche;

/**
 * RechercheDAO est une classe qui gère les opérations de base de données pour les entités de recherche.
 * 
 * Cette classe fournit des méthodes pour créer, lire, mettre à jour et supprimer des enregistrements de recherche
 * dans la base de données. Elle utilise les connexions fournies par ConnectionDAO pour interagir avec la base de données.
 * 
 * @version 1.0
 */
public class RechercheDAO {
     // Informations de connexion à la base de données
    private static String url;
    private static String user;
    private static String password;

    /**
     * Constructeur de la classe RechercheDAO.
     * Charge les paramètres de connexion depuis un fichier de configuration et assure
     * que le driver JDBC est chargé.
     */
    public RechercheDAO() {
        Properties prop = new Properties();
        try {
            // Charger le fichier de propriétés depuis les ressources
            InputStream inputStream = ClubSportifDAO.class.getClassLoader().getResourceAsStream("config.properties");
            if (inputStream != null) {
                prop.load(inputStream);
                url = prop.getProperty("db.url");
                user = prop.getProperty("db.username");
                password = prop.getProperty("db.password");
            } else {
                System.err.println("Le fichier config.properties n'a pas pu être chargé.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // Assurez-vous que le driver JDBC de MySQL est chargé
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    // Méthodes pour accéder aux informations de connexion

    /**
     * Retourne l'URL de connexion à la base de données.
     * 
     * @return URL de connexion.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Retourne le nom d'utilisateur pour la connexion à la base de données.
     * 
     * @return Nom d'utilisateur.
     */
    public String getUser() {
        return user;
    }

    /**
     * Retourne le mot de passe pour la connexion à la base de données.
     * 
     * @return Mot de passe.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Méthode pour établir la connexion à la base de données.
     * 
     * @return Une connexion à la base de données.
     * @throws SQLException Si une erreur SQL survient.
     */
    public Connection getConnection() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            // Gestion de l'erreur ici. Par exemple, en la journalisant et en lançant une exception personnalisée
            e.printStackTrace();
        }
        return DriverManager.getConnection(url, user, password);
    }

    /**
     * Méthode pour enregistrer une recherche dans la base de données.
     * 
     * @param recherche La recherche à enregistrer.
     */
    public void enregistrerRecherche(Recherche recherche) {
        String requete = "INSERT INTO recherche (date, adresseip, region, codePostal, federation) VALUES (?, ?, ?, ?, ?)";
        // Préparation de la requête avec des paramètres
        try (Connection conn = getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(requete)) {
            // Attribution des valeurs aux paramètres de la requête
            preparedStatement.setTimestamp(1, new java.sql.Timestamp(recherche.getDateConnexion().getTime()));
            preparedStatement.setString(2, recherche.getadresseip());
            preparedStatement.setString(3, recherche.getregion());
            preparedStatement.setString(4, recherche.getcodePostal());
            preparedStatement.setString(5, recherche.getfederation());
            // Exécution de la requête
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
