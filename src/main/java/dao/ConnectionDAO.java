package dao;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

/**
 * ConnectionDAO est une classe utilitaire qui gère les connexions à la base de données.
 * 
 * Cette classe fournit des méthodes pour établir et fermer des connexions avec la base de données.
 * Elle utilise un fichier de configuration pour obtenir les informations de connexion nécessaires.
 * 
 * Méthodes disponibles :
 * - getConnection() : établit et retourne une connexion à la base de données.
 * - closeConnection(Connection conn) : Ferme la connexion spécifiée.
 * 
 * @version 1.0
 */
public class ConnectionDAO {
    private static String url;
    private static String user;
    private static String password;

    /**
     * Constructeur de la classe ConnectionDAO.
     * Charge les paramètres de connexion depuis un fichier de configuration et assure
     * que le driver JDBC est chargé.
     */
    public ConnectionDAO() {
        // Chargement du fichier de propriétés pour obtenir les informations de connexion
        Properties prop = new Properties();
        try {
            InputStream inputStream = ConnectionDAO.class.getClassLoader().getResourceAsStream("config.properties");
            if (inputStream != null) {
                prop.load(inputStream);
                url = prop.getProperty("db.url");
                user = prop.getProperty("db.username");
                password = prop.getProperty("db.password");
            } else {
                System.err.println("Le fichier config.properties n'a pas pu être chargé.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // Assurez-vous que le driver JDBC de MySQL est chargé
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode principale pour tester la connexion à la base de données.
     * 
     * @param args Arguments de la ligne de commande.
     * @throws SQLException Si une erreur SQL survient.
     */
    public static void main(String[] args) throws SQLException {
        Connection con = null;

        // Connexion à la base de données
        try {
            con = DriverManager.getConnection(url, user, password);
            System.out.println("Connexion réussie");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Fermeture de la connexion
            try {
                if (con != null) {
                    con.close();
                }
            } catch (Exception ignore) {
            }
        }
    }
}
