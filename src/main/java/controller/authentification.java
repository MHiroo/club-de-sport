package controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Base64;

import dao.HistoriqueDAO;
import dao.UserDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Historique;
import model.User;

import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Servlet implementation class authentification
 * 
 * Cette servlet gère l'authentification des utilisateurs. Elle vérifie les informations d'identification,
 * met à jour les tentatives de connexion échouées, et enregistre les connexions réussies et échouées dans l'historique.
 * 
 * @version 1.0
 */
@WebServlet("/authentification")
public class authentification extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private UserDAO userDAO;
    private HistoriqueDAO historiquedao;
    private static final int MAX_ATTEMPTS = 3;
    private static final long LOCK_TIME_DURATION = 60 * 60 * 1000; // 1 heure en millisecondes

    @Override
    public void init() throws ServletException {
        super.init();
        userDAO = new UserDAO();
        historiquedao = new HistoriqueDAO();
    }

    /**
     * Méthode pour obtenir l'image de profil par défaut.
     * 
     * @return Un tableau d'octets représentant l'image de profil par défaut.
     * @throws IOException Si une erreur survient lors de la lecture du fichier.
     */
    private byte[] getDefaultProfileImage() throws IOException {
        // Chemin de l'image par défaut dans le répertoire des ressources
        String defaultImagePath = getServletContext().getRealPath("/images/logo.webp");
        File file = new File(defaultImagePath);
        try (InputStream inputStream = new FileInputStream(file);
             ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            return outputStream.toByteArray();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        User user1 = userDAO.getUserByEmail(email);

        if (user1 != null && user1.getFailedAttempts() >= MAX_ATTEMPTS) {
            long lastAttemptTime = user1.getLastFailedAttempt().getTime();
            long currentTime = System.currentTimeMillis();

            if (currentTime - lastAttemptTime < LOCK_TIME_DURATION) {
                request.setAttribute("erreurMessage", "Votre compte est verrouillé. Veuillez réessayer plus tard.");
                request.getRequestDispatcher("Authentification.jsp").forward(request, response);
                return;
            } else {
                userDAO.resetFailedAttempts(email);
            }
        }

        // Récupération de la session
        HttpSession session = request.getSession();
        
        // Suppression des attributs de la session
        session.removeAttribute("userId");
        session.removeAttribute("elu");
        session.removeAttribute("acteur");

        User user = userDAO.authenticateUser(email, password);
        InetAddress adresss = InetAddress.getLocalHost();
        String adresse_ip = adresss.getHostAddress();
        String navigateur = getBrowserName(request);
        Date date_connexion = new Date(System.currentTimeMillis());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateConnexionFormatted = dateFormat.format(date_connexion);

        int id = (user != null) ? user.getId() : 9;
        String statut_connexion = (user != null) ? "succes" : "echec";

        // Construction de la ligne de journalisation
        String logLine = String.format("%d - %s - %s - %s - %s%n", id, dateConnexionFormatted, adresse_ip, navigateur, statut_connexion);

        Historique historique = new Historique(0, date_connexion, adresse_ip, navigateur, id, statut_connexion);

        try {
            HistoriqueDAO.enregistrerConnexion(historique);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (user != null) {
            userDAO.resetFailedAttempts(email);
            String UserType = user.getUserType().substring(0, 1).toUpperCase() + user.getUserType().substring(1).toLowerCase();
            byte[] photoProfil = user.getPhotoProfil();
            
            // Assigner une photo de profil par défaut si photoProfil est null
            if (photoProfil == null) {
                photoProfil = getDefaultProfileImage();
            }
            if (user.getValider() == 1) {
                session.setAttribute("userId", user.getId());
                session.setAttribute("email", user.getEmail());
                response.sendRedirect("InitPassword.jsp");
            } else {
                if (user.getUserType().equals("entraineur") || user.getUserType().equals("president")) {
                    session.setAttribute("userId", user.getId());
                    session.setAttribute("userNom", user.getNom());
                    session.setAttribute("userPrenom", user.getPrenom());
                    session.setAttribute("userType", UserType);
                    session.setAttribute("photoProfil", photoProfil);
                    session.setAttribute("acteur", 1);
                    response.sendRedirect("GActeurs.jsp");
                } else if (user.getUserType().equals("elu")) {
                    session.setAttribute("userId", user.getId());
                    session.setAttribute("userNom", user.getNom());
                    session.setAttribute("userPrenom", user.getPrenom());
                    session.setAttribute("userType", UserType);
                    session.setAttribute("photoProfil", photoProfil);
                    session.setAttribute("elu", 2);
                    response.sendRedirect("EluServlet");
                } else if (user.getUserType().equals("autre")) {
                    session.setAttribute("userId", user.getId());
                    session.setAttribute("userNom", user.getNom());
                    session.setAttribute("userPrenom", user.getPrenom());
                    session.setAttribute("userType", UserType);
                    session.setAttribute("photoProfil", photoProfil);
                    session.setAttribute("autre", 3);
                    response.sendRedirect("index.jsp");
                }
            }
        } else {
            if (user1 != null) {
                int failedAttempts = user1.getFailedAttempts();
                userDAO.updateFailedAttempts(failedAttempts + 1, email);
            }
            request.setAttribute("erreurMessage", "Adresse e-mail ou mot de passe incorrect.");
            request.getRequestDispatcher("Authentification.jsp").forward(request, response);
        }
    }

    /**
     * Méthode pour obtenir le nom du navigateur à partir de l'en-tête User-Agent.
     * 
     * @param request La requête HttpServletRequest.
     * @return Le nom du navigateur.
     */
    private String getBrowserName(HttpServletRequest request) {
        String userAgent = request.getHeader("User-Agent");

        if (userAgent == null) {
            return "Unknown";
        }

        // Détection du navigateur basée sur l'en-tête User-Agent
        if (userAgent.contains("MSIE") || userAgent.contains("Trident")) {
            return "Internet Explorer";
        } else if (userAgent.contains("Edge")) {
            return "Microsoft Edge";
        } else if (userAgent.contains("Opera") || userAgent.contains("OPR")) {
            return "Opera";
        } else if (userAgent.contains("Firefox")) {
            return "Mozilla Firefox";
        } else if (userAgent.contains("Chrome")) {
            return "Google Chrome";
        } else if (userAgent.contains("Safari")) {
            return "Apple Safari";
        } else {
            return "Unknown";
        }
    }
}
