package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import dao.SpaceDAO;

/**
 * Servlet implementation class DeletePostServlet
 */
@WebServlet("/DeletePostServlet")
public class DeletePostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeletePostServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 int postId = Integer.parseInt(request.getParameter("postId"));
		 int spaceId = Integer.parseInt(request.getParameter("spaceId"));
	        SpaceDAO dao = new SpaceDAO();
	        try {
	            dao.deletePost(postId);
	            response.sendRedirect("PostsServlet?spaceId=" + spaceId);
	        } catch (Exception e) {
	            throw new ServletException("Error deleting post", e);
	        }
	}

}
