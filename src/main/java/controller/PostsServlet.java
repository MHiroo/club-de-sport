package controller;
import org.owasp.encoder.Encode;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import dao.SpaceDAO;
import model.Post;
import model.Space;

/**
 * Servlet implementation class PostsServlet
 */
@WebServlet("/PostsServlet")
public class PostsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PostsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int spaceId = Integer.parseInt(request.getParameter("spaceId"));
		Space spaceData = null;
		List<Post> posts = null;
		SpaceDAO dao = new SpaceDAO();
		try {
			posts = dao.getPostsBySpaceId(spaceId);
			spaceData = dao.getSpaceById(spaceId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    // Supposons que vous chargez des données ici...
		request.setAttribute("posts", posts);
	    request.setAttribute("spaceData", spaceData);
	    RequestDispatcher dispatcher = request.getRequestDispatcher("post.jsp");
	    dispatcher.forward(request, response);
	}
	 /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int spaceId = Integer.parseInt(request.getParameter("spaceId"));
		Space spaceData = null;
		List<Post> posts = null;
		SpaceDAO dao = new SpaceDAO();
		try {
			posts = dao.getPostsBySpaceId(spaceId);
			spaceData = dao.getSpaceById(spaceId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    // Supposons que vous chargez des données ici...
		request.setAttribute("posts", posts);
	    request.setAttribute("spaceData", spaceData);
	    RequestDispatcher dispatcher = request.getRequestDispatcher("post.jsp");
	    dispatcher.forward(request, response);
    	
    }
        
}
