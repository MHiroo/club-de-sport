package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.SQLException;

import dao.SpaceDAO;

/**
 * CommentServlet est une servlet qui gère l'ajout de commentaires aux posts.
 * 
 * Cette servlet traite les requêtes POST pour ajouter des commentaires aux posts
 * dans un espace spécifique. Elle vérifie que l'utilisateur est connecté avant 
 * d'autoriser l'ajout du commentaire.
 * 
 * @version 1.0
 */
@WebServlet("/CommentServlet")
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
       
    /**
     * Constructeur de la servlet CommentServlet.
     * 
     * @see HttpServlet#HttpServlet()
     */
    public CommentServlet() {
        super();
    }

    /**
     * Méthode doPost qui gère la requête POST pour ajouter un commentaire.
     * 
     * @param request La requête HttpServletRequest.
     * @param response La réponse HttpServletResponse.
     * @throws ServletException Si une erreur de servlet survient.
     * @throws IOException Si une erreur d'entrée/sortie survient.
     * 
     * @see HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int postId = Integer.parseInt(request.getParameter("postId"));
        String content = request.getParameter("content");
        HttpSession session = request.getSession(false);
        Integer userId = (Integer) session.getAttribute("userId");
        
        if (userId != null) {
            SpaceDAO dao = new SpaceDAO();
            try {
                dao.addComment(userId, postId, content);
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write("{\"success\": true, \"user\": \"" + "commentaire ajouté" + "\"}");
            } catch (SQLException e) {
                e.printStackTrace();
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write("{\"success\": false, \"message\": \"" + e.getMessage() + "\"}");
            }
        } else {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("{\"success\": false, \"message\": \"User not logged in\"}");
        }
    }
}
