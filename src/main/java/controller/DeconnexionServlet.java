package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

/**
 * DeconnexionServlet est une servlet qui gère la déconnexion des utilisateurs.
 * 
 * Cette servlet traite les requêtes GET pour invalider la session actuelle et déconnecter l'utilisateur.
 * 
 * @version 1.0
 */
@WebServlet("/DeconnexionServlet")
public class DeconnexionServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    /**
     * Méthode doGet qui gère la requête GET pour déconnecter l'utilisateur.
     * 
     * @param request La requête HttpServletRequest.
     * @param response La réponse HttpServletResponse.
     * @throws ServletException Si une erreur de servlet survient.
     * @throws IOException Si une erreur d'entrée/sortie survient.
     * 
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Récupération de la session
        HttpSession session = request.getSession();
        
        // Invalider la session pour supprimer tous les attributs de session
        session.invalidate();

        // Rediriger vers la page d'accueil
        response.sendRedirect("index.jsp");
    }
}
