package controller;

import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;

import dao.LicenceDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FederationAutocompleteServlet
 */
@WebServlet("/FederationAutocompleteServlet")
public class FederationAutocompleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FederationAutocompleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String keyword = request.getParameter("keyword");
        LicenceDAO licenceDao = new LicenceDAO();
        List<String> suggestions = licenceDao.getFederationSuggestions(keyword, 10); // Limitez à 10 suggestions
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        new Gson().toJson(suggestions, response.getWriter());
    }
}
