package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import com.google.gson.Gson;

import dao.LicenceDAO;

/**
 * CommuneAutocompleteServlet est une servlet qui gère les suggestions de communes pour l'autocomplétion.
 * 
 * Cette servlet traite les requêtes GET pour fournir des suggestions de communes basées sur un mot-clé.
 * Elle utilise LicenceDAO pour récupérer les suggestions de la base de données.
 * 
 * @version 1.0
 */
@WebServlet("/CommuneAutocompleteServlet")
public class CommuneAutocompleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
       
    /**
     * Constructeur de la servlet CommuneAutocompleteServlet.
     * 
     * @see HttpServlet#HttpServlet()
     */
    public CommuneAutocompleteServlet() {
        super();
    }

    /**
     * Méthode doGet qui gère la requête GET pour fournir des suggestions de communes.
     * 
     * @param request La requête HttpServletRequest.
     * @param response La réponse HttpServletResponse.
     * @throws ServletException Si une erreur de servlet survient.
     * @throws IOException Si une erreur d'entrée/sortie survient.
     * 
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String keyword = request.getParameter("keyword");
        LicenceDAO licenceDao = new LicenceDAO();
        List<String> suggestions = licenceDao.getCommuneSuggestions(keyword, 20); // Limitez à 20 suggestions
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        new Gson().toJson(suggestions, response.getWriter());
    }
}
