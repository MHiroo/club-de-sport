package controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Licence;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import dao.LicenceDAO;

/**
 * Servlet implementation class PublicServlet
 */
@WebServlet("/PublicServlet")
public class PublicServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PublicServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LicenceDAO licenceDao = new LicenceDAO();
        String federation = request.getParameter("federation");
        String localisation = request.getParameter("localisation");
        String localisationType = request.getParameter("localisationType");

        ArrayList<Licence> licenceList = new ArrayList<Licence>();

        if (federation != null || localisation != null) {
            if(localisationType.equals("departement")) {
                localisation = licenceDao.getCodeDepartement(localisation);
            }
            // C'est une requête AJAX 
        
            licenceList = licenceDao.getLicencesByParameters(federation, localisation, localisationType);
            int[] tab = new int[3];
            tab = licenceDao.getLicencieSums(federation, localisation, localisationType);
            int nbLicencesTotal = tab[0];
            int nbLicencesHomme = tab[1];
            int nbLicencesFemme = tab[2];
           

             // Créer un objet JSON contenant les données et les valeurs calculées
             JsonObject jsonObject = new JsonObject();
             jsonObject.addProperty("nbLicencesTotal", nbLicencesTotal);
             jsonObject.addProperty("nbLicencesHomme", nbLicencesHomme);
             jsonObject.addProperty("nbLicencesFemme", nbLicencesFemme);
             jsonObject.add("licenceList", new Gson().toJsonTree(licenceList));

             // Envoyer les données au format JSON
             response.getWriter().write(jsonObject.toString());
        } else {
            // C'est la charge initiale de la page, envoyer les fédérations et régions pour remplir les listes déroulantes
            List<String> regions = licenceDao.getUniqueRegions();
            List<String> departements = licenceDao.getUniqueDepartements();
            request.setAttribute("regions", regions);
            request.setAttribute("departements", departements);

            RequestDispatcher dispatcher = request.getRequestDispatcher("/public.jsp");
            dispatcher.forward(request, response);
        }
    }

}
