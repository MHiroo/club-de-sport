package controller;

import java.io.IOException;
import java.net.InetAddress;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import com.google.gson.Gson;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import dao.ClubSportifDAO;
import dao.RechercheDAO;
import model.ClubSportif;
import model.Recherche;

/**
 * ClubSportifServlet est une servlet qui gère les requêtes pour afficher et filtrer les clubs sportifs.
 * 
 * Cette servlet traite les requêtes GET pour afficher les clubs sportifs filtrés par fédération, code postal et région.
 * Elle enregistre également les recherches effectuées par les utilisateurs dans la base de données.
 * 
 * @version 1.0
 */
@WebServlet("/ClubSportifServlet")
public class ClubSportifServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {        
        ClubSportifDAO dao = new ClubSportifDAO();
        RechercheDAO Rdao = new RechercheDAO();

        String federation = request.getParameter("federation");
        String codePostal = request.getParameter("codePostal");
        String region = request.getParameter("region");
        InetAddress adresss = InetAddress.getLocalHost();
        String adresseip = adresss.getHostAddress();
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String dateConnexionFormatted = dateFormat.format(date);

        if (federation != null || codePostal != null || region != null) {
            // C'est une requête AJAX pour les clubs filtrés
            List<ClubSportif> clubs = dao.getClubsFiltered(federation, codePostal, region);
            Recherche recherche = new Recherche(date, adresseip, region, codePostal, federation);
            
            Rdao.enregistrerRecherche(recherche);

            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            new Gson().toJson(clubs, response.getWriter());
        } else {
            // C'est la charge initiale de la page, envoyer les fédérations et régions pour remplir les listes déroulantes
            List<String> federations = dao.getUniqueFederations();
            List<String> regions = dao.getUniqueRegions();
            request.setAttribute("federations", federations);
            request.setAttribute("regions", regions);

            RequestDispatcher dispatcher = request.getRequestDispatcher("/clubs.jsp");
            dispatcher.forward(request, response);
        }
    }
}
