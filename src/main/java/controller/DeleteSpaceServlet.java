package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import dao.SpaceDAO;

import java.io.IOException;
import java.sql.SQLException;

/**
 * DeleteSpaceServlet est une servlet qui gère la suppression des espaces.
 * 
 * Cette servlet traite les requêtes GET pour supprimer un espace spécifié par son identifiant.
 * Elle utilise SpaceDAO pour interagir avec la base de données.
 * 
 * @version 1.0
 */
@WebServlet("/DeleteSpaceServlet")
public class DeleteSpaceServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * Constructeur de la servlet DeleteSpaceServlet.
     * 
     * @see HttpServlet#HttpServlet()
     */
    public DeleteSpaceServlet() {
        super();
    }

    /**
     * Méthode doGet qui gère la requête GET pour supprimer un espace.
     * 
     * @param request La requête HttpServletRequest.
     * @param response La réponse HttpServletResponse.
     * @throws ServletException Si une erreur de servlet survient.
     * @throws IOException Si une erreur d'entrée/sortie survient.
     * 
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int spaceId = Integer.parseInt(request.getParameter("spaceId"));
        SpaceDAO dao = new SpaceDAO();

        try {
            dao.deleteSpace(spaceId);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Redirection vers la servlet HomeGServlet après suppression
        response.sendRedirect("HomeGServlet");
    }
}
