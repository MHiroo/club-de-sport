package controller;
import org.owasp.encoder.Encode;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;
import java.io.IOException;
import java.io.InputStream;

import dao.SpaceDAO;
import model.Post;

/**
 * Servlet implementation class UploadPostServlet
 */
@WebServlet("/UploadPostServlet")
@MultipartConfig
public class UploadPostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private static final long MAX_FILE_SIZE = 5 * 1024 * 1024; // 5 Mo
    private static final String[] ALLOWED_FILE_TYPES = {
        "image/jpeg", "image/png", "image/jpg", "image/webp",
        "image/gif", "image/bmp", "image/tiff", "image/svg+xml"
    };

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadPostServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int spaceId = Integer.parseInt(request.getParameter("spaceId"));
        Integer postId = request.getParameter("postId") != null ? Integer.parseInt(request.getParameter("postId")) : null;
        String title = request.getParameter("title");
        String content = request.getParameter("content");

        // Échapper les entrées pour éviter les injections XSS
        title = Encode.forHtml(title);
        PolicyFactory policy = Sanitizers.FORMATTING.and(Sanitizers.BLOCKS);
        content  = policy.sanitize(content);
        Part filePart = request.getPart("photo");
        InputStream inputStream = null;
        byte[] photo = null;

        if (filePart != null && filePart.getSize() > 0) {
            String fileType = filePart.getContentType();
            long fileSize = filePart.getSize();

            // Vérifier la taille du fichier
            if (fileSize > MAX_FILE_SIZE) {
                request.setAttribute("errorMessage", "La taille de l'image ne doit pas dépasser 5 Mo.");
                request.getRequestDispatcher("PostsServlet?spaceId=" + spaceId).forward(request, response);
                return;
            }

            // Vérifier le type du fichier
            boolean isValidType = false;
            for (String allowedType : ALLOWED_FILE_TYPES) {
                if (allowedType.equals(fileType)) {
                    isValidType = true;
                    break;
                }
            }

            if (!isValidType) {
                request.setAttribute("errorMessage", "Seuls les types d'images suivants sont autorisés: JPEG, PNG, JPG, WEBP, GIF, BMP, TIFF, SVG.");
                request.getRequestDispatcher("PostsServlet?spaceId=" + spaceId).forward(request, response);
                return;
            }

            inputStream = filePart.getInputStream();
            photo = inputStream.readAllBytes();
        }

        Post post = new Post();
        if (postId != null) {
            post.setPostId(postId);
        }
        post.setSpaceId(spaceId);
        post.setTitle(title);
        post.setContent(content);

        if (photo != null) {
            post.setPhoto(photo);
        }

        SpaceDAO dao = new SpaceDAO();
        boolean result;
        if (post.getPostId() != 0) {
            result = dao.updatePost(post);
        } else {
            result = dao.insertPost(post);
        }

        if (result) {
            response.sendRedirect("PostsServlet?spaceId=" + spaceId);
        } else {
            request.getRequestDispatcher("PostsServlet?spaceId=" + spaceId).forward(request, response);
            return;
        }
    }
}
