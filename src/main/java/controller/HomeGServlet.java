package controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Space;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.SpaceDAO;

/**
 * Servlet implementation class HomeGServlet
 */
@WebServlet("/HomeGServlet")
public class HomeGServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeGServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Récupération de la session créée dans un autre servlet
        HttpSession session = request.getSession(false); // Mettre false pour ne pas créer de nouvelle session si elle n'existe pas
        Integer userId = 0;
        List<Space> allSpaces = new ArrayList<>();
        List<Space> subscribedSpaces = new ArrayList<>();
        List<Space> otherSpaces = new ArrayList<>();
        SpaceDAO spaceDao = new SpaceDAO();

        try {
            // Si la session existe
            if (session != null) {
                String userType = (String) session.getAttribute("userType");
                System.out.println(userType);
                Integer userIdInteger = (Integer) session.getAttribute("userId");
                
                if (userIdInteger != null) {
                    userId = userIdInteger;
                }

                if (userType != null && (userType.equals("Entraineur") || userType.equals("President"))) {
                    allSpaces = spaceDao.listAllSpaces(userId);
                } else {
                    // Si l'utilisateur est un "elu" ou "autre", on réinitialise userId à 0 et ils voient tous les espaces
                    userId = 0;
                    allSpaces = spaceDao.listAllSpaces(userId);
                    
                    // Récupération des abonnements de l'utilisateur
                    if (userIdInteger != null) {
                        subscribedSpaces = spaceDao.getUserSubscriptions(userIdInteger);
                    }
                }


                // Séparation des espaces en abonnements et autres espaces
                for (Space space : allSpaces) {
                    boolean isSubscribed = false;
                    for (Space subscribedSpace : subscribedSpaces) {
                        if (subscribedSpace.getSpaceId() == space.getSpaceId()) {
                            isSubscribed = true;
                            break;
                        }
                    }
                    if (!isSubscribed) {
                        otherSpaces.add(space);
                    }
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Définir les attributs de requête
        request.setAttribute("subscribedSpaces", subscribedSpaces);
        request.setAttribute("otherSpaces", otherSpaces);

        // Forward vers la JSP
        RequestDispatcher dispatcher = request.getRequestDispatcher("HomeSpaces.jsp");
        dispatcher.forward(request, response);
        }
}

