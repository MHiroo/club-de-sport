package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.SQLException;

import dao.SpaceDAO;

/**
 * LikeServlet est une servlet qui gère les likes sur les posts.
 * 
 * Cette servlet traite les requêtes POST pour ajouter un like à un post par un utilisateur.
 * Elle vérifie si l'utilisateur a déjà aimé le post avant d'ajouter le like.
 * 
 * @version 1.0
 */
@WebServlet("/LikeServlet")
public class LikeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
       
    /**
     * Constructeur de la servlet LikeServlet.
     * 
     * @see HttpServlet#HttpServlet()
     */
    public LikeServlet() {
        super();
    }

    /**
     * Méthode doPost qui gère la requête POST pour ajouter un like à un post.
     * 
     * @param request La requête HttpServletRequest.
     * @param response La réponse HttpServletResponse.
     * @throws ServletException Si une erreur de servlet survient.
     * @throws IOException Si une erreur d'entrée/sortie survient.
     * 
     * @see HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int postId = Integer.parseInt(request.getParameter("postId"));
        HttpSession session = request.getSession(false);
        Integer userId = (Integer) session.getAttribute("userId");

        if (userId != null) {
            SpaceDAO dao = new SpaceDAO();
            try {
                if (dao.hasLiked(userId, postId)) {
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write("{\"success\": false, \"message\": \"Vous avez déjà aimé ce post.\"}");
                } else {
                    dao.likePost(userId, postId);
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write("{\"success\": true}");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                response.getWriter().write("{\"success\": false, \"message\": \"" + e.getMessage() + "\"}");
            }
        } else {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("{\"success\": false, \"message\": \"Utilisateur non connecté.\"}");
        }
    }
}
