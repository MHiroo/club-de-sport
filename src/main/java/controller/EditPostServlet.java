package controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import dao.SpaceDAO;
import model.Post;

/**
 * Servlet implementation class EditPostServlet
 */

@WebServlet("/EditPostServlet")
public class EditPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditPostServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int postId = Integer.parseInt(request.getParameter("postId"));
		int spaceId = Integer.parseInt(request.getParameter("spaceId"));
        SpaceDAO dao = new SpaceDAO();
        try {
            Post post = dao.getPostById(postId);
            request.setAttribute("post", post);
            request.setAttribute("spaceId", spaceId);
            RequestDispatcher dispatcher = request.getRequestDispatcher("edit_post.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e) {
            throw new ServletException("Error retrieving post", e);
        }
	}

}
