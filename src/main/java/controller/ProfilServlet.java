package controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import dao.UserDAO;

/**
 * ProfilServlet est une servlet qui gère la mise à jour du profil utilisateur, 
 * y compris le téléchargement et l'affichage de la photo de profil.
 * 
 * @version 1.0
 */
@WebServlet("/ProfilServlet")
@MultipartConfig(maxFileSize = 1024 * 1024 * 2) // Limite de taille de fichier de 2 Mo
public class ProfilServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private UserDAO userDAO;

    /**
     * Constructeur de la servlet ProfilServlet.
     */
    public ProfilServlet() {
        super();
        this.userDAO = new UserDAO();
    }

    /**
     * Méthode doGet qui gère la requête GET pour afficher la page de profil.
     * 
     * @param request La requête HttpServletRequest.
     * @param response La réponse HttpServletResponse.
     * @throws ServletException Si une erreur de servlet survient.
     * @throws IOException Si une erreur d'entrée/sortie survient.
     * 
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId"); // Vérifie que l'utilisateur est connecté et a un userId valide

        if (userId == null) {
            response.sendRedirect("Authentification.jsp"); // Redirection vers la page de connexion si l'utilisateur n'est pas connecté
            return;
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("profil.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * Méthode doPost qui gère la requête POST pour mettre à jour la photo de profil de l'utilisateur.
     * 
     * @param request La requête HttpServletRequest.
     * @param response La réponse HttpServletResponse.
     * @throws ServletException Si une erreur de servlet survient.
     * @throws IOException Si une erreur d'entrée/sortie survient.
     * 
     * @see HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId");

        // Traite la photo de profil téléchargée
        Part filePart = request.getPart("photoProfil");
        if (filePart != null && filePart.getSize() > 0) {
            String contentType = filePart.getContentType();
            if (contentType.startsWith("image/")) {
                try (InputStream fileContent = filePart.getInputStream();
                     ByteArrayOutputStream buffer = new ByteArrayOutputStream()) {

                    byte[] temp = new byte[1024];
                    int bytesRead;
                    while ((bytesRead = fileContent.read(temp)) != -1) {
                        buffer.write(temp, 0, bytesRead);
                    }

                    byte[] photoProfil = buffer.toByteArray();
                    session.setAttribute("photoProfil", photoProfil);

                    // Met à jour la photo de profil dans la base de données
                    if (userId != null) {
                        userDAO.updatePhotoProfil(userId, photoProfil);
                    }
                }
            } else {
                request.setAttribute("error", "Type de fichier invalide.");
            }
        } 

        doGet(request, response);
    }
}
