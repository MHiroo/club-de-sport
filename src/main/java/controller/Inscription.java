package controller;

import model.*;
import dao.*;
import jakarta.servlet.*;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.owasp.encoder.Encode;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Inscription est une servlet qui gère l'inscription des utilisateurs.
 * 
 * Cette servlet traite les requêtes GET pour afficher le formulaire d'inscription
 * et les requêtes POST pour traiter les données soumises par le formulaire.
 * 
 * @version 1.0
 */
@WebServlet("/Inscription")
@MultipartConfig
public class Inscription extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private UserDAO userDAO; // Instance du DAO pour accéder à la base de données

    /**
     * Méthode init pour initialiser la servlet et instancier le UserDAO.
     * 
     * @throws ServletException Si une erreur de servlet survient.
     */
    @Override
    public void init() throws ServletException {
        super.init();
        // Initialisation du UserDAO lors du démarrage du servlet
        userDAO = new UserDAO();
    }

    /**
     * Méthode doGet qui gère la requête GET pour afficher le formulaire d'inscription.
     * 
     * @param request La requête HttpServletRequest.
     * @param response La réponse HttpServletResponse.
     * @throws ServletException Si une erreur de servlet survient.
     * @throws IOException Si une erreur d'entrée/sortie survient.
     * 
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Création du DAO pour accéder aux données des clubs sportifs
        ClubSportifDAO clubSportifDAO = new ClubSportifDAO();

        // Récupération de la liste des fédérations uniques
        List<String> federations = clubSportifDAO.getUniqueFederations();

        // Envoi des données à la JSP
        request.setAttribute("federations", federations);
        request.getRequestDispatcher("Inscription.jsp").forward(request, response);
    }

    /**
     * Méthode doPost qui gère la requête POST pour traiter les données du formulaire d'inscription.
     * 
     * @param request La requête HttpServletRequest.
     * @param response La réponse HttpServletResponse.
     * @throws ServletException Si une erreur de servlet survient.
     * @throws IOException Si une erreur d'entrée/sortie survient.
     * 
     * @see HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Récupération des paramètres du formulaire
        String nom = request.getParameter("nom");
        String prenom = request.getParameter("prenom");
        String mail = request.getParameter("email");
        String type = request.getParameter("type");
        String codePostal = request.getParameter("codePostal");
        String federation = request.getParameter("federation");

        // Échapper les entrées pour éviter les injections XSS
        nom = Encode.forHtml(nom);
        prenom = Encode.forHtml(prenom);
        mail = Encode.forHtml(mail);
        type = Encode.forHtml(type);
        codePostal = Encode.forHtml(codePostal);
        federation = Encode.forHtml(federation);
        
        Part filePart = request.getPart("preuve");
        InputStream inputStream = null;
        byte[] pdf = null;
        
        if (filePart != null && filePart.getSize() > 0) {
            String fileName = filePart.getSubmittedFileName();
            String fileType = filePart.getContentType();
            long fileSize = filePart.getSize();

            // Types de fichiers autorisés
            List<String> validFileTypes = List.of("application/pdf", "image/jpeg", "image/jpg", "image/png");

            if (!validFileTypes.contains(fileType)) {
                request.setAttribute("fileErrorMsg", "Le fichier doit être de type PDF, JPEG, JPG ou PNG.");
                doGet(request, response);
                return;
            }

            if (fileSize > 5 * 1024 * 1024) { // 5 Mo en octets
                request.setAttribute("fileErrorMsg", "La taille du fichier doit être inférieure à 5 Mo.");
                doGet(request, response);
                return;
            }

            inputStream = filePart.getInputStream();
            pdf = inputStream.readAllBytes();
        }

        // Création du DAO pour accéder aux données des clubs sportifs
        ClubSportifDAO clubSportifDAO = new ClubSportifDAO();
        UserDAO userdao = new UserDAO();
        
        // Vérification si l'email existe déjà
        if (userdao.emailExists(mail)) {
            request.setAttribute("emailExistsMsg", "L'e-mail existe déjà. Veuillez utiliser une autre adresse e-mail.");
            doGet(request, response);
            return; // Arrête l'exécution de la méthode
        }
        
        if (type.equals("elu")) {
            userdao.addUser(nom, prenom, mail, type, 0, pdf);
        } else {
            // Récupération de l'ID du premier établissement correspondant
            int etablissementId = clubSportifDAO.getEtablissementIdByPostalCodeAndFederation(federation, codePostal);
            if (!federation.equals("") || !codePostal.equals("")) {       
                if (etablissementId == 0) {
                    request.setAttribute("etablissementNotFoundMsg", "L'établissement correspondant n'existe pas.");
                    doGet(request, response);
                    return; // Arrête l'exécution de la méthode
                }    
            }    

            userdao.addUser(nom, prenom, mail, type, etablissementId, pdf);
        }
        response.sendRedirect("Authentification.jsp");
    }
}
