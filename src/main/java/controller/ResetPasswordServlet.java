package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;

import org.mindrot.jbcrypt.BCrypt;

import dao.UserDAO;

/**
 * Servlet implementation class ResetPasswordServlet
 */
@WebServlet("/ResetPasswordServlet")
public class ResetPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ResetPasswordServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String newPassword = request.getParameter("newPassword");
        String confirmPassword = request.getParameter("confirmPassword");

        response.setContentType("application/json");
        PrintWriter out = response.getWriter();

        if (newPassword == null || !newPassword.equals(confirmPassword)) {
            out.write("{\"success\": false, \"message\": \"Les mots de passe ne correspondent pas.\"}");
            return;
        }

        if (!isValidPassword(newPassword)) {
            out.write("{\"success\": false, \"message\": \"Le mot de passe ne respecte pas les critères de sécurité.\"}");
            return;
        }

        HttpSession session = request.getSession();
        String email = (String) session.getAttribute("email");

        // Crypter le nouveau mot de passe avec Bcrypt
        String hashedPassword = BCrypt.hashpw(newPassword, BCrypt.gensalt());

        // Mettre à jour le mot de passe dans la base de données via UserDAO
        UserDAO udao = new UserDAO();
        if (udao.updatePassword(email, hashedPassword)) {
            out.write("{\"success\": true}");
        } else {
            out.write("{\"success\": false, \"message\": \"Erreur lors de la mise à jour du mot de passe.\"}");
        }
    }

    private boolean isValidPassword(String password) {
        if (password.length() < 8) {
            return false;
        }
        if (!password.matches(".*[A-Z].*")) {
            return false;
        }
        if (!password.matches(".*\\d.*")) {
            return false;
        }
        // Permettre seulement certains caractères spéciaux
        if (!password.matches(".*[!@#$%^&*(),.?\":{}|<>].*")) {
            return false;
        }
        // Rejeter les caractères spéciaux non autorisés
        if (password.matches(".*[^a-zA-Z0-9!@#$%^&*(),.?\":{}|<>].*")) {
            return false;
        }
        return true;
    }


}
