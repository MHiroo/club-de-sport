package controller;

import dao.UserDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.mindrot.jbcrypt.BCrypt;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

/**
 * InitPasswordServlet est une servlet qui gère l'initialisation du mot de passe pour un utilisateur lors de sa première connexion.
 * 
 * Cette servlet traite les requêtes POST pour initialiser le mot de passe de l'utilisateur et envoyer un email de confirmation.
 * 
 * @version 1.0
 */
@WebServlet("/InitPasswordServlet")
public class InitPasswordServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private UserDAO userDAO;

    /**
     * Méthode init pour initialiser la servlet et instancier le UserDAO.
     * 
     * @throws ServletException Si une erreur de servlet survient.
     */
    @Override
    public void init() throws ServletException {
        super.init();
        userDAO = new UserDAO();
    }

    /**
     * Méthode doPost qui gère la requête POST pour initialiser le mot de passe de l'utilisateur.
     * 
     * @param request La requête HttpServletRequest.
     * @param response La réponse HttpServletResponse.
     * @throws ServletException Si une erreur de servlet survient.
     * @throws IOException Si une erreur d'entrée/sortie survient.
     * 
     * @see HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session.getAttribute("userId") == null) {
            response.sendRedirect("Authentification.jsp");
            return;
        }

        int userId = (int) session.getAttribute("userId");
        String email = (String) session.getAttribute("email");
        String newPassword = request.getParameter("newPassword");
        String confirmPassword = request.getParameter("confirmPassword");

        if (!newPassword.equals(confirmPassword)) {
            request.setAttribute("errorMessage", "Les mots de passe ne correspondent pas.");
            request.getRequestDispatcher("InitPassword.jsp").forward(request, response);
            return;
        }

        try {
            if (userDAO.isFirstLogin(userId)) {
                String hashedPassword = BCrypt.hashpw(newPassword, BCrypt.gensalt());
                userDAO.updatePassword(email, hashedPassword);
                userDAO.updateValidationStatus(userId, 2); // Marquer comme initialisé
                sendConfirmationEmail(email);

                request.setAttribute("successMessage", "Votre mot de passe a été initialisé avec succès.");
                request.getRequestDispatcher("Authentification.jsp").forward(request, response);
            } else {
                request.setAttribute("errorMessage", "Erreur lors de l'initialisation du mot de passe.");
                request.getRequestDispatcher("InitPassword.jsp").forward(request, response);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            request.setAttribute("errorMessage", "Erreur interne. Veuillez réessayer plus tard.");
            request.getRequestDispatcher("InitPassword.jsp").forward(request, response);
        }
    }

    /**
     * Méthode pour envoyer un email de confirmation à l'utilisateur après l'initialisation de son mot de passe.
     * 
     * @param userEmail L'adresse email de l'utilisateur.
     */
    private void sendConfirmationEmail(String userEmail) {
        final String username = "clubssportif@gmail.com";
        final String password = "kdsabinqkpvkruhm"; // Utilisez un mot de passe d'application pour plus de sécurité

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(userEmail));
            message.setSubject("Confirmation d'initialisation de mot de passe");
            message.setText("Votre mot de passe a été initialisé avec succès.");

            Transport.send(message);
            System.out.println("Email de confirmation envoyé avec succès.");

        } catch (MessagingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
