package controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Licence;
import dao.LicenceDAO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * EluServlet est une servlet qui gère les requêtes pour afficher et filtrer les licences sportives.
 * 
 * Cette servlet traite les requêtes GET et POST pour afficher les licences sportives filtrées par fédération, localisation et type de localisation.
 * Elle utilise LicenceDAO pour interagir avec la base de données et obtenir les données nécessaires.
 * 
 * @version 1.0
 */
@WebServlet("/EluServlet")
public class EluServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * Constructeur de la servlet EluServlet.
     * 
     * @see HttpServlet#HttpServlet()
     */
    public EluServlet() {
        super();
    }

    /**
     * Méthode doPost qui gère la requête POST pour obtenir les licences sportives filtrées et les statistiques associées.
     * 
     * @param request La requête HttpServletRequest.
     * @param response La réponse HttpServletResponse.
     * @throws ServletException Si une erreur de servlet survient.
     * @throws IOException Si une erreur d'entrée/sortie survient.
     * 
     * @see HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LicenceDAO licenceDao = new LicenceDAO();
        String federation = request.getParameter("federation");
        String localisation = request.getParameter("localisation");
        String localisationType = request.getParameter("localisationType");
        int offset = 0; // Initialiser à 0 si non spécifié
        int limit = 10; // Initialiser à une valeur par défaut si non spécifié
        String offsetParam = request.getParameter("offset");
        String limitParam = request.getParameter("limit");
        String orderBy = request.getParameter("orderBy");
        String orderDirection = request.getParameter("ascending");

        if (offsetParam != null && !offsetParam.isEmpty() && limitParam != null && !limitParam.isEmpty()) {
            try {
                offset = Integer.parseInt(offsetParam);
                limit = Integer.parseInt(limitParam);
            } catch (NumberFormatException e) {
                // Gérer l'exception si les paramètres ne sont pas des entiers valides
                e.printStackTrace();
            }
        }

        ArrayList<Licence> licenceListTab = new ArrayList<>();

        if (federation != null || localisation != null) {
            if (localisationType.equals("departement")) {
                localisation = licenceDao.getCodeDepartement(localisation);
            }
            // C'est une requête AJAX 
            licenceListTab = licenceDao.getLicencesByParametersOffset(federation, localisation, localisationType, offset, limit, orderBy, orderDirection);
            int[] tab = licenceDao.getLicencieSums(federation, localisation, localisationType);
            int nbLicencesTotal = tab[0];
            int nbLicencesHomme = tab[1];
            int nbLicencesFemme = tab[2];

            // Créer un objet JSON contenant les données et les valeurs calculées
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("nbLicencesTotal", nbLicencesTotal);
            jsonObject.addProperty("nbLicencesHomme", nbLicencesHomme);
            jsonObject.addProperty("nbLicencesFemme", nbLicencesFemme);
            jsonObject.add("licenceListTab", new Gson().toJsonTree(licenceListTab));
            // Envoyer les données au format JSON
            response.getWriter().write(jsonObject.toString());
        }
    }

    /**
     * Méthode doGet qui gère la requête GET pour obtenir les licences sportives filtrées et les statistiques associées.
     * 
     * @param request La requête HttpServletRequest.
     * @param response La réponse HttpServletResponse.
     * @throws ServletException Si une erreur de servlet survient.
     * @throws IOException Si une erreur d'entrée/sortie survient.
     * 
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId"); // Assurez-vous que l'utilisateur est connecté et a un userId valide
        Integer connected = (Integer) session.getAttribute("elu"); // Assurez-vous que l'utilisateur est connecté et a un userId valide

        if (connected == null) {
            response.sendRedirect("Authentification.jsp"); // Redirection vers la page de connexion si l'utilisateur n'est pas connecté ou si l'utilisateur n'a pas le bon userId
            return;
        }

        LicenceDAO licenceDao = new LicenceDAO();
        String federation = request.getParameter("federation");
        String localisation = request.getParameter("localisation");
        String localisationType = request.getParameter("localisationType");
        int offset = 0; // Initialiser à 0 si non spécifié
        int limit = 10; // Initialiser à une valeur par défaut si non spécifié
        String offsetParam = request.getParameter("offset");
        String limitParam = request.getParameter("limit");
        String orderBy = request.getParameter("orderBy");
        String orderDirection = request.getParameter("ascending");

        if (offsetParam != null && !offsetParam.isEmpty() && limitParam != null && !limitParam.isEmpty()) {
            try {
                offset = Integer.parseInt(offsetParam);
                limit = Integer.parseInt(limitParam);
            } catch (NumberFormatException e) {
                // Gérer l'exception si les paramètres ne sont pas des entiers valides
                e.printStackTrace();
            }
        }

        ArrayList<Licence> licenceList = new ArrayList<>();
        ArrayList<Licence> licenceListTab = new ArrayList<>();

        if (federation != null || localisation != null) {
            if (localisationType.equals("departement")) {
                localisation = licenceDao.getCodeDepartement(localisation);
            }
            // C'est une requête AJAX 
            licenceListTab = licenceDao.getLicencesByParametersOffset(federation, localisation, localisationType, offset, limit, orderBy, orderDirection);
            licenceList = licenceDao.getLicencesByParameters(federation, localisation, localisationType);
            int[] tab = licenceDao.getLicencieSums(federation, localisation, localisationType);
            int nbLicencesTotal = tab[0];
            int nbLicencesHomme = tab[1];
            int nbLicencesFemme = tab[2];

            // Créer un objet JSON contenant les données et les valeurs calculées
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("nbLicencesTotal", nbLicencesTotal);
            jsonObject.addProperty("nbLicencesHomme", nbLicencesHomme);
            jsonObject.addProperty("nbLicencesFemme", nbLicencesFemme);
            jsonObject.add("licenceListTab", new Gson().toJsonTree(licenceListTab));
            jsonObject.add("licenceList", new Gson().toJsonTree(licenceList));

            // Envoyer les données au format JSON
            response.getWriter().write(jsonObject.toString());
        } else {
            // C'est la charge initiale de la page, envoyer les fédérations et régions pour remplir les listes déroulantes
            List<String> regions = licenceDao.getUniqueRegions();
            List<String> departements = licenceDao.getUniqueDepartements();
            request.setAttribute("regions", regions);
            request.setAttribute("departements", departements);

            RequestDispatcher dispatcher = request.getRequestDispatcher("/elu.jsp");
            dispatcher.forward(request, response);
        }
    }
}
