package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.SQLException;

import org.owasp.encoder.Encode;

import dao.SpaceDAO;

/**
 * Servlet implementation class CreateSpaceServlet
 */
@WebServlet("/CreateSpaceServlet")
public class CreateSpaceServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateSpaceServlet() {
        super();
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");

        // Échapper les entrées pour éviter les injections XSS
        name = Encode.forHtml(name);
        description = Encode.forHtml(description);

        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId");
        Integer connected = (Integer) session.getAttribute("acteur");

        if (connected == null) {
            response.sendRedirect("Authentification.jsp"); // Redirection vers la page de connexion si l'utilisateur n'est pas connecté
            return;
        }

        int id_user = userId;
        SpaceDAO dao = new SpaceDAO();
        try {
            dao.insertSpace(id_user, name, description);
        } catch (SQLException e) {
            e.printStackTrace();
            response.sendRedirect("index.jsp"); // Redirection vers la page d'accueil après la création réussie
            return;
        }

        response.sendRedirect("HomeGServlet"); // Redirection vers la page d'accueil après la création réussie
    }
}
