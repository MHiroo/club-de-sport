package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.sql.SQLException;

import dao.SpaceDAO;

/**
 * Servlet implementation class UnsubscribeServlet
 */
@WebServlet("/UnsubscribeServlet")
public class UnsubscribeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UnsubscribeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Integer userId = (Integer) session.getAttribute("userId");
        Integer spaceId = Integer.parseInt(request.getParameter("spaceId"));

        if (userId != null) {
            SpaceDAO dao = new SpaceDAO();
            try {
                dao.unsubscribe(userId, spaceId);
                response.getWriter().write("{\"success\": true}");
            } catch (SQLException e) {
                e.printStackTrace();
                response.getWriter().write("{\"success\": false, \"message\": \"" + e.getMessage() + "\"}");
            }
        } else {
            response.getWriter().write("{\"success\": false, \"message\": \"Veuillez d'abord vous connecter !!\"}");
        }
	}

}
