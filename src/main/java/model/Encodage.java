package model;

import java.util.Base64;

/**
 * La classe Encodage fournit des méthodes utilitaires pour encoder des données en Base64.
 * 
 * Cette classe contient une méthode pour encoder une image représentée sous forme de tableau d'octets en une chaîne Base64.
 * 
 * @version 1.0
 */
public class Encodage {

    /**
     * Encode une image en Base64.
     * 
     * @param data Les données de l'image sous forme de tableau d'octets.
     * @return Une chaîne représentant l'image encodée en Base64.
     */
    public static String encodeImage(byte[] data) {
        return Base64.getEncoder().encodeToString(data);
    }
}
