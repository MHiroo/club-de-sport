package model;

import java.sql.Timestamp;

/**
 * La classe User représente un utilisateur avec ses informations de connexion et de profil.
 * 
 * Cette classe contient des informations telles que l'identifiant de l'utilisateur, 
 * le nom, le prénom, le mot de passe, l'email, le type d'utilisateur, le nombre de tentatives de connexion échouées,
 * la date de la dernière tentative échouée, la photo de profil et le statut de validation.
 * 
 * @version 1.0
 */
public class User {
    private int id;
    private String nom;
    private String prenom;
    private String password;
    private String email;
    private String userType;
    private int failedAttempts;
    private Timestamp lastFailedAttempt;
    private byte[] photoProfil;
    private int valider;

    /**
     * Constructeur par défaut.
     */
    public User() {
    }

    /**
     * Constructeur avec email et password pour authentification.
     * 
     * @param email    L'email de l'utilisateur.
     * @param password Le mot de passe de l'utilisateur.
     */
    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    /**
     * Constructeur avec tous les attributs de l'utilisateur sauf la photo de profil.
     * 
     * @param nom      Le nom de l'utilisateur.
     * @param prenom   Le prénom de l'utilisateur.
     * @param password Le mot de passe de l'utilisateur.
     * @param email    L'email de l'utilisateur.
     * @param userType Le type d'utilisateur.
     */
    public User(String nom, String prenom, String password, String email, String userType) {
        this.nom = nom;
        this.prenom = prenom;
        this.password = password;
        this.email = email;
        this.userType = userType;
    }

    /**
     * Constructeur avec tous les attributs de l'utilisateur.
     * 
     * @param nom         Le nom de l'utilisateur.
     * @param prenom      Le prénom de l'utilisateur.
     * @param password    Le mot de passe de l'utilisateur.
     * @param email       L'email de l'utilisateur.
     * @param userType    Le type d'utilisateur.
     * @param photoProfil La photo de profil de l'utilisateur.
     */
    public User(String nom, String prenom, String password, String email, String userType, byte[] photoProfil) {
        this.nom = nom;
        this.prenom = prenom;
        this.password = password;
        this.email = email;
        this.userType = userType;
        this.photoProfil = photoProfil;
    }

    // Getters et setters pour chaque attribut

    /**
     * @return l'identifiant de l'utilisateur.
     */
    public int getId() {
        return id;
    }

    /**
     * @param id l'identifiant de l'utilisateur à définir.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return le nom de l'utilisateur.
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom le nom de l'utilisateur à définir.
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return le prénom de l'utilisateur.
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom le prénom de l'utilisateur à définir.
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return le mot de passe de l'utilisateur.
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password le mot de passe de l'utilisateur à définir.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return l'email de l'utilisateur.
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email l'email de l'utilisateur à définir.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return le type d'utilisateur.
     */
    public String getUserType() {
        return userType;
    }

    /**
     * @param userType le type d'utilisateur à définir.
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * @return le nombre de tentatives de connexion échouées.
     */
    public int getFailedAttempts() {
        return failedAttempts;
    }

    /**
     * @param failedAttempts le nombre de tentatives de connexion échouées à définir.
     */
    public void setFailedAttempts(int failedAttempts) {
        this.failedAttempts = failedAttempts;
    }

    /**
     * @return la date de la dernière tentative de connexion échouée.
     */
    public Timestamp getLastFailedAttempt() {
        return lastFailedAttempt;
    }

    /**
     * @param lastFailedAttempt la date de la dernière tentative de connexion échouée à définir.
     */
    public void setLastFailedAttempt(Timestamp lastFailedAttempt) {
        this.lastFailedAttempt = lastFailedAttempt;
    }

    /**
     * @return la photo de profil de l'utilisateur.
     */
    public byte[] getPhotoProfil() {
        return photoProfil;
    }

    /**
     * @param photoProfil la photo de profil de l'utilisateur à définir.
     */
    public void setPhotoProfil(byte[] photoProfil) {
        this.photoProfil = photoProfil;
    }

    /**
     * @return le statut de validation de l'utilisateur.
     */
    public int getValider() {
        return valider;
    }

    /**
     * @param valider le statut de validation de l'utilisateur à définir.
     */
    public void setValider(int valider) {
        this.valider = valider;
    }
}
