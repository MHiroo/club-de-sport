package model;

import java.util.Date;

/**
 * La classe Historique représente l'historique de connexion d'un utilisateur.
 * 
 * Cette classe contient des informations telles que l'identifiant de l'historique, la date de connexion, 
 * l'adresse IP de l'utilisateur, le navigateur utilisé, l'identifiant de l'utilisateur, et le statut de la connexion.
 * 
 * @version 1.0
 */
public class Historique {

    private int id;
    private Date date_connexion;
    private String adresse_ip;
    private String navigateur;
    private int id_users;
    private String statut_connexion;

    /**
     * Constructeur de la classe Historique.
     * 
     * @param id                L'identifiant de l'historique.
     * @param date_connexion    La date de connexion.
     * @param adresse_ip        L'adresse IP de l'utilisateur.
     * @param navigateur        Le navigateur utilisé par l'utilisateur.
     * @param id_users          L'identifiant de l'utilisateur.
     * @param statut_connexion  Le statut de la connexion.
     */
    public Historique(int id, Date date_connexion, String adresse_ip, String navigateur, int id_users, String statut_connexion) {
        this.id = id;
        this.date_connexion = date_connexion;
        this.adresse_ip = adresse_ip;
        this.navigateur = navigateur;
        this.id_users = id_users;
        this.statut_connexion = statut_connexion;
    }

    // Getters et setters

    /**
     * @return l'identifiant de l'historique.
     */
    public int getId() {
        return id;
    }

    /**
     * @param id l'identifiant de l'historique à définir.
     */
    public void setIdConnexion(int id) {
        this.id = id;
    }

    /**
     * @return la date de connexion.
     */
    public Date getDateConnexion() {
        return date_connexion;
    }

    /**
     * @param date_connexion la date de connexion à définir.
     */
    public void setDateConnexion(Date date_connexion) {
        this.date_connexion = date_connexion;
    }

    /**
     * @return l'adresse IP de l'utilisateur.
     */
    public String getAdresseIP() {
        return adresse_ip;
    }

    /**
     * @param adresse_ip l'adresse IP de l'utilisateur à définir.
     */
    public void setAdresseIP(String adresse_ip) {
        this.adresse_ip = adresse_ip;
    }

    /**
     * @return le navigateur utilisé par l'utilisateur.
     */
    public String getNavigateur() {
        return navigateur;
    }

    /**
     * @param navigateur le navigateur utilisé par l'utilisateur à définir.
     */
    public void setNavigateur(String navigateur) {
        this.navigateur = navigateur;
    }

    /**
     * @return l'identifiant de l'utilisateur.
     */
    public int getId_users() {
        return id_users;
    }

    /**
     * @param id_users l'identifiant de l'utilisateur à définir.
     */
    public void setIdUtilisateur(int id_users) {
        this.id_users = id_users;
    }

    /**
     * @return le statut de la connexion.
     */
    public String getStatut() {
        return statut_connexion;
    }

    /**
     * @param statut_connexion le statut de la connexion à définir.
     */
    public void setStatut(String statut_connexion) {
        this.statut_connexion = statut_connexion;
    }
}
