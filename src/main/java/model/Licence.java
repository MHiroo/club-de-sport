package model;

/**
 * Modèle représentant une licence.
 * 
 * @author Hiroo MIZUNO
 * @version 1.0
 */
public class Licence {
	private int licenceId;
	private String codeCommune;
	private String commune;
	private String departement;
	private String region;
	private String statutGeo;
	private String code;
	private String federation;
	private int f_1_4_ans;
	private int f_5_9_ans;
	private int f_10_14_ans;
	private int f_15_19_ans;
	private int f_20_24_ans;
	private int f_25_29_ans;
	private int f_30_34_ans;
	private int f_35_39_ans;
	private int f_40_44_ans;
	private int f_45_49_ans;
	private int f_50_54_ans;
	private int f_55_59_ans;
	private int f_60_64_ans;
	private int f_65_69_ans;
	private int f_70_74_ans;
	private int f_75_79_ans;
	private int f_80_99_ans;
	private int f_NR;
	private int h_1_4_ans;
	private int h_5_9_ans;
	private int h_10_14_ans;
	private int h_15_19_ans;
	private int h_20_24_ans;
	private int h_25_29_ans;
	private int h_30_34_ans;
	private int h_35_39_ans;
	private int h_40_44_ans;
	private int h_45_49_ans;
	private int h_50_54_ans;
	private int h_55_59_ans;
	private int h_60_64_ans;
	private int h_65_69_ans;
	private int h_70_74_ans;
	private int h_75_79_ans;
	private int h_80_99_ans;
	private int h_NR;
	private int nr_NR;
	private int total;
    private int TotalFemme;
    private int TotalHomme;
    private int TotalLicencies;

	/**
	 * Constructeur par défaut.
	 */
	 public Licence() {
	}

	/**
	 * Constructeur avec tous les attributs.
	 * 
	 * @param codeCommune   Le code de la commune.
	 * @param commune       Le nom de la commune.
	 * @param departement   Le département.
	 * @param region        La région.
	 * @param statutGeo     Le statut géographique.
	 * @param code          Le code.
	 * @param federation    La fédération.
	 * @param f_1_4_ans     Nombre de licenciés femmes de 1 à 4 ans.
	 * @param f_5_9_ans     Nombre de licenciés femmes de 5 à 9 ans.
	 * @param f_10_14_ans   Nombre de licenciés femmes de 10 à 14 ans.
	 * @param f_15_19_ans   Nombre de licenciés femmes de 15 à 19 ans.
	 * @param f_20_24_ans   Nombre de licenciés femmes de 20 à 24 ans.
	 * @param f_25_29_ans   Nombre de licenciés femmes de 25 à 29 ans.
	 * @param f_30_34_ans   Nombre de licenciés femmes de 30 à 34 ans.
	 * @param f_35_39_ans   Nombre de licenciés femmes de 35 à 39 ans.
	 * @param f_40_44_ans   Nombre de licenciés femmes de 40 à 44 ans.
	 * @param f_45_49_ans   Nombre de licenciés femmes de 45 à 49 ans.
	 * @param f_50_54_ans   Nombre de licenciés femmes de 50 à 54 ans.
	 * @param f_55_59_ans   Nombre de licenciés femmes de 55 à 59 ans.
	 * @param f_60_64_ans   Nombre de licenciés femmes de 60 à 64 ans.
	 * @param f_65_69_ans   Nombre de licenciés femmes de 65 à 69 ans.
	 * @param f_70_74_ans   Nombre de licenciés femmes de 70 à 74 ans.
	 * @param f_75_79_ans   Nombre de licenciés femmes de 75 à 79 ans.
	 * @param f_80_99_ans   Nombre de licenciés femmes de 80 à 99 ans.
	 * @param f_NR          Nombre de licenciés femmes sans précision d'âge.
	 * @param h_1_4_ans     Nombre de licenciés hommes de 1 à 4 ans.
	 * @param h_5_9_ans     Nombre de licenciés hommes de 5 à 9 ans.
	 * @param h_10_14_ans   Nombre de licenciés hommes de 10 à 14 ans.
	 * @param h_15_19_ans   Nombre de licenciés hommes de 15 à 19 ans.
	 * @param h_20_24_ans   Nombre de licenciés hommes de 20 à 24 ans.
	 * @param h_25_29_ans   Nombre de licenciés hommes de 25 à 29 ans.
	 * @param h_30_34_ans   Nombre de licenciés hommes de 30 à 34 ans.
	 * @param h_35_39_ans   Nombre de licenciés hommes de 35 à 39 ans.
	 * @param h_40_44_ans   Nombre de licenciés hommes de 40 à 44 ans.
	 * @param h_45_49_ans   Nombre de licenciés hommes de 45 à 49 ans.
	 * @param h_50_54_ans   Nombre de licenciés hommes de 50 à 54 ans.
	 * @param h_55_59_ans   Nombre de licenciés hommes de 55 à 59 ans.
	 * @param h_60_64_ans   Nombre de licenciés hommes de 60 à 64 ans.
	 * @param h_65_69_ans   Nombre de licenciés hommes de 65 à 69 ans.
	 * @param h_70_74_ans   Nombre de licenciés hommes de 70 à 74 ans.
	 * @param h_75_79_ans   Nombre de licenciés hommes de 75 à 79 ans.
	 * @param h_80_99_ans   Nombre de licenciés hommes de 80 à 99 ans.
	 * @param h_NR          Nombre de licenciés hommes sans précision d'âge.
	 * @param nr_NR         Nombre de licenciés sans précision de sexe ou d'âge.
	 * @param total         Total des licenciés.
	 */
	 public Licence(String codeCommune, String commune, String departement, String region, String statutGeo, String code, String federation, int f_1_4_ans, int f_5_9_ans, int f_10_14_ans, int f_15_19_ans, int f_20_24_ans, int f_25_29_ans, int f_30_34_ans, int f_35_39_ans, int f_40_44_ans, int f_45_49_ans, int f_50_54_ans, int f_55_59_ans, int f_60_64_ans, int f_65_69_ans, int f_70_74_ans, int f_75_79_ans, int f_80_99_ans, int f_NR, int h_1_4_ans, int h_5_9_ans, int h_10_14_ans, int h_15_19_ans, int h_20_24_ans, int h_25_29_ans, int h_30_34_ans, int h_35_39_ans, int h_40_44_ans, int h_45_49_ans, int h_50_54_ans, int h_55_59_ans, int h_60_64_ans, int h_65_69_ans, int h_70_74_ans, int h_75_79_ans, int h_80_99_ans, int h_NR, int nr_NR, int total) {
		 this.setCodeCommune(codeCommune);
		 this.setCommune(commune);
		 this.setDepartement(departement);
		 this.setRegion(region);
		 this.setStatutGeo(statutGeo);
		 this.setCode(code);
		 this.setFederation(federation);
		 this.f_1_4_ans = f_1_4_ans;
		 this.f_5_9_ans = f_5_9_ans;
		 this.f_10_14_ans = f_10_14_ans;
		 this.f_15_19_ans = f_15_19_ans;
		 this.f_20_24_ans = f_20_24_ans;
		 this.f_25_29_ans = f_25_29_ans;
		 this.f_30_34_ans = f_30_34_ans;
		 this.f_35_39_ans = f_35_39_ans;
		 this.f_40_44_ans = f_40_44_ans;
		 this.f_45_49_ans = f_45_49_ans;
		 this.f_50_54_ans = f_50_54_ans;
		 this.f_55_59_ans = f_55_59_ans;
		 this.f_60_64_ans = f_60_64_ans;
		 this.f_65_69_ans = f_65_69_ans;
		 this.f_70_74_ans = f_70_74_ans;
		 this.f_75_79_ans = f_75_79_ans;
		 this.f_80_99_ans = f_80_99_ans;
		 this.f_NR = f_NR;
		 this.h_1_4_ans = h_1_4_ans;
		 this.h_5_9_ans = h_5_9_ans;
		 this.h_10_14_ans = h_10_14_ans;
		 this.h_15_19_ans = h_15_19_ans;
		 this.h_20_24_ans = h_20_24_ans;
		 this.h_25_29_ans = h_25_29_ans;
		 this.h_30_34_ans = h_30_34_ans;
		 this.h_35_39_ans = h_35_39_ans;
		 this.h_40_44_ans = h_40_44_ans;
		 this.h_45_49_ans = h_45_49_ans;
		 this.h_50_54_ans = h_50_54_ans;
		 this.h_55_59_ans = h_55_59_ans;
		 this.h_60_64_ans = h_60_64_ans;
		 this.h_65_69_ans = h_65_69_ans;
		 this.h_70_74_ans = h_70_74_ans;
		 this.h_75_79_ans = h_75_79_ans;
		 this.h_80_99_ans = h_80_99_ans;
		 this.h_NR = h_NR;
		 this.nr_NR = nr_NR;
		 this.total = total;
	 }
	 
	 public Licence(String commune, String departement, String region, String federation, int f_1_4_ans, int f_5_9_ans, int f_10_14_ans, int f_15_19_ans, int f_20_24_ans, int f_25_29_ans, int f_30_34_ans, int f_35_39_ans, int f_40_44_ans, int f_45_49_ans, int f_50_54_ans, int f_55_59_ans, int f_60_64_ans, int f_65_69_ans, int f_70_74_ans, int f_75_79_ans, int f_80_99_ans, int f_NR, int h_1_4_ans, int h_5_9_ans, int h_10_14_ans, int h_15_19_ans, int h_20_24_ans, int h_25_29_ans, int h_30_34_ans, int h_35_39_ans, int h_40_44_ans, int h_45_49_ans, int h_50_54_ans, int h_55_59_ans, int h_60_64_ans, int h_65_69_ans, int h_70_74_ans, int h_75_79_ans, int h_80_99_ans, int h_NR, int nr_NR, int total) {
		 this.setCommune(commune);
		 this.setDepartement(departement);
		 this.setRegion(region);
		 this.setFederation(federation);
		 this.f_1_4_ans = f_1_4_ans;
		 this.f_5_9_ans = f_5_9_ans;
		 this.f_10_14_ans = f_10_14_ans;
		 this.f_15_19_ans = f_15_19_ans;
		 this.f_20_24_ans = f_20_24_ans;
		 this.f_25_29_ans = f_25_29_ans;
		 this.f_30_34_ans = f_30_34_ans;
		 this.f_35_39_ans = f_35_39_ans;
		 this.f_40_44_ans = f_40_44_ans;
		 this.f_45_49_ans = f_45_49_ans;
		 this.f_50_54_ans = f_50_54_ans;
		 this.f_55_59_ans = f_55_59_ans;
		 this.f_60_64_ans = f_60_64_ans;
		 this.f_65_69_ans = f_65_69_ans;
		 this.f_70_74_ans = f_70_74_ans;
		 this.f_75_79_ans = f_75_79_ans;
		 this.f_80_99_ans = f_80_99_ans;
		 this.f_NR = f_NR;
		 this.h_1_4_ans = h_1_4_ans;
		 this.h_5_9_ans = h_5_9_ans;
		 this.h_10_14_ans = h_10_14_ans;
		 this.h_15_19_ans = h_15_19_ans;
		 this.h_20_24_ans = h_20_24_ans;
		 this.h_25_29_ans = h_25_29_ans;
		 this.h_30_34_ans = h_30_34_ans;
		 this.h_35_39_ans = h_35_39_ans;
		 this.h_40_44_ans = h_40_44_ans;
		 this.h_45_49_ans = h_45_49_ans;
		 this.h_50_54_ans = h_50_54_ans;
		 this.h_55_59_ans = h_55_59_ans;
		 this.h_60_64_ans = h_60_64_ans;
		 this.h_65_69_ans = h_65_69_ans;
		 this.h_70_74_ans = h_70_74_ans;
		 this.h_75_79_ans = h_75_79_ans;
		 this.h_80_99_ans = h_80_99_ans;
		 this.h_NR = h_NR;
		 this.nr_NR = nr_NR;
		 this.total = total;
	 }

	 public Licence( int f_1_4_ans, int f_5_9_ans, int f_10_14_ans, int f_15_19_ans, int f_20_24_ans, int f_25_29_ans, int f_30_34_ans, int f_35_39_ans, int f_40_44_ans, int f_45_49_ans, int f_50_54_ans, int f_55_59_ans, int f_60_64_ans, int f_65_69_ans, int f_70_74_ans, int f_75_79_ans, int f_80_99_ans, int f_NR, int h_1_4_ans, int h_5_9_ans, int h_10_14_ans, int h_15_19_ans, int h_20_24_ans, int h_25_29_ans, int h_30_34_ans, int h_35_39_ans, int h_40_44_ans, int h_45_49_ans, int h_50_54_ans, int h_55_59_ans, int h_60_64_ans, int h_65_69_ans, int h_70_74_ans, int h_75_79_ans, int h_80_99_ans, int h_NR, int total) {
		 this.f_1_4_ans = f_1_4_ans;
		 this.f_5_9_ans = f_5_9_ans;
		 this.f_10_14_ans = f_10_14_ans;
		 this.f_15_19_ans = f_15_19_ans;
		 this.f_20_24_ans = f_20_24_ans;
		 this.f_25_29_ans = f_25_29_ans;
		 this.f_30_34_ans = f_30_34_ans;
		 this.f_35_39_ans = f_35_39_ans;
		 this.f_40_44_ans = f_40_44_ans;
		 this.f_45_49_ans = f_45_49_ans;
		 this.f_50_54_ans = f_50_54_ans;
		 this.f_55_59_ans = f_55_59_ans;
		 this.f_60_64_ans = f_60_64_ans;
		 this.f_65_69_ans = f_65_69_ans;
		 this.f_70_74_ans = f_70_74_ans;
		 this.f_75_79_ans = f_75_79_ans;
		 this.f_80_99_ans = f_80_99_ans;
		 this.f_NR = f_NR;
		 this.h_1_4_ans = h_1_4_ans;
		 this.h_5_9_ans = h_5_9_ans;
		 this.h_10_14_ans = h_10_14_ans;
		 this.h_15_19_ans = h_15_19_ans;
		 this.h_20_24_ans = h_20_24_ans;
		 this.h_25_29_ans = h_25_29_ans;
		 this.h_30_34_ans = h_30_34_ans;
		 this.h_35_39_ans = h_35_39_ans;
		 this.h_40_44_ans = h_40_44_ans;
		 this.h_45_49_ans = h_45_49_ans;
		 this.h_50_54_ans = h_50_54_ans;
		 this.h_55_59_ans = h_55_59_ans;
		 this.h_60_64_ans = h_60_64_ans;
		 this.h_65_69_ans = h_65_69_ans;
		 this.h_70_74_ans = h_70_74_ans;
		 this.h_75_79_ans = h_75_79_ans;
		 this.h_80_99_ans = h_80_99_ans;
		 this.h_NR = h_NR;
		 this.total = total;
	}

	// Getters et setters
	 /**
	  * @return the licenceId
	  */
	 public int getLicenceId() {
		 return licenceId;
	 }

	 /**
	  * @param licenceId the licenceId to set
	  */
	 public void setLicenceId(int licenceId) {
		 this.licenceId = licenceId;
	 }

	 /**
	  * @return the codeCommune
	  */
	 public String getCodeCommune() {
		 return codeCommune;
	 }

	 /**
	  * @param codeCommune the codeCommune to set
	  */
	 public void setCodeCommune(String codeCommune) {
		 this.codeCommune = codeCommune;
	 }

	 /**
	  * @return the commune
	  */
	 public String getCommune() {
		 return commune;
	 }

	 /**
	  * @param commune the commune to set
	  */
	 public void setCommune(String commune) {
		 this.commune = commune;
	 }

	 /**
	  * @return the departement
	  */
	 public String getDepartement() {
		 return departement;
	 }

	 /**
	  * @param departement the departement to set
	  */
	 public void setDepartement(String departement) {
		 this.departement = departement;
	 }

	 /**
	  * @return the region
	  */
	 public String getRegion() {
		 return region;
	 }

	 /**
	  * @param region the region to set
	  */
	 public void setRegion(String region) {
		 this.region = region;
	 }

	 /**
	  * @return the statutGeo
	  */
	 public String getStatutGeo() {
		 return statutGeo;
	 }

	 /**
	  * @param statutGeo the statutGeo to set
	  */
	 public void setStatutGeo(String statutGeo) {
		 this.statutGeo = statutGeo;
	 }

	 /**
	  * @return the code
	  */
	 public String getCode() {
		 return code;
	 }

	 /**
	  * @param code the code to set
	  */
	 public void setCode(String code) {
		 this.code = code;
	 }

	 /**
	  * @return the federation
	  */
	 public String getFederation() {
		 return federation;
	 }

	 /**
	  * @param federation the federation to set
	  */
	 public void setFederation(String federation) {
		 this.federation = federation;
	 }
	 /**
	  * @return the f_1_4_ans
	  */
	 public int getF_1_4_ans() {
		 return f_1_4_ans;
	 }

	 /**
	  * @param f_1_4_ans the f_1_4_ans to set
	  */
	 public void setF_1_4_ans(int f_1_4_ans) {
		 this.f_1_4_ans = f_1_4_ans;
	 }

	 /**
	  * @return the f_5_9_ans
	  */
	 public int getF_5_9_ans() {
		 return f_5_9_ans;
	 }

	 /**
	  * @param f_5_9_ans the f_5_9_ans to set
	  */
	 public void setF_5_9_ans(int f_5_9_ans) {
		 this.f_5_9_ans = f_5_9_ans;
	 }

	 /**
	  * @return the f_10_14_ans
	  */
	 public int getF_10_14_ans() {
		 return f_10_14_ans;
	 }

	 /**
	  * @param f_10_14_ans the f_10_14_ans to set
	  */
	 public void setF_10_14_ans(int f_10_14_ans) {
		 this.f_10_14_ans = f_10_14_ans;
	 }

	 /**
	  * @return the f_15_19_ans
	  */
	 public int getF_15_19_ans() {
		 return f_15_19_ans;
	 }

	 /**
	  * @param f_15_19_ans the f_15_19_ans to set
	  */
	 public void setF_15_19_ans(int f_15_19_ans) {
		 this.f_15_19_ans = f_15_19_ans;
	 }

	 /**
	  * @return the f_20_24_ans
	  */
	 public int getF_20_24_ans() {
		 return f_20_24_ans;
	 }

	 /**
	  * @param f_20_24_ans the f_20_24_ans to set
	  */
	 public void setF_20_24_ans(int f_20_24_ans) {
		 this.f_20_24_ans = f_20_24_ans;
	 }

	 /**
	  * @return the f_25_29_ans
	  */
	 public int getF_25_29_ans() {
		 return f_25_29_ans;
	 }

	 /**
	  * @param f_25_29_ans the f_25_29_ans to set
	  */
	 public void setF_25_29_ans(int f_25_29_ans) {
		 this.f_25_29_ans = f_25_29_ans;
	 }

	 /**
	  * @return the f_30_34_ans
	  */
	 public int getF_30_34_ans() {
		 return f_30_34_ans;
	 }

	 /**
	  * @param f_30_34_ans the f_30_34_ans to set
	  */
	 public void setF_30_34_ans(int f_30_34_ans) {
		 this.f_30_34_ans = f_30_34_ans;
	 }

	 /**
	  * @return the f_35_39_ans
	  */
	 public int getF_35_39_ans() {
		 return f_35_39_ans;
	 }

	 /**
	  * @param f_35_39_ans the f_35_39_ans to set
	  */
	 public void setF_35_39_ans(int f_35_39_ans) {
		 this.f_35_39_ans = f_35_39_ans;
	 }

	 /**
	  * @return the f_40_44_ans
	  */
	 public int getF_40_44_ans() {
		 return f_40_44_ans;
	 }

	 /**
	  * @param f_40_44_ans the f_40_44_ans to set
	  */
	 public void setF_40_44_ans(int f_40_44_ans) {
		 this.f_40_44_ans = f_40_44_ans;
	 }

	 /**
	  * @return the f_45_49_ans
	  */
	 public int getF_45_49_ans() {
		 return f_45_49_ans;
	 }

	 /**
	  * @param f_45_49_ans the f_45_49_ans to set
	  */
	 public void setF_45_49_ans(int f_45_49_ans) {
		 this.f_45_49_ans = f_45_49_ans;
	 }

	 /**
	  * @return the f_50_54_ans
	  */
	 public int getF_50_54_ans() {
		 return f_50_54_ans;
	 }

	 /**
	  * @param f_50_54_ans the f_50_54_ans to set
	  */
	 public void setF_50_54_ans(int f_50_54_ans) {
		 this.f_50_54_ans = f_50_54_ans;
	 }

	 /**
	  * @return the f_55_59_ans
	  */
	 public int getF_55_59_ans() {
		 return f_55_59_ans;
	 }

	 /**
	  * @param f_55_59_ans the f_55_59_ans to set
	  */
	 public void setF_55_59_ans(int f_55_59_ans) {
		 this.f_55_59_ans = f_55_59_ans;
	 }

	 /**
	  * @return the f_60_64_ans
	  */
	 public int getF_60_64_ans() {
		 return f_60_64_ans;
	 }

	 /**
	  * @param f_60_64_ans the f_60_64_ans to set
	  */
	 public void setF_60_64_ans(int f_60_64_ans) {
		 this.f_60_64_ans = f_60_64_ans;
	 }

	 /**
	  * @return the f_65_69_ans
	  */
	 public int getF_65_69_ans() {
		 return f_65_69_ans;
	 }

	 /**
	  * @param f_65_69_ans the f_65_69_ans to set
	  */
	 public void setF_65_69_ans(int f_65_69_ans) {
		 this.f_65_69_ans = f_65_69_ans;
	 }

	 /**
	  * @return the f_70_74_ans
	  */
	 public int getF_70_74_ans() {
		 return f_70_74_ans;
	 }

	 /**
	  * @param f_70_74_ans the f_70_74_ans to set
	  */
	 public void setF_70_74_ans(int f_70_74_ans) {
		 this.f_70_74_ans = f_70_74_ans;
	 }

	 /**
	  * @return the f_75_79_ans
	  */
	 public int getF_75_79_ans() {
		 return f_75_79_ans;
	 }

	 /**
	  * @param f_75_79_ans the f_75_79_ans to set
	  */
	 public void setF_75_79_ans(int f_75_79_ans) {
		 this.f_75_79_ans = f_75_79_ans;
	 }

	 /**
	  * @return the f_80_99_ans
	  */
	 public int getF_80_99_ans() {
		 return f_80_99_ans;
	 }

	 /**
	  * @param f_80_99_ans the f_80_99_ans to set
	  */
	 public void setF_80_99_ans(int f_80_99_ans) {
		 this.f_80_99_ans = f_80_99_ans;
	 }

	 /**
	  * @return the f_NR
	  */
	 public int getF_NR() {
		 return f_NR;
	 }

	 /**
	  * @param f_NR the f_NR to set
	  */
	 public void setF_NR(int f_NR) {
		 this.f_NR = f_NR;
	 }

	 /**
	  * @return the h_1_4_ans
	  */
	 public int getH_1_4_ans() {
		 return h_1_4_ans;
	 }

	 /**
	  * @param h_1_4_ans the h_1_4_ans to set
	  */
	 public void setH_1_4_ans(int h_1_4_ans) {
		 this.h_1_4_ans = h_1_4_ans;
	 }

	 /**
	  * @return the h_5_9_ans
	  */
	 public int getH_5_9_ans() {
		 return h_5_9_ans;
	 }

	 /**
	  * @param h_5_9_ans the h_5_9_ans to set
	  */
	 public void setH_5_9_ans(int h_5_9_ans) {
		 this.h_5_9_ans = h_5_9_ans;
	 }

	 /**
	  * @return the h_10_14_ans
	  */
	 public int getH_10_14_ans() {
		 return h_10_14_ans;
	 }

	 /**
	  * @param h_10_14_ans the h_10_14_ans to set
	  */
	 public void setH_10_14_ans(int h_10_14_ans) {
		 this.h_10_14_ans = h_10_14_ans;
	 }

	 /**
	  * @return the h_15_19_ans
	  */
	 public int getH_15_19_ans() {
		 return h_15_19_ans;
	 }

	 /**
	  * @param h_15_19_ans the h_15_19_ans to set
	  */
	 public void setH_15_19_ans(int h_15_19_ans) {
		 this.h_15_19_ans = h_15_19_ans;
	 }

	 /**
	  * @return the h_20_24_ans
	  */
	 public int getH_20_24_ans() {
		 return h_20_24_ans;
	 }

	 /**
	  * @param h_20_24_ans the h_20_24_ans to set
	  */
	 public void setH_20_24_ans(int h_20_24_ans) {
		 this.h_20_24_ans = h_20_24_ans;
	 }

	 /**
	  * @return the h_25_29_ans
	  */
	 public int getH_25_29_ans() {
		 return h_25_29_ans;
	 }

	 /**
	  * @param h_25_29_ans the h_25_29_ans to set
	  */
	 public void setH_25_29_ans(int h_25_29_ans) {
		 this.h_25_29_ans = h_25_29_ans;
	 }

	 /**
	  * @return the h_30_34_ans
	  */
	 public int getH_30_34_ans() {
		 return h_30_34_ans;
	 }

	 /**
	  * @param h_30_34_ans the h_30_34_ans to set
	  */
	 public void setH_30_34_ans(int h_30_34_ans) {
		 this.h_30_34_ans = h_30_34_ans;
	 }

	 /**
	  * @return the h_35_39_ans
	  */
	 public int getH_35_39_ans() {
		 return h_35_39_ans;
	 }

	 /**
	  * @param h_35_39_ans the h_35_39_ans to set
	  */
	 public void setH_35_39_ans(int h_35_39_ans) {
		 this.h_35_39_ans = h_35_39_ans;
	 }

	 /**
	  * @return the h_40_44_ans
	  */
	 public int getH_40_44_ans() {
		 return h_40_44_ans;
	 }

	 /**
	  * @param h_40_44_ans the h_40_44_ans to set
	  */
	 public void setH_40_44_ans(int h_40_44_ans) {
		 this.h_40_44_ans = h_40_44_ans;
	 }

	 /**
	  * @return the h_45_49_ans
	  */
	 public int getH_45_49_ans() {
		 return h_45_49_ans;
	 }

	 /**
	  * @param h_45_49_ans the h_45_49_ans to set
	  */
	 public void setH_45_49_ans(int h_45_49_ans) {
		 this.h_45_49_ans = h_45_49_ans;
	 }

	 /**
	  * @return the h_50_54_ans
	  */
	 public int getH_50_54_ans() {
		 return h_50_54_ans;
	 }

	 /**
	  * @param h_50_54_ans the h_50_54_ans to set
	  */
	 public void setH_50_54_ans(int h_50_54_ans) {
		 this.h_50_54_ans = h_50_54_ans;
	 }

	 /**
	  * @return the h_55_59_ans
	  */
	 public int getH_55_59_ans() {
		 return h_55_59_ans;
	 }

	 /**
	  * @param h_55_59_ans the h_55_59_ans to set
	  */
	 public void setH_55_59_ans(int h_55_59_ans) {
		 this.h_55_59_ans = h_55_59_ans;
	 }

	 /**
	  * @return the h_60_64_ans
	  */
	 public int getH_60_64_ans() {
		 return h_60_64_ans;
	 }

	 /**
	  * @param h_60_64_ans the h_60_64_ans to set
	  */
	 public void setH_60_64_ans(int h_60_64_ans) {
		 this.h_60_64_ans = h_60_64_ans;
	 }

	 /**
	  * @return the h_65_69_ans
	  */
	 public int getH_65_69_ans() {
		 return h_65_69_ans;
	 }

	 /**
	  * @param h_65_69_ans the h_65_69_ans to set
	  */
	 public void setH_65_69_ans(int h_65_69_ans) {
		 this.h_65_69_ans = h_65_69_ans;
	 }

	 /**
	  * @return the h_70_74_ans
	  */
	 public int getH_70_74_ans() {
		 return h_70_74_ans;
	 }

	 /**
	  * @param h_70_74_ans the h_70_74_ans to set
	  */
	 public void setH_70_74_ans(int h_70_74_ans) {
		 this.h_70_74_ans = h_70_74_ans;
	 }

	 /**
	  * @return the h_75_79_ans
	  */
	 public int getH_75_79_ans() {
		 return h_75_79_ans;
	 }

	 /**
	  * @param h_75_79_ans the h_75_79_ans to set
	  */
	 public void setH_75_79_ans(int h_75_79_ans) {
		 this.h_75_79_ans = h_75_79_ans;
	 }

	 /**
	  * @return the h_80_99_ans
	  */
	 public int getH_80_99_ans() {
		 return h_80_99_ans;
	 }

	 /**
	  * @param h_80_99_ans the h_80_99_ans to set
	  */
	 public void setH_80_99_ans(int h_80_99_ans) {
		 this.h_80_99_ans = h_80_99_ans;
	 }

	 /**
	  * @return the h_NR
	  */
	 public int getH_NR() {
		 return h_NR;
	 }

	 /**
	  * @param h_NR the h_NR to set
	  */
	 public void setH_NR(int h_NR) {
		 this.h_NR = h_NR;
	 }

	 /**
	  * @return the nr_NR
	  */
	 public int getNr_NR() {
		 return nr_NR;
	 }

	 /**
	  * @param nr_NR the nr_NR to set
	  */
	 public void setNr_NR(int nr_NR) {
		 this.nr_NR = nr_NR;
	 }

	 /**
	  * @return the total
	  */
	 public int getTotal() {
		 return total;
	 }

	 /**
	  * @param total the total to set
	  */
	 public void setTotal(int total) {
		 this.total = total;
	 }

	/**
	 * @return the totalFemme
	 */
	public int getTotalFemme() {
		return TotalFemme;
	}

	/**
	 * @param totalFemme the totalFemme to set
	 */
	public void setTotalFemme(int totalFemme) {
		TotalFemme = totalFemme;
	}

	/**
	 * @return the totalHomme
	 */
	public int getTotalHomme() {
		return TotalHomme;
	}

	/**
	 * @param totalHomme the totalHomme to set
	 */
	public void setTotalHomme(int totalHomme) {
		TotalHomme = totalHomme;
	}

	/**
	 * @return the totalLicencies
	 */
	public int getTotalLicencies() {
		return TotalLicencies;
	}

	/**
	 * @param totalLicencies the totalLicencies to set
	 */
	public void setTotalLicencies(int totalLicencies) {
		TotalLicencies = totalLicencies;
	}

}
