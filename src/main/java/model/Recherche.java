package model;

import java.util.Date;

/**
 * La classe Recherche représente une recherche effectuée par un utilisateur.
 * 
 * Cette classe contient des informations telles que l'identifiant de la recherche, la date de la recherche, 
 * l'adresse IP de l'utilisateur, la région, le code postal, et la fédération recherchée.
 * 
 * @version 1.0
 */
public class Recherche {

    private int idrecherche;
    private Date date;
    private String adresseip;
    private String region;
    private String codePostal;
    private String federation;

    /**
     * Constructeur de la classe Recherche.
     * 
     * @param date       La date de la recherche.
     * @param adresseip  L'adresse IP de l'utilisateur ayant effectué la recherche.
     * @param region     La région recherchée.
     * @param codePostal Le code postal recherché.
     * @param federation La fédération recherchée.
     */
    public Recherche(Date date, String adresseip, String region, String codePostal, String federation) {
        this.idrecherche = idrecherche;
        this.date = date;
        this.adresseip = adresseip;
        this.region = region;
        this.codePostal = codePostal;
        this.federation = federation;
    }

    // Getters et setters

    /**
     * @return l'identifiant de la recherche.
     */
    public int getId() {
        return idrecherche;
    }

    /**
     * @param idrecherche l'identifiant de la recherche à définir.
     */
    public void setIdConnexion(int idrecherche) {
        this.idrecherche = idrecherche;
    }

    /**
     * @return la date de la recherche.
     */
    public Date getDateConnexion() {
        return date;
    }

    /**
     * @param date la date de la recherche à définir.
     */
    public void setDateConnexion(Date date) {
        this.date = date;
    }

    /**
     * @return l'adresse IP de l'utilisateur ayant effectué la recherche.
     */
    public String getadresseip() {
        return adresseip;
    }

    /**
     * @param adresseip l'adresse IP de l'utilisateur à définir.
     */
    public void setadresseip(String adresseip) {
        this.adresseip = adresseip;
    }

    /**
     * @return la région recherchée.
     */
    public String getregion() {
        return region;
    }

    /**
     * @param region la région à définir.
     */
    public void setregion(String region) {
        this.region = region;
    }

    /**
     * @return le code postal recherché.
     */
    public String getcodePostal() {
        return codePostal;
    }

    /**
     * @param codePostal le code postal à définir.
     */
    public void setcodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * @return la fédération recherchée.
     */
    public String getfederation() {
        return federation;
    }

    /**
     * @param federation la fédération à définir.
     */
    public void setfederation(String federation) {
        this.federation = federation;
    }
}
