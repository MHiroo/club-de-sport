package model;

import java.sql.Timestamp;

/**
 * La classe Comment représente un commentaire fait sur un post.
 * 
 * Cette classe contient des informations telles que l'identifiant du commentaire,
 * l'identifiant du post auquel il appartient, l'identifiant de l'utilisateur qui l'a fait,
 * le nom d'utilisateur, le contenu du commentaire, et la date de création du commentaire.
 * 
 * @version 1.0
 */
public class Comment {
    private int commentId;
    private int postId;
    private int userId;
    private String username;
    private String content;
    private Timestamp createdAt;

    /**
     * @return l'identifiant du commentaire.
     */
    public int getCommentId() {
        return commentId;
    }

    /**
     * @param commentId l'identifiant du commentaire à définir.
     */
    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    /**
     * @return l'identifiant du post auquel le commentaire appartient.
     */
    public int getPostId() {
        return postId;
    }

    /**
     * @param postId l'identifiant du post à définir pour le commentaire.
     */
    public void setPostId(int postId) {
        this.postId = postId;
    }

    /**
     * @return l'identifiant de l'utilisateur qui a fait le commentaire.
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId l'identifiant de l'utilisateur à définir pour le commentaire.
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return le nom d'utilisateur qui a fait le commentaire.
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username le nom d'utilisateur à définir pour le commentaire.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return le contenu du commentaire.
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content le contenu du commentaire à définir.
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return la date de création du commentaire.
     */
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt la date de création du commentaire à définir.
     */
    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }
}
