package model;

/**
 * La classe Space représente un espace ou une zone avec des détails tels que le nom, la description, et des informations sur le propriétaire.
 * 
 * Cette classe contient des informations telles que l'identifiant de l'espace, le nom, la description, le nom et le prénom du propriétaire,
 * la ville, et la fédération à laquelle l'espace appartient.
 * 
 * @version 1.0
 */
public class Space {
    private int spaceId;
    private String name;
    private String description;
    private String nom;
    private String prenom;
    private String ville;
    private String federation;

    /**
     * Constructeur de la classe Space avec les paramètres de base.
     * 
     * @param spaceId L'identifiant de l'espace.
     * @param name Le nom de l'espace.
     * @param description La description de l'espace.
     */
    public Space(int spaceId, String name, String description) {
        this.spaceId = spaceId;
        this.name = name;
        this.description = description;
    }

    /**
     * Constructeur de la classe Space avec tous les attributs.
     * 
     * @param spaceId L'identifiant de l'espace.
     * @param name Le nom de l'espace.
     * @param description La description de l'espace.
     * @param nom Le nom du propriétaire.
     * @param prenom Le prénom du propriétaire.
     * @param federation La fédération à laquelle appartient l'espace.
     * @param ville La ville où se trouve l'espace.
     */
    public Space(int spaceId, String name, String description, String nom, String prenom, String federation, String ville) {
        this.spaceId = spaceId;
        this.name = name;
        this.description = description;
        this.nom = nom;
        this.prenom = prenom;
        this.ville = ville;
        this.federation = federation;
    }

    // Getters

    /**
     * @return l'identifiant de l'espace.
     */
    public int getSpaceId() {
        return spaceId;
    }

    /**
     * @return le nom de l'espace.
     */
    public String getName() {
        return name;
    }

    /**
     * @return la description de l'espace.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return le nom du propriétaire de l'espace.
     */
    public String getNom() {
        return nom;
    }

    /**
     * @return le prénom du propriétaire de l'espace.
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @return la ville où se trouve l'espace.
     */
    public String getVille() {
        return ville;
    }

    /**
     * @return la fédération à laquelle appartient l'espace.
     */
    public String getFederation() {
        return federation;
    }

    // Setters

    /**
     * @param spaceId l'identifiant de l'espace à définir.
     */
    public void setSpaceId(int spaceId) {
        this.spaceId = spaceId;
    }

    /**
     * @param name le nom de l'espace à définir.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param description la description de l'espace à définir.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param nom le nom du propriétaire à définir.
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @param prenom le prénom du propriétaire à définir.
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @param ville la ville où se trouve l'espace à définir.
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * @param federation la fédération à laquelle appartient l'espace à définir.
     */
    public void setFederation(String federation) {
        this.federation = federation;
    }
}
