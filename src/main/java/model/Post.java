package model;

import java.util.List;

/**
 * La classe Post représente un message ou une publication dans un espace.
 * 
 * Cette classe contient des informations telles que l'identifiant de l'espace, l'identifiant du message,
 * le titre, le contenu, la photo, les dates de création et de mise à jour, le nombre de "likes", et les commentaires associés.
 * 
 * @version 1.0
 */
public class Post {
    private int spaceId;
    private int postId;
    private String title;
    private String content;
    private byte[] photo; // Les images sont généralement stockées sous forme de tableau de bytes
    private java.sql.Timestamp createdAt;
    private java.sql.Timestamp updatedAt;
    private int likes;
    private List<Comment> comments;

    /**
     * Constructeur sans paramètres.
     */
    public Post() {
    }

    /**
     * Constructeur avec tous les paramètres.
     * 
     * @param postId     L'identifiant du message.
     * @param title      Le titre du message.
     * @param content    Le contenu du message.
     * @param photo      La photo associée au message.
     * @param createdAt  La date de création du message.
     * @param updatedAt  La date de mise à jour du message.
     */
    public Post(int postId, String title, String content, byte[] photo, java.sql.Timestamp createdAt, java.sql.Timestamp updatedAt) {
        this.postId = postId;
        this.title = title;
        this.content = content;
        this.photo = photo;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    // Getters et Setters

    /**
     * @return l'identifiant du message.
     */
    public int getPostId() {
        return postId;
    }

    /**
     * @param postId l'identifiant du message à définir.
     */
    public void setPostId(int postId) {
        this.postId = postId;
    }

    /**
     * @return le titre du message.
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title le titre du message à définir.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return le contenu du message.
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content le contenu du message à définir.
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return la photo associée au message.
     */
    public byte[] getPhoto() {
        return photo;
    }

    /**
     * @param photo la photo associée au message à définir.
     */
    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    /**
     * @return la date de création du message.
     */
    public java.sql.Timestamp getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt la date de création du message à définir.
     */
    public void setCreatedAt(java.sql.Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return la date de mise à jour du message.
     */
    public java.sql.Timestamp getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @param updatedAt la date de mise à jour du message à définir.
     */
    public void setUpdatedAt(java.sql.Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * @return l'identifiant de l'espace.
     */
    public int getSpaceId() {
        return spaceId;
    }

    /**
     * @param spaceId l'identifiant de l'espace à définir.
     */
    public void setSpaceId(int spaceId) {
        this.spaceId = spaceId;
    }

    /**
     * @return le nombre de "likes" du message.
     */
    public int getLikes() {
        return likes;
    }

    /**
     * @param likes le nombre de "likes" du message à définir.
     */
    public void setLikes(int likes) {
        this.likes = likes;
    }

    /**
     * @return la liste des commentaires associés au message.
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * @param comments la liste des commentaires associés au message à définir.
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
