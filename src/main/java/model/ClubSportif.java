package model;

/**
 * La classe ClubSportif représente un club sportif avec ses détails géographiques et administratifs.
 * 
 * Cette classe contient des informations telles que l'identifiant de l'établissement, 
 * la commune, le département, la région, la fédération, le code postal, la latitude et la longitude.
 * 
 * @version 1.0
 */
public class ClubSportif {
    private int etablissementId;
    private String commune;
    private String departement;
    private String region;
    private String federation;
    private int total;
    private String CodePostal;
    private float latitude;
    private float longitude;

    /**
     * Constructeur par défaut.
     */
    public ClubSportif() {
        // Constructeur vide
    }

    /**
     * @return l'identifiant de l'établissement.
     */
    public int getEtablissementId() {
        return etablissementId;
    }

    /**
     * @param etablissementId l'identifiant de l'établissement à définir.
     */
    public void setEtablissementId(int etablissementId) {
        this.etablissementId = etablissementId;
    }

    /**
     * @return la commune du club sportif.
     */
    public String getCommune() {
        return commune;
    }

    /**
     * @param commune la commune à définir pour le club sportif.
     */
    public void setCommune(String commune) {
        this.commune = commune;
    }

    /**
     * @return le département du club sportif.
     */
    public String getDepartement() {
        return departement;
    }

    /**
     * @param departement le département à définir pour le club sportif.
     */
    public void setDepartement(String departement) {
        this.departement = departement;
    }

    /**
     * @return la région du club sportif.
     */
    public String getRegion() {
        return region;
    }

    /**
     * @param region la région à définir pour le club sportif.
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * @return la fédération à laquelle appartient le club sportif.
     */
    public String getFederation() {
        return federation;
    }

    /**
     * @param federation la fédération à définir pour le club sportif.
     */
    public void setFederation(String federation) {
        this.federation = federation;
    }

    /**
     * @return le nombre total de membres du club sportif.
     */
    public int getTotal() {
        return total;
    }

    /**
     * @param total le nombre total de membres à définir pour le club sportif.
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * @return le code postal du club sportif.
     */
    public String getCodePostal() {
        return CodePostal;
    }

    /**
     * @param codePostal le code postal à définir pour le club sportif.
     */
    public void setCodePostal(String codePostal) {
        CodePostal = codePostal;
    }

    /**
     * @return la latitude de l'emplacement du club sportif.
     */
    public float getLatitude() {
        return latitude;
    }

    /**
     * @param latitude la latitude à définir pour l'emplacement du club sportif.
     */
    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    /**
     * @return la longitude de l'emplacement du club sportif.
     */
    public float getLongitude() {
        return longitude;
    }

    /**
     * @param longitude la longitude à définir pour l'emplacement du club sportif.
     */
    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
}
