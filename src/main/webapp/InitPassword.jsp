<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Initialisation du mot de passe</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="styleHeaderFooter.css">
    <link rel="stylesheet" type="text/css" href="styleAuthentification.css">
</head>
<body>
    <%@ include file="HeaderGrandPublic.jsp"%>
    
    <video autoplay muted loop>
        <source src="/club-de-sport/images/82636-580974567_large.mp4" type="video/mp4">
    </video>

    <div class="container main-content d-flex flex-column min-vh-100 justify-content-center mt-1 mb-1">
        <div class="container">
            <div class="auth-container">
                <div class="profile-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#fff">
                        <path d="M12 2C6.477 2 2 6.477 2 12s4.477 10 10 10 10-4.477 10-10S17.523 2 12 2zm0 18c-2.598 0-4.954-1.151-6.567-3.015C5.446 14.863 8.308 13 12 13c3.691 0 6.554 1.863 6.567 4.985C16.954 18.849 14.598 20 12 20zm0-7c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4z"/>
                    </svg>
                </div>
                <h2>Initialisation du mot de passe</h2>
                <form id="passwordForm" action="InitPasswordServlet" method="post">
                    <div class="form-group">
                        <input type="password" id="newPassword" name="newPassword" placeholder="Nouveau mot de passe" required>
                    </div>
                    <div class="form-group">
                        <input type="password" id="confirmPassword" name="confirmPassword" placeholder="Confirmer le mot de passe" required>
                    </div>
                    <div id="passwordCriteria" class="mt-2">
                        <p class="mb-1">Le mot de passe doit contenir :</p>
                        <ul class="list-unstyled">
                            <li id="lengthCriteria" class="text-danger">Au moins 8 caractères</li>
                            <li id="uppercaseCriteria" class="text-danger">Au moins une majuscule</li>
                            <li id="specialCharCriteria" class="text-danger">Au moins un caractère spécial (@, #, etc.)</li>
                            <li id="numberCriteria" class="text-danger">Au moins un chiffre</li>
                        </ul>
                    </div>
                    <div class="progress mt-2">
                        <div id="passwordStrength" class="progress-bar" role="progressbar" style="width: 0;"></div>
                    </div>
                    <div class="form-group mt-3">
                        <input type="submit" value="Initialiser le mot de passe">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal for error messages -->
    <div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="errorModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="errorModalLabel">Erreur</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="errorModalBody">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for success messages -->
    <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="successModalLabel">Succès</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="successModalBody">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>

    <%@ include file="footer.jsp" %>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <script>
        const passwordForm = document.getElementById('passwordForm');
        const newPassword = document.getElementById('newPassword');
        const confirmPassword = document.getElementById('confirmPassword');
        const passwordStrength = document.getElementById('passwordStrength');
        const lengthCriteria = document.getElementById('lengthCriteria');
        const uppercaseCriteria = document.getElementById('uppercaseCriteria');
        const specialCharCriteria = document.getElementById('specialCharCriteria');
        const numberCriteria = document.getElementById('numberCriteria');
        
        newPassword.addEventListener('input', updatePasswordStrength);

        function updatePasswordStrength() {
            const value = newPassword.value;
            let strength = 0;

            if (value.length >= 8) {
                lengthCriteria.classList.remove('text-danger');
                lengthCriteria.classList.add('text-success');
                strength += 25;
            } else {
                lengthCriteria.classList.remove('text-success');
                lengthCriteria.classList.add('text-danger');
            }

            if (/[A-Z]/.test(value)) {
                uppercaseCriteria.classList.remove('text-danger');
                uppercaseCriteria.classList.add('text-success');
                strength += 25;
            } else {
                uppercaseCriteria.classList.remove('text-success');
                uppercaseCriteria.classList.add('text-danger');
            }

            if (/[@#$%^&+=!]/.test(value)) {
                specialCharCriteria.classList.remove('text-danger');
                specialCharCriteria.classList.add('text-success');
                strength += 25;
            } else {
                specialCharCriteria.classList.remove('text-success');
                specialCharCriteria.classList.add('text-danger');
            }

            if (/[0-9]/.test(value)) {
                numberCriteria.classList.remove('text-danger');
                numberCriteria.classList.add('text-success');
                strength += 25;
            } else {
                numberCriteria.classList.remove('text-success');
                numberCriteria.classList.add('text-danger');
            }

            passwordStrength.style.width = strength + '%';
            passwordStrength.className = strength === 100 ? 'progress-bar bg-success' : 'progress-bar bg-warning';
        }

        passwordForm.addEventListener('submit', function(event) {
            event.preventDefault();
            const errorModalBody = document.getElementById('errorModalBody');

            // Regular expression for password validation
            const passwordRegex = /^(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=.*[a-z])(?=.*[0-9]).{8,}$/;

            if (!newPassword.value.match(passwordRegex)) {
                errorModalBody.textContent = "Le mot de passe doit contenir au moins 8 caractères, une majuscule, un caractère spécial et un chiffre.";
                $('#errorModal').modal('show');
                return;
            }

            if (newPassword.value !== confirmPassword.value) {
                errorModalBody.textContent = "Les mots de passe ne correspondent pas.";
                $('#errorModal').modal('show');
                return;
            }

            passwordForm.submit();
        });

        <% if (request.getAttribute("errorMessage") != null) { %>
            $(document).ready(function() {
                document.getElementById('errorModalBody').textContent = "<%= request.getAttribute("errorMessage") %>";
                $('#errorModal').modal('show');
            });
        <% } %>
        <% if (request.getAttribute("successMessage") != null) { %>
            $(document).ready(function() {
                document.getElementById('successModalBody').textContent = "<%= request.getAttribute("successMessage") %>";
                $('#successModal').modal('show');
            });
        <% } %>
    </script>
</body>
</html>
