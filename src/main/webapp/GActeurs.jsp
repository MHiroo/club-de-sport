<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.owasp.encoder.Encode"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    // Vérifier si l'attribut de session "username" n'existe pas
    if (session.getAttribute("userId") == null || session.getAttribute("acteur") == null) {
        // Rediriger vers une autre page, par exemple login.jsp
        response.sendRedirect("Authentification.jsp");
        return; // Important pour arrêter l'exécution du reste de la page
    }
%>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Créer un espace</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="styleActeur.css"> 
    <link rel="stylesheet" type="text/css" href="styleHeaderFooter.css">
    
</head>
<body>
<%@ include file="HeaderGA.jsp" %>

<div class="main-content d-flex flex-column min-vh-100">
 <div class="container createSpace">
        <h2 style="color: #fff; text-shadow: 2px 2px 4px #000;">Créer votre espace personnel</h2>
        <form action="CreateSpaceServlet" method="post" class="mt-4">
            <div class="form-group">
                <label for="name">Nom de l'espace:</label>
                <input type="text" class="form-control" id="name" name="name" value="${fn:escapeXml(name)}" required>
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
				<textarea class="form-control" id="description" name="description" rows="4" required><c:out value="${description}"/></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Créer l'espace</button>
        </form>
    </div>
</div>

<br><br><br>
     <%@ include file="footer.jsp" %>
</body>
</html>
