<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="model.ClubSportif" %>
<%@ page import="org.owasp.encoder.Encode"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Map of Sports Clubs</title>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin="" />
<link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
<link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">    
<link rel="stylesheet" type="text/css" href="styleMap.css">
<link rel="stylesheet" type="text/css" href="styleHeaderFooter.css">
</head>
<body>
<%@ include file="HeaderGrandPublic2.jsp" %>

<div class="main-content d-flex flex-column min-vh-100 transparent-div">
	<div class="container op">
	    <form id="searchForm" class="mb-5">
	        <div class="form-group">
	            <label for="federationSelect">Fédération :</label>
	            <select id="federationSelect" name="federation" class="form-control">
	                <option value="">Toutes les fédérations</option>
	                <% List<String> federations = (List<String>) request.getAttribute("federations");
	                   for (String federation : federations) {
	                %>
	                <option value="<%= federation %>"><%= federation %></option>
	                <% } %>
	            </select>
	        </div>
	        <div class="form-group">
	            <label for="searchTypeSelect">Rechercher par :</label>
	            <select id="searchTypeSelect" name="searchType" class="form-control" onchange="toggleSearchType()">
	                <option value="codePostal">Code Postal</option>
	                <option value="region">Région</option>
	            </select>
	        </div>
	        <div class="form-group" id="regionGroup" style="display: none;">
	            <label for="regionSelect">Région :</label>
	            <select id="regionSelect" name="region" class="form-control">
	                <% List<String> regions = (List<String>) request.getAttribute("regions");
	                   for (String region : regions) {
	                %>
	                <option value="<%= region %>"><%= region %></option>
	                <% } %>
	            </select>
	        </div>
	        <div class="form-group" id="codePostalGroup">
	            <label for="codePostalInput">Code Postal :</label>
	            <input type="text" id="codePostalInput" name="codePostal" class="form-control">
	        </div>
	        <div class="form-group" id="distanceSliderGroup" style="display: none;">
	            <label for="distanceSlider">Distance (km) :</label>
	            <input type="range" id="distanceSlider" name="distance" min="0" max="100" class="form-control-range" oninput="updateDistanceDisplay(this.value)" onchange="updateSearchRadius(this.value)">
	            <span id="distanceDisplay">0</span> km
	        </div>
	        <button type="button" class="btn btn-primary" onclick="searchClubs()">Rechercher</button>
	    </form>
	
	    <div id="mapid"></div>
	</div>
</div>
<script>
    var map, userLocation, searchCircle, markers;

    document.addEventListener('DOMContentLoaded', function() {
        map = L.map('mapid').setView([46.52863469527167, 2.43896484375], 6);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 19 }).addTo(map);
        markers = L.markerClusterGroup();
        map.on('locationfound', onLocationFound);
        map.on('locationerror', onLocationError);
        map.locate({setView: true, maxZoom: 16});
    });

    function toggleSearchType() {
        var searchType = document.getElementById('searchTypeSelect').value;
        document.getElementById('distanceSliderGroup').style.display = searchType === 'region' ? 'block' : 'none';
        document.getElementById('regionGroup').style.display = searchType === 'region' ? 'block' : 'none';
        document.getElementById('codePostalGroup').style.display = searchType === 'codePostal' ? 'block' : 'none';

        // Réinitialiser les champs non utilisés
        if (searchType === 'region') {
            document.getElementById('codePostalInput').value = '';
        } else {
            document.getElementById('regionSelect').value = '';
        }
    }

    function searchClubs() {
        var federation = document.getElementById('federationSelect').value;
        var region = document.getElementById('regionSelect').value;
        var codePostal = document.getElementById('codePostalInput').value;

        // Réinitialiser les champs non utilisés
        if (document.getElementById('searchTypeSelect').value === 'region') {
            codePostal = '';
        } else {
            region = '';
        }

        $.ajax({
            url: 'ClubSportifServlet',
            type: 'GET',
            dataType: 'json',
            data: {
                federation: federation,
                region: region,
                codePostal: codePostal
            },
            success: function(clubs) {
                updateMapWithClubs(clubs);
            },
            error: function(xhr, status, error) {
                alert('Erreur lors de la récupération des données: ' + error);
            }
        });
    }

    function updateMapWithClubs(clubs) {
        markers.clearLayers();
        clubs.forEach(function(club) {
            var popupContent = '<div>' +
                               '<strong>Commune:</strong> ' + club.commune + '<br/>' +
                               '<strong>Département:</strong> ' + club.departement + '<br/>' +
                               '<strong>Région:</strong> ' + club.region + '<br/>' +
                               '<strong>Fédération:</strong> ' + club.federation + '<br/>' +
                               '<strong>Total:</strong> ' + club.total + '<br/>' +
                               '<strong>Code Postal:</strong> ' + club.CodePostal + '</div>';
            var marker = L.marker([club.latitude, club.longitude]).bindPopup(popupContent);
            markers.addLayer(marker);
        });
        map.addLayer(markers);
        if (markers.getLayers().length > 0) {
            map.fitBounds(markers.getBounds());
        }
    }

    function onLocationFound(e) {
        userLocation = e.latlng;
        L.marker(userLocation).addTo(map).bindPopup("Vous êtes ici!").openPopup();
        //updateSearchRadius(document.getElementById('distanceSlider').value);
    }

    function onLocationError(e) {
        alert("Erreur de localisation: " + e.message);
    }

    function updateDistanceDisplay(value) {
        document.getElementById('distanceDisplay').textContent = value + ' km';
    }

    function updateSearchRadius(value) {
        if (searchCircle) {
            map.removeLayer(searchCircle);
        }
        var radius = parseInt(value, 10) * 1000;
        if (!isNaN(radius) && userLocation) {
            searchCircle = L.circle(userLocation, {
                color: 'blue',
                fillColor: '#add8e6',
                fillOpacity: 0.5,
                radius: radius
            }).addTo(map);
            map.fitBounds(searchCircle.getBounds());
        }
    }

    document.getElementById('distanceSlider').addEventListener('input', function() {
        updateDistanceDisplay(this.value);
        updateSearchRadius(this.value);
    });

    getLocation();
</script>
<%@ include file="footer.jsp" %>

<!-- Scripts JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js" integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw==" crossorigin=""></script>
    <script src="https://unpkg.com/leaflet.markercluster@1.3.0/dist/leaflet.markercluster.js"></script>
</body>
</html>


