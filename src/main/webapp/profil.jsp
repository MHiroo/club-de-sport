<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.Base64" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Profile Page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="styleProfil.css">
    <link rel="stylesheet" type="text/css" href="styleHeaderFooter.css">
</head>
<body>

<%@ include file="/HeaderGrandPublic.jsp" %>

<div class="main-content d-flex flex-column min-vh-100 justify-content-center align-items-center">
	<div class="container profile-container">
	    <%
	        String userNom = (String) session.getAttribute("userNom");
	        String userPrenom = (String) session.getAttribute("userPrenom");
	        String userType = (String) session.getAttribute("userType");
	        byte[] photoProfil = (byte[]) session.getAttribute("photoProfil");
	        String base64Image = Base64.getEncoder().encodeToString(photoProfil);
	    %>
	    <img src="data:image/jpeg;base64,<%= Encode.forHtml(base64Image) %>" alt="Profile Photo" class="profile-photo"/>
	    <div class="profile-info">
	        <h1><%= Encode.forHtml(userPrenom) %> <%= Encode.forHtml(userNom) %></h1>
	        <p>Détails:</p>
	        <ul>
	            <li>Nom: <%= Encode.forHtml(userNom) %></li>
	            <li>Prénom: <%= Encode.forHtml(userPrenom) %></li>
	            <li>Statut: <%= Encode.forHtml(userType) %></li>
	        </ul>
	    </div>
	    <form action="ProfilServlet" method="post" enctype="multipart/form-data" class="profile-form" onsubmit="return validateFileSize()">
	        <input type="file" name="photoProfil" id="photoProfil" accept="image/*"/>
	        <label for="photoProfil">Modifier la photo de profil</label>
	        <button type="submit">Upload</button>
	    </form>
	    <%
	        String error = (String) request.getAttribute("error");
	        if (error != null) {
	    %>
	        <div class="error"><%= Encode.forHtml(error) %></div>
	    <%
	        }
	    %>
	</div>
</div>

<!-- Bootstrap Modal -->
<div class="modal fade" id="fileSizeModal" tabindex="-1" aria-labelledby="fileSizeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="fileSizeModalLabel">Erreur de taille de fichier</h5>
            </div>
            <div class="modal-body">
                Le fichier dépasse la taille maximale autorisée de 2 Mo.
            </div>
        </div>
    </div>
</div>

<%@ include file="/footer.jsp" %>

<script>
    function validateFileSize() {
        const fileInput = document.getElementById('photoProfil');
        const file = fileInput.files[0];
        const maxSize = 2 * 1024 * 1024; // 2 MB

        if (file && file.size > maxSize) {
            var myModal = new bootstrap.Modal(document.getElementById('fileSizeModal'));
            myModal.show();

            setTimeout(() => {
                myModal.hide();
            }, 3000); // Auto-close after 3 seconds

            fileInput.value = ''; // Clear the input
            return false;
        }
        return true;
    }
</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
