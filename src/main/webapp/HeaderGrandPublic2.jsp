
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.util.Base64" %>
<nav class="navbar navbar-expand-lg">
	<div></div>
    <div class="brand-container text-center">
		<a class="navbar-brand site-name" href="index.jsp">Club Sportif</a>
    </div>    
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<i class="bi bi-list" style="float: right; margin-top: 10px;"></i>
	</button>

    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link text-light" href="index.jsp" style="font-weight: bold;">Accueil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-light" href="HomeGServlet" style="font-weight: bold;">Actualités</a>
            </li>
            <%if(session.getAttribute("userId") != null && session.getAttribute("elu") != null) { %>
           <li class="nav-item">
             <a class="nav-link text-light" href="EluServlet" style="font-weight: bold;">Elu</a>
           </li>
           <% }%>
           
           <%if(session.getAttribute("userId") != null) { %>
           <!-- Partie profil -->
           <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="ProfilServlet" id="profileDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="profile-icon-header">
                            <% 
                                byte[] photoProfil = (byte[]) session.getAttribute("photoProfil");
                                if (photoProfil != null) {
                                    String base64Image = Base64.getEncoder().encodeToString(photoProfil);
                            %>
                                    <img src="data:image/jpeg;base64,<%= Encode.forHtmlAttribute(base64Image) %>" alt="Profile Photo"/>
                            <% 
                                }
                            %>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="profileDropdown">
                        <span class="dropdown-item-text user-type"><%= Encode.forHtml((String) session.getAttribute("userType")) %> :</span>
                        <span class="dropdown-item-text font-weight-bold user-name mb-3"><%= Encode.forHtml((String) session.getAttribute("userNom")) %> <%= Encode.forHtml((String) session.getAttribute("userPrenom")) %></span>
                        <div class="dropdown-divider mb-0"></div>
                        <a class="dropdown-item profil-btn mb-0" href="ProfilServlet">Profil</a>
                        <div class="dropdown-divider mb-0"></div>
                        <form action="DeconnexionServlet" method="get" class="p-0 mb-0">
                            <button type="submit" class="nav-link btn btn-link deconnexion-btn mb-0">Déconnexion</button>
                        </form>
                    </div>
                </li>

           <% }else{ %>
           <li class="nav-item">
               <a class="nav-link text-light" href="Authentification.jsp" style="font-weight: bold;">S'identifier</a> 
           </li>
           <% }%>
        </ul>
    </div>
</nav>

<!-- Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.5.0/font/bootstrap-icons.min.css">
<script>
$(document).ready(function() {
    $('.nav-item.dropdown').hover(function() {
        $(this).addClass('show');
        $(this).find('.dropdown-menu').addClass('show');
    }, function() {
        $(this).removeClass('show');
        $(this).find('.dropdown-menu').removeClass('show');
    });
});
</script>