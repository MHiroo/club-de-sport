<%@ page import="java.util.List"%>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Inscription</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="styleInscription.css">
    <link rel="stylesheet" type="text/css" href="styleHeaderFooter.css">
</head>
<body>
    <%@ include file="HeaderGrandPublic.jsp" %>
    <div class="main-content d-flex flex-column min-vh-100">
        <form action="Inscription" method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
	         <div class="profile-icon">
	             <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#fff">
	                 <!-- Ajout de fill="#fff" -->
	                 <path d="M12 2C6.477 2 2 6.477 2 12s4.477 10 10 10 10-4.477 10-10S17.523 2 12 2zm0 18c-2.598 0-4.954-1.151-6.567-3.015C5.446 14.863 8.308 13 12 13c3.691 0 6.554 1.863 6.567 4.985C16.954 18.849 14.598 20 12 20zm0-7c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4z"/>
	             </svg>
	         </div>
            <%-- V�rification de l'existence de l'e-mail --%>
            <% if (request.getAttribute("emailExistsMsg") != null) { %>
            <div class="alert alert-danger" role="alert">
                <%= request.getAttribute("emailExistsMsg") %>
            </div>
            <% } %>

            <%-- V�rification de l'existence de l'�tablissement --%>
            <% if (request.getAttribute("etablissementNotFoundMsg") != null) { %>
            <div class="alert alert-danger" role="alert">
                <%= request.getAttribute("etablissementNotFoundMsg") %>
            </div>
            <% } %>
            <%-- V�rification de la preuve--%>
            <% if (request.getAttribute("fileErrorMsg") != null) { %>
            <div class="alert alert-danger" role="alert">
                <%= request.getAttribute("fileErrorMsg") %>
            </div>
            <% } %>

            <label for="nom">Nom :</label>
            <input type="text" name="nom" id="nom" required placeholder="Nom">
            <label for="prenom">Pr�nom :</label>
            <input type="text" name="prenom" id="prenom" required placeholder="Pr�nom">
            <label for="email">Email :</label>
            <input type="email" name="email" id="email" required placeholder="Email">
            <label for="type">Type de participant :</label>
            <select name="type" id="type" required onchange="toggleEtablissementFields()">
                <option value="" disabled selected>Type de participant</option>
                <option value="entraineur">Entra�neur</option>
                <option value="president">Pr�sident</option>
                <option value="elu">�lu</option>
                <option value="autre">Autre</option>
            </select>

            <div id="etablissementFields">
                <label for="etablissementSearch">Rechercher un �tablissement :</label>
                <input type="text" name="codePostal" id="codePostal" placeholder="Code Postal" required>
                <label for="federationSelect">F�d�ration :</label>
                <select id="federationSelect" name="federation" class="form-control" required>
                    <option value="">Toutes les f�d�rations</option>
                    <% List<String> federations = (List<String>) request.getAttribute("federations");
                       for (String federation : federations) {
                    %>
                    <option value="<%= federation %>"><%= federation %></option>
                    <% } %>
                </select>
            </div>
            <div id="proofFields">
            <label for="preuve">Preuve :</label>
            <input type="file" name="preuve" id="preuve" required>
            </div>
			<button type="submit" class="submit-btn">S'inscrire</button>
        </form>
    </div>

    <%@ include file="footer.jsp" %>

    <script>
    function toggleEtablissementFields() {
        const typeSelect = document.getElementById('type');
        const etablissementFields = document.getElementById('etablissementFields');
        const codePostal = document.getElementById('codePostal');
        const federationSelect = document.getElementById('federationSelect');

        if (typeSelect.value === 'elu') {
            etablissementFields.style.display = 'none';
            codePostal.required = false;
            federationSelect.required = false;
        } else if (typeSelect.value === 'autre') {
            etablissementFields.style.display = 'none';
            proofFields.style.display = 'none';
            preuve.required = false;
            codePostal.required = false;
            federationSelect.required = false;
        }
        else {
            etablissementFields.style.display = 'block';
            proofFields.style.display = 'block';
            preuve.required = true;
            codePostal.required = true;
            federationSelect.required = true;
        }
    }

    function validateForm() {
        const typeSelect = document.getElementById('type').value;
        const codePostal = document.getElementById('codePostal').value;
        const federationSelect = document.getElementById('federationSelect').value;

        if (typeSelect !== 'elu' && typeSelect !== 'autre') {
            if (codePostal === '' || federationSelect === '') {
                alert("Les champs 'Code Postal' et 'F�d�ration' sont obligatoires.");
                return false;
            }
        }
        return true;
    }

    // Ex�cuter la fonction au chargement de la page pour d�finir l'�tat initial
    window.onload = toggleEtablissementFields;
    </script>
</body>
</html>
