<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Mot de passe oublié</title>
    <link rel="stylesheet" type="text/css" href="styleHeaderFooter.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> <!-- jQuery inclus ici -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        body {
            background-image: url('/club-de-sport/images/mdp-ou.jpg');
            background-size: cover; /* Pour que l'image couvre toute la surface du corps */
            background-repeat: no-repeat;
            background-position: center; /* Pour centrer l'image */    
            color: #000000;
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        }
        .auth-container {
            max-width: 500px;
            margin: auto;
            padding: 20px;
            background: #fff;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        .modal-header {
            border-bottom: 0;
        }
        .progress-bar {
            height: 5px;
        }
    </style>
</head>
<body>
<%@ include file="HeaderGrandPublic3.jsp" %>
    <div class="container main-content d-flex flex-column min-vh-100 justify-content-center mt-1 mb-1">
        <div class="auth-container">
            <h2>Mot de passe oublié</h2>
            <form id="requestForm" action="SendVerificationCodeServlet" method="post">
                <div class="form-group">
                    <input type="email" id="email" name="email" class="form-control" placeholder="E-mail" required>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block" value="Envoyer le code de vérification">
                </div>
            </form>
        </div>
    </div>

    <!-- Modal pour la vérification du code -->
    <div class="modal fade" id="verifyCodeModal" tabindex="-1" role="dialog" aria-labelledby="verifyCodeModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="verifyCodeModalLabel">Vérification de code</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="verifyCodeForm" action="verifyCodeServlet" method="post">
                        <div class="form-group">
                            <input type="text" id="code" name="code" class="form-control" placeholder="Code de vérification" required>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary btn-block" value="Vérifier le code">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal pour la réinitialisation du mot de passe -->
    <div class="modal fade" id="resetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="resetPasswordModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="resetPasswordModalLabel">Réinitialisation du mot de passe</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="resetPasswordForm" action="ResetPasswordServlet" method="post">
                        <div class="form-group">
                            <input type="password" id="newPassword" name="newPassword" class="form-control" placeholder="Nouveau mot de passe" required>
                            <div class="progress mt-2">
                                <div id="passwordStrength" class="progress-bar" role="progressbar"></div>
                            </div>
                            <small id="passwordHelpBlock" class="form-text text-muted">
                                Votre mot de passe doit contenir au moins 8 caractères, une lettre majuscule, un chiffre, et un caractère spécial (!@#$%^&*).
                            </small>
                        </div>
                        <div class="form-group">
                            <input type="password" id="confirmPassword" name="confirmPassword" class="form-control" placeholder="Confirmer le mot de passe" required>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary btn-block" value="Réinitialiser le mot de passe">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal pour les messages -->
    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="messageModalLabel">Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modalMessage">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> <!-- jQuery inclus ici -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#requestForm').on('submit', function(e) {
            e.preventDefault();
            $.post($(this).attr('action'), $(this).serialize(), function(response) {
                if (response.success) {
                    $('#verifyCodeModal').modal('show');
                } else {
                    showModal(response.message);
                }
            }, 'json');
        });

        $('#verifyCodeForm').on('submit', function(e) {
            e.preventDefault();
            $.post($(this).attr('action'), $(this).serialize(), function(response) {
                if (response.success) {
                    $('#verifyCodeModal').modal('hide');
                    $('#resetPasswordModal').modal('show');
                } else {
                    showModal(response.message);
                }
            }, 'json');
        });

        $('#resetPasswordForm').on('submit', function(e) {
            e.preventDefault();
            
            var password = $('#newPassword').val();
            var strength = getPasswordStrength(password);
            
            if (strength < 3) {
                showModal('Le mot de passe ne respecte pas les critères de sécurité.');
                return;
            }

            $.post($(this).attr('action'), $(this).serialize(), function(response) {
                if (response.success) {
                    showModal('Mot de passe réinitialisé avec succès');
                    window.location.href = 'Authentification.jsp';
                } else {
                    showModal(response.message);
                }
            }, 'json');
        });

        $('#newPassword').on('input', function() {
            var password = $(this).val();
            var strength = getPasswordStrength(password);
            var $progressBar = $('#passwordStrength');

            $progressBar.removeClass('bg-danger bg-warning bg-success');

            if (strength < 3) {
                $progressBar.addClass('bg-danger').css('width', '33%');
            } else if (strength === 3) {
                $progressBar.addClass('bg-warning').css('width', '66%');
            } else if (strength > 3) {
                $progressBar.addClass('bg-success').css('width', '100%');
            }
        });

        function getPasswordStrength(password) {
            var lengthValid = password.length >= 8;
            var upperCaseValid = /[A-Z]/.test(password);
            var numberValid = /\d/.test(password);
            var specialCharValid = /[!@#$%^&*]/.test(password);
            var validChars = !password.match(/[^a-zA-Z0-9!@#$%^&*]/);
            
            var strength = 0;
            if (lengthValid) strength++;
            if (upperCaseValid) strength++;
            if (numberValid) strength++;
            if (specialCharValid) strength++;
            if (!validChars) strength = 0; // Réinitialiser la force si des caractères non valides sont trouvés
            
            return strength;
        }

        function showModal(message) {
            $('#modalMessage').text(message);
            $('#messageModal').modal('show');
        }
    });
    </script>
    
    <%@ include file="footer.jsp" %>
</body>
</html>
