<footer class="footer text-center py-2" style="background-color: rgba(128, 128, 128, 0.5);">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h4 class="mb-3 text-light">Contacts</h4>
                <ul class="list-unstyled">
                    <li class="text-light">Email : club@sportif.com</li>
                    <li class="text-light">T�l�phone : +33 01 455 324</li>
                </ul>
            </div>
            <div class="col-md-6">
                <h4 class="mb-3 text-light">Suivez-nous</h4>
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="https://www.data.gouv.fr/fr/datasets/donnees-geocodees-issues-du-recensement-des-licences-et-clubs-aupres-des-federations-sportives-agreees-par-le-ministere-charge-des-sports/" class="text-light">Data.gouv.fr</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
