<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="java.util.Base64" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Accueil Club</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="stylesheet" type="text/css" href="styleHeaderFooter.css">
	</head>
	<body class="accueil">
		<%@ include file="/HeaderGrandPublic.jsp" %>
		
		<video autoplay muted loop>
		    <source src="https://cdn.pixabay.com/video/2021/07/24/82636-580974567_large.mp4" type="video/mp4">
		</video>
		
		<div class="main-content d-flex flex-column min-vh-100 justify-content-center align-items-center">
			<div class="container text-center">
			    <h1 style="color: #fff; text-shadow: 2px 2px 4px #000;">Bienvenue sur votre site de gestion des clubs de sport!</h1><br>
			    <div class="btn-group">
			        <a href="ClubSportifServlet" class="btn btn-primary btn-lg mr-3 deluxe-button">Rechercher un club sportif</a>
			        <a href="PublicServlet" class="btn btn-primary btn-lg deluxe-button">Visualiser la répartition des licenciés</a>
			    </div>
			</div>
		</div>
		
		
		<%@ include file="/footer.jsp" %>
	</body>
</html>