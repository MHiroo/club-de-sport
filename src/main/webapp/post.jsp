<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="model.*" %>
<!DOCTYPE html>
<html>
<head>
    <title>${spaceData.name}</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="styleActeur.css"> 
    <link rel="stylesheet" type="text/css" href="styleHeaderFooter.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <style>
        .name-lastname {
            float: right;
        }
        .btn-action {
            display: flex;
            align-items: center;
            margin-top: 10px;
        }
        .btn-action i {
            margin-right: 5px;
        }
        .comment-section {
            margin-top: 20px;
            display: none; /* Hide comments section by default */
        }
        .comment {
            margin-bottom: 10px;
            padding: 10px;
            background: #f8f9fa;
            border-radius: 5px;
        }
        .comment strong {
            display: block;
            margin-bottom: 5px;
        }
        .btn-toggle-comments {
            margin-top: 10px;
        }
        .btn {
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .btn i {
            margin-right: 5px;
        }
    </style>
</head>
<body>
<%@ include file="HeaderGA.jsp" %>
    <div class="main-content d-flex flex-column min-vh-100 justify-content-center align-items-center">
        <div class="container" id="ff">
            <h1 style="color: #fff; text-shadow: 2px 2px 4px #000;"><b><i>${spaceData.name.toUpperCase()}</i></b></h1>
            <p class="text-center"><i>${spaceData.description}</i></p>
            <%-- Vérifier si une session est ouverte --%>  
            <div class="mt-5">
            <% if (session.getAttribute("userId") != null && session.getAttribute("acteur") != null) { %>
                <h2 style="color: #fff; text-shadow: 2px 2px 4px #000;"><i>Ajouter un nouveau Post</i></h2>
                <form action="UploadPostServlet" method="post" enctype="multipart/form-data" class="p-3 shadow rounded bg-light">
                     <%-- Vérification de la photo --%>
                    <% if (request.getAttribute("errorMessage") != null) { %>
                    <div class="alert alert-danger" role="alert">
                        <%= request.getAttribute("errorMessage") %>
                    </div>
                    <% } %>
                    <input type="hidden" name="spaceId" value="${spaceData.spaceId}" />
                    <div class="form-group">
                        <label for="title" class="font-weight-bold">Titre:</label>
                        <input type="text" class="form-control" id="title" name="title" required>
                    </div>
                    <div class="form-group">
                        <label for="content" class="font-weight-bold">Contenu:</label>
                        <textarea class="form-control" id="content" name="content" rows="4" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="photo" class="font-weight-bold">Photo:</label>
                        <input type="file" class="form-control-file" id="photo" name="photo">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Publier</button>
                </form>
                <% } %>
            </div>
            
            <div class="mt-5">
                <div id="postsList">
                <h3 style="color: #fff; text-shadow: 2px 2px 4px #000;"><b><i>LES POSTS</i></b></h3>
                    <c:forEach var="post" items="${posts}">
                        <div class="post-card" id="post${post.postId}">
                            <h4>${post.title}</h4>
                            <p><c:out value="${post.content}" escapeXml="false"/></p>
                            <c:if test="${not empty post.photo}">
                               <img src="data:image/jpeg;base64,${Encodage.encodeImage(post.photo)}" alt="Post Image" style="width: 200px; height: auto;" />
                               <br><br>
                            </c:if>
                            <div class="name-lastname">
                                <i>${post.createdAt}</i>
                            </div>
                            <% if (session.getAttribute("userId") != null && session.getAttribute("acteur") != null) { %>
                                <div class="btn-action">
                                    <button onclick="location.href='EditPostServlet?postId=${post.postId}&spaceId=${spaceData.spaceId}'" class="btn btn-info">
                                        <i class="fas fa-edit"></i> Modifier
                                    </button>&nbsp;&nbsp;
                                    <button onclick="location.href='DeletePostServlet?postId=${post.postId}&spaceId=${spaceData.spaceId}'" class="btn btn-danger">
                                        <i class="fas fa-trash"></i> Supprimer
                                    </button>
                                    &nbsp;&nbsp;
                              <% } %>
                              <% if (session.getAttribute("userId") != null && (session.getAttribute("autre") != null)||(session.getAttribute("elu") != null)|| (session.getAttribute("acteur") != null)) { %>
                                    <button onclick="likePost(${post.postId}, this)" class="btn btn-primary">
                                        <i class="fas fa-thumbs-up"></i> J'aime <span class="badge badge-light">${post.likes}</span>
                                    </button>&nbsp;&nbsp;
                                    <button onclick="showCommentForm(${post.postId})" class="btn btn-secondary">
                                        <i class="fas fa-comment"></i> Commenter
                                    </button>
                                </div>
                            <% } %>
                                    <button id="toggleComments${post.postId}" onclick="toggleComments(${post.postId})" class="btn btn-link btn-toggle-comments">
                                        Afficher les commentaires
                                    </button>

                            <div id="commentForm${post.postId}" style="display:none;">
                                <textarea id="commentContent${post.postId}" class="form-control" rows="3"></textarea>
                                <button onclick="submitComment(${post.postId})" class="btn btn-primary mt-2">Envoyer</button>
                            </div>
                            <div class="comment-section" id="commentSection${post.postId}">
                                <c:forEach var="comment" items="${post.comments}">
                                    <div class="comment">
                                        <strong>${comment.username}</strong>: ${comment.content}
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
         </div>
     </div>
     
     <%@ include file="footer.jsp" %>

<!-- Modal -->
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="messageModalLabel">Notification</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modalMessage">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://kit.fontawesome.com/a076d05399.js"></script> <!-- Pour les icônes FontAwesome -->
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script>
document.addEventListener("DOMContentLoaded", function() {
    const postsList = document.getElementById("postsList");

    // Animation pour les boutons Modifier et Supprimer
    postsList.addEventListener("mouseover", function(e) {
        if (e.target.tagName === 'BUTTON') {
            e.target.style.transform = "scale(1.1)";
        }
    });

    postsList.addEventListener("mouseout", function(e) {
        if (e.target.tagName === 'BUTTON') {
            e.target.style.transform = "scale(1)";
        }
    });

    // Effet de survol pour les cartes de post
    postsList.querySelectorAll('.post-card').forEach(card => {
        card.addEventListener('mouseenter', () => {
            card.style.backgroundColor = '#f8f9fa';
        });

        card.addEventListener('mouseleave', () => {
            card.style.backgroundColor = '#ffffff';
        });
    });
});

function likePost(postId, button) {
    $.post("LikeServlet", {postId: postId}, function(response) {
        if (response.success) {
            var likesBadge = $(button).find('.badge');
            var likesCount = parseInt(likesBadge.text());
            likesBadge.text(likesCount + 1);
        } else {
            showModal(response.message);
        }
    }, 'json');
}

function showCommentForm(postId) {
    document.getElementById("commentForm" + postId).style.display = "block";
}

function toggleComments(postId) {
    var commentSection = document.getElementById("commentSection" + postId);
    var toggleButton = document.getElementById("toggleComments" + postId);
    if (commentSection.style.display === "none") {
        commentSection.style.display = "block";
        toggleButton.textContent = "Masquer les commentaires";
    } else {
        commentSection.style.display = "none";
        toggleButton.textContent = "Afficher les commentaires";
    }
}


function submitComment(postId) {
    var content = document.getElementById("commentContent" + postId).value;
    $.post("CommentServlet", {postId: postId, content: content}, function(response) {
        if (response.success) {
            var commentSection = document.getElementById("commentSection" + postId);
            var newComment = document.createElement("div");
            newComment.className = "comment";
            newComment.innerHTML = "<strong>" + response.username + "</strong>: " + content;
            commentSection.appendChild(newComment);
            document.getElementById("commentContent" + postId).value = "";
            document.getElementById("commentForm" + postId).style.display = "none"; // Hide comment form after submission
        } else {
            showModal(response.message);
        }
    }, 'json');
}

function showModal(message) {
    $('#modalMessage').text(message);
    $('#messageModal').modal('show');
}

CKEDITOR.replace('content');
</script>
</body>
</html>
