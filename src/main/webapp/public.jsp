<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Base64" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Affichage Licences</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="styleHeaderFooter.css">
</head>
<body class="elu">
<%@ include file="HeaderGrandPublic2.jsp" %>

<div class="main-content d-flex flex-column min-vh-100 transparent-div">
	<div class="container mt-2">
	    <div id="message" class="message-container position-relative mb-2">
	        <p>Veuillez spécifier au moins une fédération ou une localisation géographique.</p>
	    </div>
	    <form id="searchForm" class="mb-5 row align-items-center">
	        <div class="form-group col-md-5">
	            <label for="federationSelect">Fédération :</label>
	            <input type="text" id="federationSelect" name="federation" class="form-control">
	        </div>
	        <div class="form-group col-md-2">
	            <label for="localisationTypeSelect">Rechercher par :</label>
	            <select id="localisationTypeSelect" name="localisationType" class="form-control" onchange="toggleSearchType()">
	                <option value="commune">Commune</option>
	                <option value="departement">Département</option>
	                <option value="region">Région</option>
	            </select>
	        </div>
	        <div class="form-group col-md-4 col-lg-4" id="communeGroup">
	            <label for="communeSelect">Commune :</label>
	            <input type="text" id="communeSelect" name="localisation" class="form-control">
	        </div>
	        <div class="form-group col-md-4 col-lg-4" id="departementGroup" style="display: none;">
	            <label for="departementSelect">Département :</label>
	            <select id="departementSelect" name="localisation" class="form-control">
	                <% List<String> departements = (List<String>) request.getAttribute("departements"); for (String departement : departements) { %>
	                <option value="<%= Encode.forHtmlAttribute(departement) %>"><%= Encode.forHtml(departement) %></option>
	                <% } %>
	            </select>
	        </div>
	        <div class="form-group col-md-4 col-lg-4" id="regionGroup" style="display: none;">
	            <label for="regionSelect">Région :</label>
	            <select id="regionSelect" name="localisation" class="form-control">
	                <% List<String> regions = (List<String>) request.getAttribute("regions"); for (String region : regions) { %>
	                <option value="<%= Encode.forHtmlAttribute(region) %>"><%= Encode.forHtml(region) %></option>
	                <% } %>
	            </select>
	        </div>
	        <div class="form-group col-md-1">
	            <button type="button" class="btn btn-primary search-btn" onclick="searchLicence()">Rechercher</button>
	        </div>
	    </form>
	</div>
	
	
	<div class="container mt-5">
	    <div class="row">
	        <div class="col-md-6">
	            <h4 style="color: #fff; text-shadow: 2px 2px 4px #000;">Nombre Total de Licences</h4>
	            <p id="nbLicencesTotal" class="mt-5" style="color: #fff; text-shadow: 2px 2px 4px #000; font-size: 1.2em;"></p>
	            <p id="nbLicencesHomme" class="mt-5" style="color: #fff; text-shadow: 2px 2px 4px #000; font-size: 1.2em;"></p>
	            <p id="nbLicencesFemme" class="mt-5" style="color: #fff; text-shadow: 2px 2px 4px #000; font-size: 1.2em;"></p>
	        </div>
	        <div class="col-md-6">
	            <h4 style="color: #fff; text-shadow: 2px 2px 4px #000;">Répartition des licences par genre:</h4>
	            <canvas id="licenseChart" class="mt-5" style="color: #fff; text-shadow: 2px 2px 4px #000;"></canvas>
	        </div>
	    </div>
	</div>
	
<br><br>
</div>

<%@ include file="footer.jsp" %>

<script>

    $(document).ready(function() {
        $('#communeSelect').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: 'CommuneAutocompleteServlet',
                    dataType: 'json',
                    data: {
                        keyword: request.term
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            }
        });
    
        // Empêcher la soumission du formulaire lorsque la touche "Entrée" est pressée
        $('#communeSelect').keydown(function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                return false;
            }
        });
    });
    
    $(document).ready(function() {
        $('#federationSelect').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: 'FederationAutocompleteServlet',
                    dataType: 'json',
                    data: {
                        keyword: request.term
                    },
                    success: function(data) {
                        response(data);
                    }
                });
            }
        });
    
        // Empêcher la soumission du formulaire lorsque la touche "Entrée" est pressée
        $('#federationSelect').keydown(function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                return false;
            }
        });
    });

    function toggleSearchType() {
        var searchType = $('#localisationTypeSelect').val();
        $('#regionGroup, #communeGroup, #departementGroup').hide();
        $('#' + searchType + 'Group').show();
    }

    var genreCtx = document.getElementById('licenseChart').getContext('2d');
    var licenseChart;

    
    function searchLicence() {
        // Récupération des valeurs de offset et limit depuis la page
        var federation = $('#federationSelect').val();
        var localisationType = $('#localisationTypeSelect').val();
        var localisation = $('#' + localisationType + 'Select').val();
        
        // Vérifier si à la fois federation et localisation sont vides
        if (federation === "" && localisation === "") {
            // Afficher le message d'erreur en rouge
            $('.message-container').html('<p style="color: red;">Veuillez spécifier au moins une fédération ou une localisation géographique.</p>');
            $('#message').show(); // Afficher le message
            return; // Sortir de la fonction si les deux champs sont vides
        } else {
            // Effacer le message d'erreur s'il existe
            $('.message-container').empty();
            $('#message').hide(); // Masquer le message
        }
        $.ajax({
            url: 'PublicServlet',
            type: 'GET',
            dataType: 'json',
            data: {
                federation: federation,
                localisation: localisation,
                localisationType: localisationType
                
            },
            success: function(data) {
                var nbLicencesTotal = data.nbLicencesTotal;
                var nbLicencesHomme = data.nbLicencesHomme;
                var nbLicencesFemme = data.nbLicencesFemme;

                var pourcentageHomme = ((nbLicencesHomme / nbLicencesTotal) * 100).toFixed(0);
                var pourcentageFemme = ((nbLicencesFemme / nbLicencesTotal) * 100).toFixed(0);

                updateNbLicences(nbLicencesTotal, nbLicencesHomme, nbLicencesFemme);
                updateCharts(data.licenceList, pourcentageHomme, pourcentageFemme);
            },
            error: function(xhr, status, error) {
                alert('Erreur lors de la récupération des données: ' + error);
            }
        });
    }


    function updateCharts(data, pourcentageHomme, pourcentageFemme) {
        if (licenseChart) licenseChart.destroy();
        licenseChart = new Chart(genreCtx, createChartConfigGenre(pourcentageHomme, pourcentageFemme));
    }

    function createChartConfigGenre(pourcentageHomme, pourcentageFemme) {
        var data = {
            labels: ['Hommes', 'Femmes'],
            datasets: [{
                data: [pourcentageHomme, pourcentageFemme],
                backgroundColor: ['#3366CC', '#DC3912'],
                hoverBackgroundColor: ['#3366CC', '#DC3912']
            }]
        };

        return {
            type: 'doughnut',
            data: data,
            options: {
                responsive: true,
                legend: {
                    position: 'right'
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var label = data.labels[tooltipItem.index] || '';
                            if (label) {
                                label += ': ';
                            }
                            label += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + '%';
                            return label;
                        }
                    }
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        };
    }



    function getTotalFemmes(licence) {
        var totalFemmes = 0;
        for (var key in licence) {
            if (key.startsWith("f_")) {
                totalFemmes += licence[key];
            }
        }
        return totalFemmes;
    }

    function getTotalHommes(licence) {
        var totalHommes = 0;
        for (var key in licence) {
            if (key.startsWith("h_")) {
                totalHommes += licence[key];
            }
        }
        return totalHommes;
    }
    
    function getPourcentageTotalFemmes(licence) {
        var totalFemmes = 0;
        for (var key in licence) {
            if (key.startsWith("f_")) {
                totalFemmes += licence[key];
            }
        }
        totalFemmes = ((totalFemmes / licence.total) * 100).toFixed(0);
        return totalFemmes;
    }

    function getPourcentageTotalHommes(licence) {
        var totalHommes = 0;
        for (var key in licence) {
            if (key.startsWith("h_")) {
                totalHommes += licence[key];
            }
        }
        totalHommes = ((totalHommes / licence.total) * 100).toFixed(0);
        return totalHommes;
    }

    
    function updateNbLicences(nbLicencesTotal, nbLicencesHomme, nbLicencesFemme) {

        $('#nbLicencesTotal').text("Total : " + nbLicencesTotal);
        $('#nbLicencesHomme').text("Hommes : " + nbLicencesHomme);
        $('#nbLicencesFemme').text("Femmes : " + nbLicencesFemme);
    }

</script>
</body>
</html>
