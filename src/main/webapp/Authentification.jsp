<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Authentification</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="styleHeaderFooter.css">
    <link rel="stylesheet" type="text/css" href="styleAuthentification.css">
</head>
<body>
    <%@ include file="HeaderGrandPublic.jsp"%>
    
	<video autoplay muted loop>
        <source src="https://cdn.pixabay.com/video/2021/07/24/82636-580974567_large.mp4" type="video/mp4">
    </video>

    <div class="container main-content d-flex flex-column min-vh-100 justify-content-center mt-1 mb-1">
	    <div class="container">
	        <div class="auth-container">
	            <div class="profile-icon">
	                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#fff">
	                    <!-- Ajout de fill="#fff" -->
	                    <path d="M12 2C6.477 2 2 6.477 2 12s4.477 10 10 10 10-4.477 10-10S17.523 2 12 2zm0 18c-2.598 0-4.954-1.151-6.567-3.015C5.446 14.863 8.308 13 12 13c3.691 0 6.554 1.863 6.567 4.985C16.954 18.849 14.598 20 12 20zm0-7c-2.206 0-4-1.794-4-4s1.794-4 4-4 4 1.794 4 4-1.794 4-4 4z"/>
	                </svg>
	            </div>
	            <form action="authentification" method="post">
	            	      <%-- V�rification de lidentifiant et du mot de passe --%>
					<% if (request.getAttribute("erreurMessage") != null) { %>
					<div class="alert alert-danger" role="alert">
					<%= request.getAttribute("erreurMessage") %>
			        </div>
			        <% } %>
	                <div class="form-group">
	                    <input type="email" id="email" name="email" placeholder="E-mail" required>
	                </div>
	                <div class="form-group">
	                    <input type="password" id="password" name="password" placeholder="Mot de passe" required>
   	                    <p><a href="Reini_mdp.jsp">mot de passe oubli�?</a></p>           
	                </div>
	                <div class="form-group">
	                    <input type="submit" value="Se connecter">
	                </div>
	            </form>
	            <p>Vous n'avez pas de compte ? <a href="Inscription">Inscrivez-vous!</a></p>
	        </div>
	    </div>
    </div>


	 <%@ include file="footer.jsp" %>
</body>
</html>
