<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <title>Editez votre publication</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="styleHeaderFooter.css">
</head>
<body class= "editPost">
<%@ include file="HeaderGA.jsp" %>
<div class="main-content d-flex flex-column min-vh-100">
	<div class="container mt-5">
	    <h1 style="color: #fff; text-shadow: 2px 2px 4px #000;">Edit Post</h1>
	    <form action="UploadPostServlet?spaceId=${spaceId}" method="post" enctype="multipart/form-data">
	   		 <%-- Vérification de la photo--%>
            <% if (request.getAttribute("errorMessage") != null) { %>
            <div class="alert alert-danger" role="alert">
                <%= request.getAttribute("errorMessage") %>
            </div>
            <% } %>
	        <input type="hidden" name="postId" value="${post.postId}" />
	        <div class="form-group">
	            <label for="title">Title:</label>
	            <input type="text" class="form-control" id="title" name="title" value="<c:out value='${post.title}'/>" required>
	        </div>
	        <div class="form-group">
	            <label for="content">Content:</label>
	            <textarea class="form-control" id="content" name="content" rows="5" required><c:out value="${post.content}"/></textarea>
	        </div>
	        <div class="form-group">
	            <label for="photo">Photo (Si vous voulez changer la photo ):</label>
	            <input type="file" class="form-control-file" id="photo" name="photo">
	        </div>
	        <button type="submit" class="btn btn-primary">Update Post</button>
	    </form>
	</div>
</div>
     <%@ include file="footer.jsp" %>
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.3/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script>     
     CKEDITOR.replace('content');
</script>
</body>
</html>
