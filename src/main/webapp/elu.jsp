<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Base64" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Affichage Licences</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="styleHeaderFooter.css">

</head>
<body class="elu">
	<%@ include file="HeaderElu.jsp"%>
	
	<div class="main-content d-flex flex-column min-vh-100 transparent-div">
		<div class="container mt-2">
			<button id="exportXlsxBtn" class="btn btn-primary mb-2 mt-10 custom-btn" onclick="exportToXLSX(0)">
			    <i class="fas fa-file-excel"></i> Exporter les données
			</button>
			
			<button id="downloadPdfBtn" class="btn btn-primary mb-2 mt-10 custom-btn" onclick="generatePDF()">
			    <i class="fas fa-file-pdf"></i> Télécharger la page en PDF
			</button>
			<br><br>
			<div id="message" class="message-container position-relative mb-2">
				<p>Veuillez spécifier au moins une fédération ou une localisation géographique.</p>
			</div>
			<form id="searchForm" class="mb-5 row align-items-center">
				<div class="form-group col-md-5">
					<label for="federationSelect" class="white-shadow" style="font-size: 1.1em;">Fédération :</label> <input
						type="text" id="federationSelect" name="federation"
						class="form-control">
				</div>
				<div class="form-group col-md-2">
					<label for="localisationTypeSelect" class="white-shadow" style="font-size: 1.1em;">Rechercher par :</label> <select
						id="localisationTypeSelect" name="localisationType"
						class="form-control" onchange="toggleSearchType()">
						<option value="commune">Commune</option>
						<option value="departement">Département</option>
						<option value="region">Région</option>
					</select>
				</div>
				<div class="form-group col-md-4 col-lg-4" id="communeGroup">
					<label for="communeSelect" class="white-shadow" style="font-size: 1.1em;">Commune :</label> <input type="text"
						id="communeSelect" name="localisation" class="form-control">
				</div>
				<div class="form-group col-md-4 col-lg-4" id="departementGroup"
					style="display: none;">
					<label for="departementSelect" class="white-shadow" style="font-size: 1.1em;">Département :</label> <select
						id="departementSelect" name="localisation" class="form-control">
						<% List<String> departements = (List<String>) request.getAttribute("departements"); for (String departement : departements) { %>
						<option value="<%= Encode.forHtmlAttribute(departement) %>"><%= Encode.forHtml(departement) %></option>
						<% } %>
					</select>
				</div>
				<div class="form-group col-md-4 col-lg-4" id="regionGroup"
					style="display: none;">
					<label for="regionSelect" class="white-shadow" style="font-size: 1.1em;">Région :</label> <select id="regionSelect"
						name="localisation" class="form-control">
						<% List<String> regions = (List<String>) request.getAttribute("regions"); for (String region : regions) { %>
						<option value="<%= Encode.forHtmlAttribute(region) %>"><%= Encode.forHtml(region) %></option>
						<% } %>
					</select>
				</div>
				<div class="form-group col-md-1">
					<button type="button" class="btn btn-primary search-btn" onclick="searchLicence(0)">Rechercher</button>
				</div>
			</form>
		</div>
	
	
	
		<div class="container mt-5">
			<div class="row">
				<div class="col-md-6">
					<h3 class="white-shadow">Nombre Total de Licences</h3>
					<p id="nbLicencesTotal" class="mt-5 white-shadow" style="font-size: 1.2em;"></p>
					<p id="nbLicencesHomme" class="mt-5 white-shadow" style="font-size: 1.2em;"></p>
					<p id="nbLicencesFemme" class="mt-5 white-shadow" style="font-size: 1.2em;"></p>
				</div>
				<div class="col-md-6 white-shadow">
					<h3>Répartition des licences par genre:</h3>
					<canvas id="licenseChart" class="mt-5"></canvas>
				</div>
			</div>
		</div>
	
	
	
	
		<div class="container mt-5">
			<h3 class="mt-5 white-shadow">Nombre de licences par tranche d'âges:</h3>
			<br>
			<div class="row">
				<div class="col-md-6">
					<h4 class="mt-5 white-shadow">Hommes</h4>
					<canvas id="menLicenseChart" class="mt-5"></canvas>
				</div>
				<div class="col-md-6">
					<h4 class="mt-5 white-shadow">Femmes</h4>
					<canvas id="womenLicenseChart" class="mt-5"></canvas>
				</div>
			</div>
		</div>
	
	
	
		<div class="container mt-5">
			<div class="mt-3" id="pagination">
				<h3 class="mt-5 white-shadow">Classement Nombre de Licences</h3>
				<br>
				<div class="mt-3 d-flex justify-content-between align-items-center">
					<div class="d-flex align-items-center">
						<label for="limitSelect">Nombre de lignes par
							page :</label> <select id="limitSelect" class="form-control"
							onchange="changeLimit()" style="width: 80px;">
							<option value="10">10</option>
							<option value="20">20</option>
							<option value="50">50</option>
							<option value="100">100</option>
						</select>
					</div>
					<div class="form-inline">
						<div style="margin-left: 10px; margin-right: 10px;"></div>
						<!-- Espace horizontal ajouté ici -->
						<div class="form-group">
							<label for="orderBySelect">Trier par :</label> <select
								id="orderBySelect" class="form-control" onchange="toggleOrder()"
								style="width: 150px;">
								<option value="Total">Total</option>
								<option value="Commune">Commune</option>
								<option value="Departement">Département</option>
								<option value="Federation">Fédération</option>
							</select>
						</div>
						<div style="margin-left: 10px; margin-right: 10px;"></div>
						<!-- Espace horizontal ajouté ici -->
						<div id="orderDirection" style="text-align: center;">
							<label style="margin-right: 20px;"> <input type="radio"
								name="orderDirection" value="ASC" id="ascendingSelect"
								onclick="toggleDirection('ASC')"> Croissant
							</label> <label> <input type="radio" name="orderDirection"
								value="DESC" id="descendingSelect" checked
								onclick="toggleDirection('DESC')"> Décroissant
							</label>
						</div>
	
					</div>
	
				</div>
	
	
			</div>
	
			<div class="container-fluid">
				<div class="row justify-content-center">
				    <div class="col-lg-10">
				        <div class="table-responsive"> <!-- Ajouter une classe responsive pour le tableau -->
				            <table id="licenceTable" class="datatable table-centered mt-3 table-bordered">
				                <thead>
				                    <tr>
				                        <th>Fédération</th>
				                        <th>Commune</th>
				                        <th>Département</th>
				                        <th>Région</th>
				                        <th>Total</th>
				                        <th>Total Femme</th>
				                        <th>Total Homme</th>
				                        <th>Proportion Femme</th>
				                        <th>Proportion Homme</th>
				                    </tr>
				                </thead>
				                <tbody id="tabId"></tbody>
				            </table>
				        </div>
				    </div>
				</div>
																																			
				<br>
				<div class="d-flex align-items-center justify-content-end">
					<nav aria-label="pagination">
						<ul class="pagination justify-content-center"></ul>
					</nav>
				</div>
	
			</div>
	
		</div>
	<br>
	<br>
	</div>
	

	<%@ include file="footer.jsp"%>

	<script>
		var menCtx = document.getElementById('menLicenseChart').getContext('2d');
	    var womenCtx = document.getElementById('womenLicenseChart').getContext('2d');
	    var genreCtx = document.getElementById('licenseChart').getContext('2d');
	    var menLicenseChart, womenLicenseChart, licenseChart;
	
	
		var orderBy = 'Total'; // Ordre initial par commune
		var ascending = 'DESC'; // Par défaut, tri ascendant
		
		var currentPage = 0;
		var limit = 10; // Définir la limite initiale à 10
		var totalRows = 0; // Variable pour stocker le nombre total de lignes
	
		function exportToXLSX(offset) {
		    var federation = $('#federationSelect').val();
		    var localisationType = $('#localisationTypeSelect').val();
		    var localisation = $('#' + localisationType + 'Select').val();
		    var limit = 999999999;
		    var orderBy = document.getElementById("orderBySelect").value;
	
		    if (federation === "" && localisation === "") {
		        $('.message-container').html('<p style="color: red;">Veuillez spécifier au moins une fédération ou une localisation géographique.</p>');
		        $('#message').show();
		        return;
		    } else {
		        $('.message-container').empty();
		        $('#message').hide();
		    }
	
		    $.ajax({
		        url: 'EluServlet',
		        type: 'POST',
		        dataType: 'json',
		        data: {
		            federation: federation,
		            localisation: localisation,
		            localisationType: localisationType,
		            offset: offset,
		            limit: limit,
		            orderBy: orderBy,
		            ascending: ascending
		        },
		        success: function(data) {
		            var licences = data.licenceListTab.map(function(licence) {
		                // Calculer le pourcentage total des femmes et hommes
		                var pourcentageTotalFemmes = (( getTotalFemmes(licence) / licence.total) * 100).toFixed(0);
		                var pourcentageTotalHommes = ((getTotalHommes(licence) / licence.total) * 100).toFixed(0);
	
		                return {
		                    Federation: licence.federation,
		                    Commune: licence.commune,
		                    Departement: licence.departement,
		                    Region: licence.region,
		                    Total: licence.total,
		                    TotalFemmes: getTotalFemmes(licence),
		                    TotalHommes: getTotalHommes(licence),
		                    PourcentageFemmes: pourcentageTotalFemmes,
		                    PourcentageHommes: pourcentageTotalHommes
		                };
		            });
	
		            var wb = XLSX.utils.book_new();
		            var ws = XLSX.utils.json_to_sheet(licences);
		            XLSX.utils.book_append_sheet(wb, ws, "Licences");
		            XLSX.writeFile(wb, 'export_licences.xlsx');
		        },
		        error: function(xhr, status, error) {
		            alert('Erreur lors de la récupération des données: ' + error);
		        }
		    });
		}

								
		// Fonction pour générer le PDF
		function generatePDF() {
		    // Créer une instance de jsPDF
		    var doc = new jsPDF('p', 'pt', 'a4');
		
		    // Options pour html2canvas
		    var options = {
		        scale: 1, // Échelle à 1 pour une résolution standard
		        logging: false, // Désactiver les journaux de console de html2canvas
		        useCORS: true, // Activer CORS pour permettre la capture d'éléments distants
		        windowHeight: document.body.scrollHeight, // Spécifier la hauteur de la fenêtre de capture
		        windowWidth: document.body.scrollWidth, // Spécifier la largeur de la fenêtre de capture
		        x: window.scrollX, // Position horizontale de la zone à capturer
		        y: window.scrollY, // Position verticale de la zone à capturer
		        width: document.body.scrollWidth, // Largeur de la zone à capturer
		        height: document.body.scrollHeight // Hauteur de la zone à capturer
		    };
		
		    // Capturer la page HTML avec html2canvas
		    html2canvas(document.body, options).then(function(canvas) {
		        var imgData = canvas.toDataURL('image/jpeg', 1.0);
		
		        // Ajouter l'image au PDF
		        var imgWidth = 595.28; // Largeur de la page PDF
		        var pageHeight = 841.89; // Hauteur de la page PDF
		        var imgHeight = (canvas.height * imgWidth) / canvas.width;
		        var heightLeft = imgHeight;
		        var position = 0;
		
		        // Ajouter l'image en tant que pages multiples si elle dépasse la hauteur de la page PDF
		        while (heightLeft >= 0) {
		            doc.addImage(imgData, 'JPEG', 0, position, imgWidth, imgHeight);
		            heightLeft -= pageHeight;
		            position -= pageHeight;
		
		            // Ajouter une nouvelle page si nécessaire
		            if (heightLeft >= 0) {
		                doc.addPage();
		            }
		        }
		
		        // Enregistrer le PDF
		        doc.save('page.pdf');
		    });
		}
							


		
	    
		function toggleOrder() {
		    var selectElement = document.getElementById('orderBySelect');
		    orderBy = selectElement.value;
		    searchLicenceOffset(0); // Appel de la fonction de recherche avec les nouveaux paramètres de tri
		}
		function toggleDirection(direction) {
		    ascending = direction;
		    searchLicenceOffset(0); // Appel de la fonction de recherche avec les nouveaux paramètres de tri
		}
	
		$(document).ready(function() {
		    $('#communeSelect').autocomplete({
		        source: function(request, response) {
		            $.ajax({
		                url: 'CommuneAutocompleteServlet',
		                dataType: 'json',
		                data: {
		                    keyword: request.term
		                },
		                success: function(data) {
		                    response(data);
		                }
		            });
		        }
		    });
	
		    // Empêcher la soumission du formulaire lorsque la touche "Entrée" est pressée
		    $('#communeSelect').keydown(function(event) {
		        if (event.keyCode === 13) {
		            event.preventDefault();
		            return false;
		        }
		    });
		});
	
		$(document).ready(function() {
		    $('#federationSelect').autocomplete({
		        source: function(request, response) {
		            $.ajax({
		                url: 'FederationAutocompleteServlet',
		                dataType: 'json',
		                data: {
		                    keyword: request.term
		                },
		                success: function(data) {
		                    response(data);
		                }
		            });
		        }
		    });
		
		    // Empêcher la soumission du formulaire lorsque la touche "Entrée" est pressée
		    $('#federationSelect').keydown(function(event) {
		        if (event.keyCode === 13) {
		            event.preventDefault();
		            return false;
		        }
		    });
		});
		
		/*
		*Cacher les choix de recherche des localisations non sélectionnées
		*/
	    function toggleSearchType() {
	        var searchType = $('#localisationTypeSelect').val();
	        $('#regionGroup, #communeGroup, #departementGroup').hide();
	        $('#' + searchType + 'Group').show();
	    }
	    
	    function searchLicence(offset) {
	    	// Vérifie si la recherche est une nouvelle recherche
	        var isNewSearch = offset === 0;
	
			// Si c'est une nouvelle recherche ou si l'offset est négatif, réinitialiser currentPage à 0
			if (isNewSearch || offset < 0) {
			    currentPage = 0;
			}
	
	        // Récupération des valeurs de offset et limit depuis la page
	        var federation = $('#federationSelect').val();
	        var localisationType = $('#localisationTypeSelect').val();
	        var localisation = $('#' + localisationType + 'Select').val();
	        var limit = document.getElementById("limitSelect").value;
	        var orderBy = document.getElementById("orderBySelect").value;
	        
	     	// Vérifier si à la fois federation et localisation sont vides
	        if (federation === "" && localisation === "") {
	            // Afficher le message d'erreur en rouge
	            $('.message-container').html('<p style="color: red;">Veuillez spécifier au moins une fédération ou une localisation géographique.</p>');
	            $('#message').show(); // Afficher le message
	            return; // Sortir de la fonction si les deux champs sont vides
	        } else {
	            // Effacer le message d'erreur s'il existe
	            $('.message-container').empty();
	            $('#message').hide(); // Masquer le message
	        }
	        $.ajax({
	            url: 'EluServlet',
	            type: 'GET',
	            dataType: 'json',
	            data: {
	                federation: federation,
	                localisation: localisation,
	                localisationType: localisationType,
	                offset: offset,
	                limit: limit,
	                orderBy: orderBy,
	                ascending: ascending // Envoyez le paramètre d'ordre ascendant ou descendant
	                
	            },
	            success: function(data) {
	                var nbLicencesTotal = data.nbLicencesTotal;
	                var nbLicencesHomme = data.nbLicencesHomme;
	                var nbLicencesFemme = data.nbLicencesFemme;
	
	                var pourcentageHomme = ((nbLicencesHomme / nbLicencesTotal) * 100).toFixed(0);
	                var pourcentageFemme = ((nbLicencesFemme / nbLicencesTotal) * 100).toFixed(0);
	                
	           	    // Mettre à jour la variable totalRows avec le nombre total de lignes retournées
	                totalRows = data.licenceList.length;
	
	                updateNbLicences(nbLicencesTotal, nbLicencesHomme, nbLicencesFemme);
	                updateCharts(data.licenceList, pourcentageHomme, pourcentageFemme);
	                updateTabWithLicences(data.licenceListTab);
	             	// Générer les boutons de pagination
	                generatePaginationButtons();
	            },
	            error: function(xhr, status, error) {
	                alert('Erreur lors de la récupération des données: ' + error);
	            }
	        });
	    }
	    
	    function searchLicenceOffset(offset) {
	    	// Vérifie si la recherche est une nouvelle recherche
	        var isNewSearch = offset <= 0;
	
			// Si c'est une nouvelle recherche ou si l'offset est négatif, réinitialiser currentPage à 0
			if (isNewSearch) {
			    currentPage = 0;
			}
	
	        // Récupération des valeurs de offset et limit depuis la page
	        var federation = $('#federationSelect').val();
	        var localisationType = $('#localisationTypeSelect').val();
	        var localisation = $('#' + localisationType + 'Select').val();
	        var limit = document.getElementById("limitSelect").value;
	        var orderBy = document.getElementById("orderBySelect").value;
	        
	     	// Vérifier si à la fois federation et localisation sont vides
	        if (federation === "" && localisation === "") {
	            // Afficher le message d'erreur en rouge
	            $('.message-container').html('<p style="color: red;">Veuillez spécifier au moins une fédération ou une localisation géographique.</p>');
	            $('#message').show(); // Afficher le message
	            return; // Sortir de la fonction si les deux champs sont vides
	        } else {
	            // Effacer le message d'erreur s'il existe
	            $('.message-container').empty();
	            $('#message').hide(); // Masquer le message
	        }
	        $.ajax({
	            url: 'EluServlet',
	            type: 'POST',
	            dataType: 'json',
	            data: {
	                federation: federation,
	                localisation: localisation,
	                localisationType: localisationType,
	                offset: offset,
	                limit: limit,
	                orderBy: orderBy,
	                ascending: ascending // Envoyez le paramètre d'ordre ascendant ou descendant
	                
	            },
	            success: function(data) {
	                var nbLicencesTotal = data.nbLicencesTotal;
	                var nbLicencesHomme = data.nbLicencesHomme;
	                var nbLicencesFemme = data.nbLicencesFemme;
	
	                var pourcentageHomme = ((nbLicencesHomme / nbLicencesTotal) * 100).toFixed(0);
	                var pourcentageFemme = ((nbLicencesFemme / nbLicencesTotal) * 100).toFixed(0);
	                
	                updateTabWithLicences(data.licenceListTab);
	             	// Générer les boutons de pagination
	                generatePaginationButtons();
	            },
	            error: function(xhr, status, error) {
	                alert('Erreur lors de la récupération des données: ' + error);
	            }
	        });
	    }
	
	
	    function updateCharts(data, pourcentageHomme, pourcentageFemme) {
	        var menData = extractChartData(data, 'h_');
	        var womenData = extractChartData(data, 'f_');
	
	        if (menLicenseChart) menLicenseChart.destroy();
	        if (womenLicenseChart) womenLicenseChart.destroy();
	        if (licenseChart) licenseChart.destroy();
	
	        menLicenseChart = new Chart(menCtx, createChartConfig(menData));
	        womenLicenseChart = new Chart(womenCtx, createChartConfig(womenData));
	        licenseChart = new Chart(genreCtx, createChartConfigGenre(pourcentageHomme, pourcentageFemme));
	    }
	
	    function extractChartData(licenceList, genderPrefix) {
	        var labels = [];
	        var data = [];
	        var backgroundColors = [];
	        var percentages = [];
	
	        var ageRanges = ['1_4_ans', '5_9_ans', '10_14_ans', '15_19_ans', '20_24_ans', '25_29_ans', '30_34_ans', '35_39_ans', '40_44_ans', '45_49_ans', '50_54_ans', '55_59_ans', '60_64_ans', '65_69_ans', '70_74_ans', '75_79_ans', '80_99_ans', 'NR'];
	        var colors = ['#FF6384', '#36A2EB', '#FFCE56', '#E7E9ED', '#4BC0C0', '#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360', '#AC64AD', '#616774', '#FAA75A', '#6B9BC7', '#5E9BD4', '#A084CF', '#AFB42B', '#616161'];
	
	        var totalLicences = 0;
	        licenceList.forEach(function(licence) {
	            if (genderPrefix === 'h_') {
	                totalLicences += getTotalHommes(licence);
	            } else if (genderPrefix === 'f_') {
	                totalLicences += getTotalFemmes(licence);
	            }
	        });
	
	        for (var i = 0; i < ageRanges.length; i++) {
	            var total = 0;
	            licenceList.forEach(function(licence) {
	                total += licence[genderPrefix + ageRanges[i]];
	            });
	
	            if (total > 0) {
	                var percentage = (total / totalLicences) * 100;
	                labels.push(ageRanges[i].replace('_', ' à '));
	                data.push(total);
	                backgroundColors.push(colors[i]);
	                percentages.push(percentage.toFixed(2)); // Ajoutez le pourcentage calculé à la liste des pourcentages
	            }
	        }
	
	        return {
	            labels: labels,
	            datasets: [{
	                data: data,
	                backgroundColor: backgroundColors,
	                hoverBackgroundColor: backgroundColors,
	                percentages: percentages // Ajoutez la liste des pourcentages à l'objet retourné
	            }]
	        };
	    }
	
	    function createChartConfig(chartData) {
	        return {
	            type: 'bar',
	            data: chartData,
	            options: {
	                responsive: true,
	                legend: {
	                    display: false
	                },
	                tooltips: {
	                    callbacks: {
	                        label: function(tooltipItem, data) {
	                            var dataset = data.datasets[tooltipItem.datasetIndex];
	                            var currentValue = dataset.data[tooltipItem.index];
	                            var percentage = dataset.percentages[tooltipItem.index]; // Récupérez le pourcentage correspondant
	                            return 'Total : ' + currentValue + ' (' + percentage + '%)'; // Ajoutez le pourcentage à l'affichage de la tooltip
	                        }
	                    }
	                },
	                animation: {
	                    animateScale: true,
	                    animateRotate: true
	                }
	            }
	        };
	    }
	
	
	
	    function createChartConfigGenre(pourcentageHomme, pourcentageFemme) {
	        var data = {
	            labels: ['Hommes', 'Femmes'],
	            datasets: [{
	                data: [pourcentageHomme, pourcentageFemme],
	                backgroundColor: ['#3366CC', '#DC3912'],
	                hoverBackgroundColor: ['#3366CC', '#DC3912']
	            }]
	        };
	
	        return {
	            type: 'doughnut',
	            data: data,
	            options: {
	                responsive: true,
	                legend: {
	                    position: 'right'
	                },
	                tooltips: {
	                    callbacks: {
	                        label: function(tooltipItem, data) {
	                            var label = data.labels[tooltipItem.index] || '';
	                            if (label) {
	                                label += ': ';
	                            }
	                            label += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + '%';
	                            return label;
	                        }
	                    }
	                },
	                animation: {
	                    animateScale: true,
	                    animateRotate: true
	                }
	            }
	        };
	    }
	
	
	
	    function getTotalFemmes(licence) {
	        var totalFemmes = 0;
	        for (var key in licence) {
	            if (key.startsWith("f_")) {
	                totalFemmes += licence[key];
	            }
	        }
	        return totalFemmes;
	    }
	
	    function getTotalHommes(licence) {
	        var totalHommes = 0;
	        for (var key in licence) {
	            if (key.startsWith("h_")) {
	                totalHommes += licence[key];
	            }
	        }
	        return totalHommes;
	    }
	    
	    function getPourcentageTotalFemmes(licence) {
	        var totalFemmes = 0;
	        for (var key in licence) {
	            if (key.startsWith("f_")) {
	                totalFemmes += licence[key];
	            }
	        }
	        totalFemmes = ((totalFemmes / licence.total) * 100).toFixed(0);
	        return totalFemmes;
	    }
	
	    function getPourcentageTotalHommes(licence) {
	        var totalHommes = 0;
	        for (var key in licence) {
	            if (key.startsWith("h_")) {
	                totalHommes += licence[key];
	            }
	        }
	        totalHommes = ((totalHommes / licence.total) * 100).toFixed(0);
	        return totalHommes;
	    }
	
	    
	    function updateNbLicences(nbLicencesTotal, nbLicencesHomme, nbLicencesFemme) {
	
	        $('#nbLicencesTotal').text("Total : " + nbLicencesTotal);
	        $('#nbLicencesHomme').text("Hommes : " + nbLicencesHomme);
	        $('#nbLicencesFemme').text("Femmes : " + nbLicencesFemme);
	    }
	
	
		// Ajoutez les fonctions pour gérer la pagination
	
		
		function changeLimit() {
			limit = parseInt($('#limitSelect').val());
			currentPage = 0; // Réinitialiser la page actuelle lors du changement de limite
			searchLicenceOffset(0);
		}
		
		function updateTabWithLicences(licenceList) {
		    var tabId = $('#tabId');
		    tabId.empty(); // Videz le contenu du tbody avant de le remplir à nouveau
	
			 // Boucle pour ajouter les nouvelles données à la table
		        for (var i = 0; i < licenceList.length; i++) {
		            var licence = licenceList[i];
		            var row = "<tr>" +
		                "<td>" + licence.federation + "</td>" +
		                "<td>" + licence.commune + "</td>" +
		                "<td>" + licence.departement + "</td>" +
		                "<td>" + licence.region + "</td>" +
		                "<td>" + licence.total + "</td>" +
		                "<td>" + getTotalFemmes(licence) + "</td>" +
		                "<td>" + getTotalHommes(licence) + "</td>" +
		                "<td>" + getPourcentageTotalFemmes(licence) + "%</td>" +
		                "<td>" + getPourcentageTotalHommes(licence) + "%</td>";
	
		            $('#tabId').append(row);
		        }
		}
		
		// Configuration de la pagination adaptée à la taille des données
		
		
		function generatePaginationButtons() {
		    var totalPages = Math.ceil(totalRows / limit)-1; // Calcul du nombre total de pages
		    var paginationList = document.querySelector('.pagination'); // Sélection de la liste de pagination
		
		    // Effacer les anciens boutons de pagination
		    paginationList.innerHTML = '';
		
		    // Ajouter le bouton pour la première page
		    var firstPageButton = createPageButton('<<', 0);
		    paginationList.appendChild(firstPageButton);
		
		    // Ajouter le bouton pour la page précédente si currentPage n'est pas la première page
		    if (currentPage > 0) {
		        var prevPageButton = createPageButton('<', currentPage - 1);
		        paginationList.appendChild(prevPageButton);
		    }
		
		    // Ajouter les boutons pour les quelques pages avant la page actuelle
		    for (var i = Math.max(0, currentPage - 2); i < currentPage; i++) {
		        var pageButton = createPageButton(i, i);
		        paginationList.appendChild(pageButton);
		    }
		
		    // Ajouter le bouton pour la page actuelle
		    var currentPageButton = createPageButton(currentPage, currentPage);
		    paginationList.appendChild(currentPageButton);
		
		    // Ajouter les boutons pour les quelques pages après la page actuelle
		    for (var i = currentPage + 1; i <= Math.min(currentPage + 2, totalPages); i++) {
		        var pageButton = createPageButton(i, i);
		        paginationList.appendChild(pageButton);
		    }
		
		    // Ajouter le bouton pour la page suivante si currentPage n'est pas la dernière page
		    if (currentPage < totalPages) {
		        var nextPageButton = createPageButton('>', currentPage + 1);
		        paginationList.appendChild(nextPageButton);
		    }
		
		    // Ajouter le bouton pour la dernière page
		    var lastPageButton = createPageButton('>>', totalPages);
		    paginationList.appendChild(lastPageButton);
		}
		
		function createPageButton(label, pageNumber) {
		    var li = document.createElement('li'); // Création d'un élément li pour chaque bouton de page
		    var a = document.createElement('a'); // Création d'un élément a (lien) pour chaque bouton de page
		    a.href = "#"; // Définition de l'attribut href du lien à "#" pour empêcher la redirection
		    a.textContent = label; // Définition du texte du lien comme numéro de page ou marqueur de première/dernière page
		    if (pageNumber === currentPage) {
		        li.classList.add('active'); // Ajoute la classe .active au bouton de la page actuelle
		    }
		    a.onclick = function () { // Ajout d'un gestionnaire d'événement onclick pour chaque lien de page
		        currentPage = pageNumber; // Met à jour la page actuelle
		        searchLicenceOffset(currentPage * limit); // Appel de la fonction searchLicence avec le nouvel offset
		        generatePaginationButtons(); // Regénère les boutons de pagination après le changement de page
		    };
		    li.appendChild(a); // Ajout du lien à l'élément li
		    return li; // Retourne l'élément li avec le bouton de page correspondant
		}

	</script>
</body>
</html>
