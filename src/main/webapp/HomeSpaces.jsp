<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="jakarta.tags.core" %>
<html>
<head>
    <title>Accueil</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="styleActeur.css">
    <link rel="stylesheet" type="text/css" href="styleHeaderFooter.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <style>
        .name-lastname {
            position: absolute;
            bottom: 10px; /* Ajustez la distance par rapport au bas selon vos besoins */
            right: 10px; /* Ajustez la distance par rapport à la droite selon vos besoins */
        }
        .btn-subscribe, .btn-unsubscribe {
            display: flex;
            align-items: center;
            justify-content: center;
            margin-top: 10px;
        }
        .btn-subscribe i, .btn-unsubscribe i {
            margin-right: 5px;
        }
        .list-group-item {
            position: relative;
            margin-bottom: 10px;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }
        .list-group-item h3 {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<%@ include file="HeaderGA.jsp" %>
<div class="main-content d-flex flex-column min-vh-100" style="margin-top: 50px;">
    <div class="container mt-5">
        <h2 style="color: #fff; text-shadow: 2px 2px 4px #000;">Mes Abonnements</h2>
        <ul class="list-group" id="subscriptionsList">
            <c:forEach var="space" items="${subscribedSpaces}">
                <li class="list-group-item">
                    <h3>${space.federation}-${space.ville}</h3>
                    <a href="PostsServlet?spaceId=${space.spaceId}" style="font-weight: bold; color: #007bff;">${space.name}</a>
                    <p style="font-size: 0.9rem; color: #666;">${space.description}</p>
                    <div class="name-lastname">
                        <i>${space.nom} ${space.prenom}</i>
                    </div>
                    <button onclick="unsubscribe(${space.spaceId}, this)" class="btn btn-danger btn-unsubscribe">
                        <i class="fas fa-minus"></i> Se désabonner
                    </button>
                </li>
            </c:forEach>
        </ul>
        
        <h2 style="color: #fff; text-shadow: 2px 2px 4px #000;" class="mt-5">Liste des Espaces</h2>
        <ul class="list-group" id="spacesList">
            <c:forEach var="space" items="${otherSpaces}">
                <li class="list-group-item">
                    <h3>${space.federation}-${space.ville}</h3>
                    <a href="PostsServlet?spaceId=${space.spaceId}" style="font-weight: bold; color: #007bff;">${space.name}</a>
                    <p style="font-size: 0.9rem; color: #666;">${space.description}</p>
                    <div class="name-lastname">
                        <i>${space.nom} ${space.prenom}</i>
                    </div>
                    <button onclick="subscribe(${space.spaceId}, this)" class="btn btn-primary btn-subscribe">
                        <i class="fas fa-plus"></i> S'abonner
                    </button>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>
<br><br><br>
<%@ include file="footer.jsp" %>

<!-- Modal -->
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="messageModalLabel">Notification</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modalMessage">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://kit.fontawesome.com/a076d05399.js"></script> <!-- Pour les icônes FontAwesome -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script>
function subscribe(spaceId, button) {
    $.post("SubscribeServlet", {spaceId: spaceId}, function(response) {
        if (response.success) {
            showModal("Abonné avec succès !");
            // Ajouter l'espace aux abonnements de l'utilisateur
            var spaceItem = $(button).closest('li').clone();
            spaceItem.find('button').remove();
            spaceItem.append('<button onclick="unsubscribe(' + spaceId + ', this)" class="btn btn-danger btn-unsubscribe"><i class="fas fa-minus"></i> Se désabonner</button>');
            $('#subscriptionsList').append(spaceItem);
            // Supprimer le bouton "S'abonner"
            $(button).closest('li').remove();
        } else {
            showModal(response.message);
        }
    }, 'json');
}

function unsubscribe(spaceId, button) {
    $.post("UnsubscribeServlet", {spaceId: spaceId}, function(response) {
        if (response.success) {
            showModal("Désabonné avec succès !");
            // Ajouter l'espace à la liste des espaces
            var spaceItem = $(button).closest('li').clone();
            spaceItem.find('button').remove();
            spaceItem.append('<button onclick="subscribe(' + spaceId + ', this)" class="btn btn-primary btn-subscribe"><i class="fas fa-plus"></i> S\'abonner</button>');
            $('#spacesList').append(spaceItem);
            // Supprimer le bouton "Se désabonner"
            $(button).closest('li').remove();
        } else {
            showModal(response.message);
        }
    }, 'json');
}

function showModal(message) {
    $('#modalMessage').text(message);
    $('#messageModal').modal('show');
}
</script>
</body>
</html>
